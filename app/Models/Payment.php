<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'code', 'content', 'total_money','book_id','create_time','money_word',
    ];
}
