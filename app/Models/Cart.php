<?php

namespace App\Models;

class Cart {

    public $courses = null;
    public $totalPrice = 0;
    public $totalQuantity = 0;

    public function __construct($cart) {
        if ($cart) {
            $this->courses = $cart->courses;
            $this->totalPrice = $cart->totalPrice;
            $this->totalQuantity = $cart->totalQuantity;
        }
    }

    public function addCart($course, $id) {
//        $newProduct = ['quantity' => 0, 'fees' =>  $course->fees, 'courseInfo' => $course];
        $newProduct = ['fees' =>  $course->fees, 'courseInfo' => $course];
        $check = 0;
        if ($this->courses) {
            if (array_key_exists($id, $this->courses)) {
                $newProduct = $this->courses[$id];
                $check++;
            }
        }
//        $newProduct['quantity']++;
//        $newProduct['fees'] = $newProduct['quantity'] * $course->fees;
//        $newProduct['fees'] = $course->fees;
        if ($check == 0) {
            $this->courses[$id] = $newProduct;
            $this->totalPrice += $course->fees;
            $this->totalQuantity++;
        }
    }

    public function deleteItemCart($id) {
        $this->totalQuantity--;
        $this->totalPrice -= $this->courses[$id]['fees'];
        unset($this->courses[$id]);
    }

}

?>
