<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    protected $table = 'certifications';

    protected $fillable = [
        'student_id', 'course_id', 'code', 'create_date', 'rank', 'point', 'test_date'
    ];

    public function createCode() {
        //Get date now
        $now = Carbon::now();
        $year = $now->year;
        $certification = Certification::get()->last();
        $code = "CN-TQ/".$year.'/0001';
        if ($certification != null) {
            $string = explode('/', $certification->code);
            $previousCode = (integer)$string[2];
            if ($certification->id > 8) {
                $code = "CN-TQ/".$year.'/00'.($previousCode + 1);
            }
            elseif ($certification->id > 98) {
                $code = "CN-TQ/".$year.'/0'.($previousCode + 1);
            }
            elseif ($certification->id > 998) {
                $code = "CN-TQ/".$year.'/'.($previousCode + 1);
            }
            else {
                $code = "CN-TQ/".$year.'/000'.($previousCode + 1);
            }
        }
        return $code;
    }
}
