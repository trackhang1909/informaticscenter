<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ReceiptDifferent extends Model
{
    protected $table = 'receipt_differents';

    protected $fillable = [
        'code', 'name', 'address', 'reason', 'total_money', 'money_text',
    ];

    public function createCode() {
        //Get date now
        $now = Carbon::now();
        $year = $now->year;
        $receipt = ReceiptDifferent::get()->last();
        $code = "PT/".$year.'/0001';
        if ($receipt != null) {
            $string = explode('/', $receipt->code);
            $previousCode = (integer)$string[2];
            if ($previousCode > 8) {
                $code = "PT/".$year.'/00'.($previousCode + 1);
            }
            elseif ($previousCode > 98) {
                $code = "PT/".$year.'/0'.($previousCode + 1);
            }
            elseif ($previousCode > 998) {
                $code = "PT/".$year.'/'.($previousCode + 1);
            }
            else {
                $code = "PT/".$year.'/000'.($previousCode + 1);
            }
        }
        return $code;
    }

    public function read($code) {
        $data = ReceiptDifferent::where('code', '=', $code)->first();
        return $data;
    }
}
