<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class WareHouseBook extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    protected $table = 'warehouse_books';

    protected $fillable = [
        'book_id', 'quantity', 'total_money',
    ];

}
