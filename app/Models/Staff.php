<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Staff extends Model
{
    protected $table = 'staffs';

    protected $fillable =[
        'name','birthday','email','phone_number','country','start_time','salary','position'
    ];
    protected $rule=[];
    protected $messages=[];
}
