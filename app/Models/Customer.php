<?php
namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';

    protected $fillable = [
        'name', 'phone', 'address','book_id','quantity','money',
    ];
//    public function createCode() {
//        //Get date now
//        $now = Carbon::now();
//        $year = $now->year;
//        $receipt = Receipt::get()->last();
//        $code = "PTS-TQ/".$year.'/0001';
//        if ($receipt != null) {
//            $string = explode('/', $receipt->code);
//            $previousCode = (integer)$string[2]; //9
//            if ($previousCode > 8) {
//                $code = "PTS-TQ/".$year.'/00'.($previousCode + 1);
//            }
//            elseif ($previousCode > 98) {
//                $code = "PTS-TQ/".$year.'/0'.($previousCode + 1);
//            }
//            elseif ($previousCode > 998) {
//                $code = "PTS-TQ/".$year.'/'.($previousCode + 1);
//            }
//            else {
//                $code = "PTS-TQ/".$year.'/000'.($previousCode + 1);
//            }
//        }
//        return $code;
//    }

}
