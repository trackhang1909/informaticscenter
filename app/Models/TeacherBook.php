<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherBook extends Model
{
    protected $table = 'teacher_book';

    protected $fillable = [
        'teacher_id', 'book_id'
    ];
}
