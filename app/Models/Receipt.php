<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Receipt extends Model
{
    protected $table = 'receipts';

    protected $fillable = [
        'code', 'student_id', 'schedule_id', 'register_time', 'reason',
        'advance_money', 'total_money', 'remain_money', 'money_text'
    ];

    public function createCode() {
        //Get date now
        $now = Carbon::now();
        $year = $now->year;
        $receipt = Receipt::get()->last();
        $code = "PT-TQ/".$year.'/0001';
        if ($receipt != null) {
            $string = explode('/', $receipt->code);
            $previousCode = (integer)$string[2]; //9
            if ($previousCode > 8) {
                $code = "PT-TQ/".$year.'/00'.($previousCode + 1);
            }
            elseif ($previousCode > 98) {
                $code = "PT-TQ/".$year.'/0'.($previousCode + 1);
            }
            elseif ($previousCode > 998) {
                $code = "PT-TQ/".$year.'/'.($previousCode + 1);
            }
            else {
                $code = "PT-TQ/".$year.'/000'.($previousCode + 1);
            }
        }
        return $code;
    }

    public function resetMembershipInClass($id, $require) {
        $receipt = DB::table('receipts')
            ->join('students', 'students.id','=', 'receipts.student_id')
            ->where('receipts.id', '=', $id)->first();
        //Check total schedule in receipts table
        $allReceipt = Receipt::get();
        $allSchedule = new Collection();
        foreach ($allReceipt as $item) {
            $schedule = explode(' ', $item->schedule_id);
            foreach ($schedule as $item2) {
                if ($item2 !== '')  {
                    $allSchedule->push($item2);
                }
            }
        }
        //Current schedule
        $currentSchedule = new Collection();
        $string = explode(' ', $receipt->schedule_id);
        foreach ($string as $item) {
            if ($item !== '')  {
                //Get membership of current schedules
                $count = 0;
                foreach ($allSchedule as $item2) {
                    if ($item2 === $item) {
                        $count++;
                    }
                }
                $currentSchedule->put($item, $count);
            }
        }
        //Update membership in class (current schedule)
        foreach ($string as $item) {
            if ($item !== '')  {
                $query = DB::table('schedules')
                    ->join('classes', 'classes.id', '=', 'schedules.class_id')
                    ->where('schedules.id', '=', $item)
                    ->select('schedules.*', 'classes.membership as class_membership')
                    ->first();
                if ($require == 'minus') {
                    $newMembership = $query->class_membership - $currentSchedule->get($item);
                    //Save new membership
                    $schedule = Schedule::find($item);
                    $schedule->membership = $newMembership;
                    $schedule->save();
                }
                else {
                    //Save new membership
                    $schedule = Schedule::find($item);
                    $schedule->membership += 1;
                    $schedule->save();
                }
            }
        }
    }

    public function read($code) {
        $receipt = DB::table('receipts')
            ->join('students', 'students.id', '=', 'receipts.student_id')
            ->select('receipts.*', 'students.id as student_id','students.name as name', 'students.phone_number as phone_number', 'students.email as email')
            ->where('receipts.code', '=', $code)
            ->first();
        if ($receipt != null) {
            //Get id, name of courses
            $course_name = '';
            $course_id = '';
            $scheduleText = explode(' ', $receipt->schedule_id);
            for ($i = 0; $i < sizeof($scheduleText); $i++) {
                if ($scheduleText[$i] !== '') {
                    $query = DB::table('schedules')
                        ->join('courses', 'courses.id', '=', 'schedules.course_id')
                        ->where('schedules.id', '=', $scheduleText[$i])
                        ->select('courses.name as course_name', 'courses.id as course_id')
                        ->first();
                    $course_id .= $query->course_id;
                    $course_name .= $query->course_name;
                    if ($i < (sizeof($scheduleText) - 2)) {
                        $course_id .= ', ';
                        $course_name .= ', ';
                    }
                }
            }
            $receipt->course_id = $course_id;
            $receipt->course_name = $course_name;

            return $receipt;
        }
    }

}
