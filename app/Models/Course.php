<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $table = 'courses';

    protected $fillable = [
        'name', 'content', 'fees', 'open_time', 'close_time', 'group', 'type',
    ];
}
