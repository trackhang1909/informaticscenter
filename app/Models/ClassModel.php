<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassModel extends Model
{
    protected $table = 'classes';

    protected $fillable = [
        'name', 'campus_code', 'membership',
    ];

    public function getCampusName($code) {
        return Campus::where('code', 'like', $code)->value('name');
    }
}
