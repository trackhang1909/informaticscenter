<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Campus extends Model
{
    protected $table = 'campuses';

    protected $fillable = [
        'name', 'code',
    ];

    public function createCode($code) {
        $new_code = explode(" ", $code);
        $first = substr($new_code[0], 0, 1);
        $second = substr($new_code[1], 0,1);
        $result = strtoupper($first.$second);
        return $result;
    }
}
