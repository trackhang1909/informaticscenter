<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'students';

    protected $fillable = [
        'name', 'phone_number', 'email', 'birthday', 'address', 'user_id',
    ];
}
