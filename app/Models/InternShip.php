<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class InternShip extends Model
{
    use SoftDeletes;
    protected $table ='internships';

    protected $fillable = [
        'name', 'email','phone','university',
        'image','topic_id','type','total_time',
        'start_time','end_time','cmnd','address','level','cmndday','cmndplace','status_id',
    ];
}
