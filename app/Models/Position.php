<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'positions';

    protected $fillable = [
        'name', 'code',
    ];
    public function parstCode($code) {
        $new_code = explode(" ", $code);
        $first = substr($new_code[0], 0, 1);
        $second = substr($new_code[1], 0,1);
//        $end =  substr($code, strlen($code) - 0, 1);
        $result = strtoupper($first.$second);
        return $result;
    }
}
