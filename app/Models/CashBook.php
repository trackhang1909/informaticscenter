<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CashBook extends Model
{
    protected $table = 'cash_book';

    protected $fillable = [
        'id', 'cash_code',
    ];
}
