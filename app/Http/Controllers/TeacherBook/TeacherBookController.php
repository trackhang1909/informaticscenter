<?php

namespace App\Http\Controllers\TeacherBook;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeacherBookController extends Controller
{
    public function index(){
        $data = DB::table('teacher_book')
            ->join('staffs','staffs.id','like','teacher_book.teacher_id')
            ->join('books','books.id','like','teacher_book.book_id')
            ->select('teacher_book.*','staffs.name as staffname','books.name as namebook')
            ->paginate('6');
        return view('teacher_book', compact('data'));
    }
}
