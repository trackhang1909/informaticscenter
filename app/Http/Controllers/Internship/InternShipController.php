<?php

namespace App\Http\Controllers\Internship;

use App\Models\InternShip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Models\Status;

class InternShipController extends Controller
{
    const IMAGE_SERVICE = 'images/book';

    public function index()
    {
        $data = DB::table('internships')
            ->join('topics','topics.id','like','internships.topic_id')
            ->select('internships.*','topics.name as topicname')
            ->paginate('4');
        $top = DB::table('topics')->get();
        return view('register_intern',compact('data'))->with('top',$top);
    }

    //
    public function show(){
        $data = DB::table('internships')
            ->join('topics','topics.id','like','internships.topic_id')
            ->join('status','status.id','like','internships.status_id')
            ->select('internships.*','topics.name as topicname','status.name as statusname')
            ->paginate('8');

        $top = DB::table('topics')->get();
        $sta = DB::table('status')->get();
        return view('intern', compact('data'))
            ->with('top',$top)
            ->with('sta',$sta);
    }
    public function store(Request $request){
        $intern = new InternShip();
        $check = 0;
        if($request->id){
            $check = 1;
        }
        try {
//          Cách 2:  $intern = InternShip::updateOrCreate(['id' => $request->id], $request->all());
            $intern->name = $request->name;
            $intern->email = $request->email;
            $intern->phone = $request->phone;
            $intern->university = $request->university;
            $intern->image = $request->image;
            $intern->topic_id = $request->topic_id;
            $intern->type = $request->type;
            $intern->total_time = $request->total_time;
            $intern->start_time = $request->start_time;
            $intern->end_time = $request->end_time;
            $intern->cmnd = $request->cmnd;
            $intern->address = $request->address;
            $intern->level = $request->level;
            $intern->cmndday = $request->cmndday;
            $intern->cmndplace = $request->cmndplace;
            if($request->hasFile('image')){
                $file = $request->file('image');
                $name = $file->getClientOriginalName();
                $destination = self::IMAGE_SERVICE.'/' .$name;
                Storage::disk('public')->put($destination, file_get_contents($file),'public');
                $intern->image = $destination;
            }
            $intern->save();
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));

        }catch (\Exception $ex){
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'.$ex));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }

    }

    public function destroy(Request $request) {
        $intern = new InternShip();
        try {
            $intern = InternShip::find($request->id)->delete();
            return redirect()->back()->with('success','Sinh viên đã hoàn thành');
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function trash(){

        $data = DB::table('internships')
            ->join('topics','topics.id','like','internships.topic_id')
            ->where('internships.deleted_at','<>', null)
            ->select('internships.*','topics.name as topicname')
            ->paginate('4');
        $top = DB::table('topics')->get();
        return view('complete', compact('data'))->with('top',$top);
    }

    public function unfinished($id){

        $data2 = InternShip::withTrashed()->find($id);
        $data2->restore();

        $data = DB::table('internships')
            ->join('topics','topics.id','like','internships.topic_id')
            ->where('internships.deleted_at','=', null)
            ->select('internships.*','topics.name as topicname')
            ->paginate('4');
        return redirect()->back()->with('success','Đã chuyển về danh sách chưa hoàn thành');
    }

    public function ajax(Request $request)
    {
        $id = $request->id;
        $sta = Status::find($id);
        $innerShipId = $request->innerShipId;
        $InternShip = InternShip::findOrfail($innerShipId);
        $InternShip->status_id = $id;
        $InternShip->update();
        echo $sta->name;
    }
    public function filter1(){
        $data = DB::table('internships')
            ->join('status','status.id','like','internships.status_id')
            ->join('topics','topics.id','like','internships.topic_id')
            ->select('internships.*','status.name as statusname','topics.name as topicname')
            ->where('internships.status_id','=', 1)
            ->paginate('16');
        $sta = DB::table('status')->get();
        $top = DB::table('topics')->get();
        return view('intern', compact('data'))
            ->with('sta',$sta)
            ->with('top',$top);

    }
    public function filter2(){
        $data = DB::table('internships')
            ->join('status','status.id','like','internships.status_id')
            ->join('topics','topics.id','like','internships.topic_id')
            ->select('internships.*','status.name as statusname','topics.name as topicname')
            ->where('internships.status_id','=', 2)
            ->paginate('16');
        $sta = DB::table('status')->get();
        $top = DB::table('topics')->get();
        return view('intern', compact('data'))
            ->with('sta',$sta)
            ->with('top',$top);

    }
    public function filter3(){
        $data = DB::table('internships')
            ->join('status','status.id','like','internships.status_id')
            ->join('topics','topics.id','like','internships.topic_id')
            ->select('internships.*','status.name as statusname','topics.name as topicname')
            ->where('internships.status_id','=', 3)
            ->paginate('16');
        $sta = DB::table('status')->get();
        $top = DB::table('topics')->get();
        return view('intern', compact('data'))
            ->with('sta',$sta)
            ->with('top',$top);

    }
    public function filter4(){
        $data = DB::table('internships')
            ->join('status','status.id','like','internships.status_id')
            ->join('topics','topics.id','like','internships.topic_id')
            ->select('internships.*','status.name as statusname','topics.name as topicname')
            ->where('internships.status_id','=', 4)
            ->paginate('16');
        $sta = DB::table('status')->get();
        $top = DB::table('topics')->get();
        return view('intern', compact('data'))
            ->with('sta',$sta)
            ->with('top',$top);
    }

}
