<?php

namespace App\Http\Controllers;

use App\Models\Campus;
use App\Models\ClassModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Charts;

class CampusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = Campus::where('name', 'like', '%'.$_GET['data'].'%')->paginate(7);
            return view('campus.campus', compact('data'));
        }
        $data = DB::table('campuses')->paginate(7);
        return view('campus.campus', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $campus = new Campus();
        $check = $request->id ? 1 : 0;

        if ($request->id) {
            $campus = Campus::find($request->id);
        }

        $campus->name = $request->name;
        $campus->code = $campus->createCode($request->name);

        try {
            $campus->save();
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $campus = Campus::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Show detail
    public function detail($name)
    {
        $data = Campus::where('name', $name)->paginate(7);
        return view('campus.campus', compact('data'));
    }

    //Find with name
    public function find(Request $request)
    {
        $data = Campus::where('name', 'like', '%' . $request->value . '%')->get();
        return response()->json($data);
    }

    //Display class list in campus
    public function classList($code)
    {
        $data = DB::table('classes')
            ->join('campuses', 'code', 'like', 'classes.campus_code')
            ->where('classes.campus_code', 'like', $code)
            ->select('classes.*', 'campuses.name as campus_name')
            ->orderBy('classes.name', 'asc')
            ->paginate(7);

        $codeCampus = DB::table('campuses')->get();
        return view('class.class', compact('data'))->with('campuses', $codeCampus);
    }

}
