<?php

namespace App\Http\Controllers\Topic;

use App\Http\Controllers\Controller;
use App\Models\Topic;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TopicController extends Controller
{
    public function index(){
        $data = DB::table('topics')
            ->paginate('4');
        return view('topic', compact('data'));
    }
    public function store(Request $request){
        $topic = new Topic();
        $check = 0;
        if ($request->id) {
            $check = 1;
        }

        try {
            $topic = Topic::updateOrCreate(['id' => $request->id], $request->all());
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }
    public function destroy(Request $request){
        try {
            $data = Topic::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }
}
