<?php

namespace App\Http\Controllers\Customer;

use App\Models\Book;
use App\Models\CashBook;
use App\Models\Customer;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CustomerController extends Controller
{
    public function sell(){
        $data = DB::table('customers')
            ->join('books','books.id','like','customers.book_id')
            ->select('customers.*','books.name as bookname','books.image as bookimage',
                'books.price as bookprice','books.author as bookauthor')
            ->paginate('3');
        $book = DB::table('books')->get();
        return view('customer_book', compact('data'));
    }
    public function createCode() {
        //Get date now
        $now = Carbon::now();
        $year = $now->year;
        $receipt = Customer::get()->last();
        $code = "PT-SACH/".$year.'/0001';
        if ($receipt != null) {
            $string = explode('/', $receipt->code);
            $previousCode = (integer)$string[2];
            if ($previousCode > 8) {
                $code = "PT-SACH/" . $year . '/00' . ($previousCode + 1);
            } elseif ($previousCode > 98) {
                $code = "PT-SACH/" . $year . '/0' . ($previousCode + 1);
            } elseif ($previousCode > 998) {
                $code = "PT-SACH/" . $year . '/' . ($previousCode + 1);
            } else {
                $code = "PT-SACH/" . $year . '/000' . ($previousCode + 1);
            }
        }
        return $code;
    }

    public function store(Request $request)
    {
        $book = Book::find($request->book_id);
        $custom = new Customer();
        $custom->name = $request->name;
        $custom->phone = $request->phone;
        $custom->address = $request->address;
        $custom->book_id = $request->book_id;
        $custom->quantity = $request->quantity;
        $custom->money = ($book->price)*($request->quantity);
        $custom->code = $this->createCode();
        $custom->save();
        return redirect()->back()->with('success', 'Đăng kí mua sách thành công');
    }
    public function register(){

        $data = DB::table('customers')
            ->join('books','books.name','like','customers.book_id')
            ->select('customers.*','books.name as bookname')
            ->get();
        $book = DB::table('books')->get();
        return view('register_book', compact('data'))->with('book',$book);
    }
    public function price(Request $request){
        $id = $request->id;
        $price = Book::find($id);


    }

}
