<?php

namespace App\Http\Controllers;

use App\Models\CashBook;
use App\Models\Receipt;
use App\Models\ReceiptDifferent;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceiptDifferentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\Response|\Illuminate\View\View
     */
    public function index()
    {
        $data = ReceiptDifferent::paginate(7);
        return view('receipt.receipt_different', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.ch
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $check = $request->id ? 1 : 0;
            $id = $request->id ? $request->id : null;
            $receiptDiff = $id ? ReceiptDifferent::find($id)->first() : new ReceiptDifferent();
            $receiptDiff->code = $id ? $receiptDiff->code : $receiptDiff->createCode();
            $receiptDiff->name = $request->name;
            $receiptDiff->address = $request->address;
            $receiptDiff->reason = $request->reason;
            $receiptDiff->total_money = $request->total_money;
            $receiptDiff->money_text = $request->money_text;
            $receiptDiff->save();


            if ($check === 0) {
                return redirect()->back()->with('success', __('receipt.create').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $exception) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('receipt.create').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $receipt = ReceiptDifferent::findOrFail($id);
            $cashBook = CashBook::where("cash_code", "=", $receipt->code)->first()->delete();
            $receipt->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Review, export and save pdf receipt
    public function reviewReceipt($id, Request $request) {
        try {
            if (isset($request->code)) {
                $receipt = ReceiptDifferent::where("code", "=", $request->code)->first();
            }
            else {
                $receipt = ReceiptDifferent::find($id);
            }
            if (isset($request->export)) {
                //Save to cash_book
                $cashBook = CashBook::where("cash_code", "=", $receipt->code)->first() ? CashBook::where("cash_code", "=", $receipt->code)->first() : new CashBook();
                $cashBook->cash_code = $receipt->code;
                $cashBook->save();
                //Export pdf
                $pdf = PDF::loadView('receipt.cash_different_pdf', array('receipt' => $receipt));
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download('Receipt-Different.pdf');
            }
            else {
                return view('receipt.cash_different_pdf', compact('receipt'));
            }
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

    //Show detail
    public function detail($input)
    {
        try {
            $data = ReceiptDifferent::find($input)->paginate(1);
            return view('receipt.receipt_different', compact('data'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

    //Find receipt
    public function find(Request $request)
    {
        try {
            $data = DB::table('receipt_differents')
                ->where('code', 'like', '%' . $request->value . '%')
                ->orWhere('name', 'like', '%' . $request->value . '%')
                ->orWhere('address', 'like', '%' . $request->value . '%')
                ->get();
            return response()->json($data);
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }
}
