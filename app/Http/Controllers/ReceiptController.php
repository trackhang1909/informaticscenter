<?php

namespace App\Http\Controllers;

use App\Models\CashBook;
use App\Models\Receipt;
use App\Models\Schedule;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function index()
    {
        try {
            if (isset($_GET['data'])) {
                $receipt = DB::table('receipts')
                    ->join('students', 'students.id', '=', 'receipts.student_id')
                    ->where('receipts.code', 'like', '%' . $_GET['data'] . '%')
                    ->orWhere('students.name', 'like', '%' . $_GET['data'] . '%')
                    ->select('receipts.*', 'students.name as name', 'students.phone_number as phone_number', 'students.email as email')
                    ->paginate(7);
                //Get id, name of courses
                foreach ($receipt as $item) {
                    $course_name = '';
                    $course_id = '';
                    $scheduleText = explode(' ', $item->schedule_id);
                    for ($i = 0; $i < sizeof($scheduleText); $i++) {
                        if ($scheduleText[$i] !== '') {
                            $query = DB::table('schedules')
                                ->join('courses', 'courses.id', '=', 'schedules.course_id')
                                ->where('schedules.id', '=', $scheduleText[$i])
                                ->select('courses.name as course_name', 'courses.id as course_id')
                                ->first();
                            $course_id .= $query->course_id;
                            $course_name .= $query->course_name;
                            if ($i < (sizeof($scheduleText) - 2)) {
                                $course_id .= ', ';
                                $course_name .= ', ';
                            }
                        }
                    }
                    $item->course_id = $course_id;
                    $item->course_name = $course_name;
                }
                return view('receipt.receipt', compact('receipt'));
            }
            $receipt = DB::table('receipts')
                ->join('students', 'students.id', '=', 'receipts.student_id')
                ->select('receipts.*', 'students.id as student_id','students.name as name', 'students.phone_number as phone_number', 'students.email as email')
                ->paginate(7);
            //Get id, name of courses
            foreach ($receipt as $item) {
                $course_name = '';
                $course_id = '';
                $scheduleText = explode(' ', $item->schedule_id);
                for ($i = 0; $i < sizeof($scheduleText); $i++) {
                    if ($scheduleText[$i] !== '') {
                        $query = DB::table('schedules')
                            ->join('courses', 'courses.id', '=', 'schedules.course_id')
                            ->where('schedules.id', '=', $scheduleText[$i])
                            ->select('courses.name as course_name', 'courses.id as course_id')
                            ->first();
                        $course_id .= $query->course_id;
                        $course_name .= $query->course_name;
                        if ($i < (sizeof($scheduleText) - 2)) {
                            $course_id .= ', ';
                            $course_name .= ', ';
                        }
                    }
                }
                $item->course_id = $course_id;
                $item->course_name = $course_name;
            }
            return view('receipt.receipt', compact('receipt'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $receipt = Receipt::findOrFail($id);
            $cashBook = CashBook::where('cash_code', '=', $receipt->code)->first();
            //Reset membership if receipt saved
            if ($cashBook != null) {
                $receipt2 = new Receipt();
                $receipt2->resetMembershipInClass($id, 'plus');
            }
            $cashBook->delete();
            $receipt->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Review, export and save pdf receipt
    public function reviewReceipt($id, Request $request) {
        try {
            if (isset($request->code)) {
                $receipt = DB::table('receipts')
                    ->join('students', 'students.id','=', 'receipts.student_id')
                    ->where('receipts.code', '=', $request->code)->first();
            }
            else {
                $receipt = DB::table('receipts')
                    ->join('students', 'students.id','=', 'receipts.student_id')
                    ->where('receipts.id', '=', $id)->first();
            }
            $courses = new Collection();
            //Get course in schedule of receipt
            $string = explode(' ', $receipt->schedule_id);
            foreach ($string as $item) {
                if ($item !== '')  {
                    $query = DB::table('schedules')
                        ->join('courses', 'courses.id', '=', 'schedules.course_id')
                        ->where('schedules.id', '=', $item)
                        ->first();
                    $courses->push($query);
                }
            }
            if (isset($request->export)) {
                //Save to cash_book
                $cashBook = CashBook::where("cash_code", "=", $receipt->code)->first() ? CashBook::where("cash_code", "=", $receipt->code)->first() : new CashBook();
                $cashBook->cash_code = $receipt->code;
                $cashBook->save();
                //Reset membership
                if (isset($request->code)) {}
                else {
                    $receipt2 = new Receipt();
                    $receipt2->resetMembershipInClass($id, 'minus');
                }
                //Export pdf
                $pdf = PDF::loadView('receipt.cash_pdf', array('receipt' => $receipt, 'courses' => $courses));
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download('Receipt.pdf');
            }
            else {
                return view('receipt.cash_pdf', compact('receipt'), compact('courses'));
            }
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

    //Show detail
    public function detail($input)
    {
        try {
            $receipt = DB::table('receipts')
                ->join('students', 'students.id', '=', 'receipts.student_id')
                ->where('receipts.id', '=', $input)
                ->select('receipts.*', 'students.name as name', 'students.phone_number as phone_number', 'students.email as email')
                ->paginate(1);
//        dd($receipt);
            foreach ($receipt as $item) {
                $course_name = '';
                $course_id = '';
                $scheduleText = explode(' ', $item->schedule_id);
                for ($i = 0; $i < sizeof($scheduleText); $i++) {
                    if ($scheduleText[$i] !== '') {
                        $query = DB::table('schedules')
                            ->join('courses', 'courses.id', '=', 'schedules.course_id')
                            ->where('schedules.id', '=', $scheduleText[$i])
                            ->select('courses.name as course_name', 'courses.id as course_id')
                            ->first();
                        $course_id .= $query->course_id;
                        $course_name .= $query->course_name;
                        if ($i < (sizeof($scheduleText) - 2)) {
                            $course_id .= ', ';
                            $course_name .= ', ';
                        }
                    }
                }
                $item->course_id = $course_id;
                $item->course_name = $course_name;
            }
            return view('receipt.receipt', compact('receipt'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

    //Find receipt
    public function find(Request $request)
    {
        try {
            $data = DB::table('receipts')
                ->join('students', 'students.id', '=', 'receipts.student_id')
                ->where('receipts.code', 'like', '%' . $request->value . '%')
                ->orWhere('students.name', 'like', '%' . $request->value . '%')
                ->select('receipts.*', 'students.name as name', 'students.phone_number as phone_number', 'students.email as email')
                ->get();
            return response()->json($data);
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.error'));
        }
    }

}
