<?php

namespace App\Http\Controllers\Chart;

use App\Http\Controllers\Controller;
use App\Charts\ReceiptChart;
use App\Models\Book;
use App\Models\Receipt;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use ConsoleTVs\Charts\Facades\Charts;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ChartController extends Controller
{
//    public function index()
//    {
//        $products = Book::where(DB::raw("(DATE_FORMAT(created_at,'%Y'))"), date('Y'))->get();
//        $chart = Charts::database($products, 'bar', 'high charts')
//            ->title('Product Details')
//            ->elementLabel('Total Products')
//            ->dimensions(1000, 500)
//            ->colors(['red', 'green', 'blue', 'yellow', 'orange', 'cyan', 'magenta'])
//            ->groupByMonth(date('Y'), true);
////        return view('charts', ['chart' => $chart]);
//        return view('chart',compact('chart'));
//
//    }

    public function index() {
        return view('chart');
    }

}
