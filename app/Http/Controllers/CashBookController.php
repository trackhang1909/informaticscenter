<?php

namespace App\Http\Controllers;

use App\Charts\ReceiptChart;
use App\Models\CashBook;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CashBookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        try {
            $data = CashBook::paginate(7);
            $result = $this->receiptMonth($request);
            $chart = $result[0];
            $monthFee = $result[1];
            $percent = $result[2];
            $percentText = $result[3];
            return view('cash_book.cash_book', compact('data'), compact('chart'))
                ->with('monthFee', $monthFee)
                ->with('percent', $percent)
                ->with('percentText', $percentText);
        }
        catch (\Exception $exception) {

        }
    }

    public function receiptMonth(Request $request) {
        try {
            $en = CarbonImmutable::now()->locale('vi_VN');
            $cash1 = DB::table('cash_book')
                ->join('receipts', 'receipts.code', '=', 'cash_book.cash_code')
                ->select('receipts.*')
                ->get();
            $cash2 = DB::table('cash_book')
                ->join('receipt_differents', 'receipt_differents.code', '=', 'cash_book.cash_code')
                ->select('receipt_differents.updated_at as upd', 'receipt_differents.total_money as total')
                ->get();

            $cash_book = $cash2->merge($cash1);

            //Get fees in month
            $fees = new Collection();
            for ($i = 1; $i <= 12; $i++) {
                $count = 0;
                foreach ($cash_book as $item) {
                    if (isset($item->register_time)) {
                        if (Carbon::parse(new \DateTime($item->register_time))->year === $en->year && Carbon::parse(new \DateTime($item->register_time))->month === $i) {
                            $count += $item->advance_money;
                        }
                    }
                    if (isset($item->upd)) {
                        if (Carbon::parse(new \DateTime($item->upd))->year === $en->year && Carbon::parse(new \DateTime($item->upd))->month === $i) {
                            $count += $item->total;
                        }
                    }
                }
                $fees->push($count);
            }
            //Create chart
            $chart = new ReceiptChart();
            $chart->labels(['Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12']);
            if ($request->month_bar == true) {
                $chart->dataset('Doanh thu', 'bar', [$fees[0], $fees[1], $fees[2], $fees[3], $fees[4], $fees[5], $fees[6], $fees[7], $fees[8], $fees[9], $fees[10], $fees[11]])
                    ->options([
                        'fill' => 'true',
                        'borderColor' => '#51C1C0',
                    ]);
            }
            else {
                $chart->dataset('Doanh thu', 'line', [$fees[0], $fees[1], $fees[2], $fees[3], $fees[4], $fees[5], $fees[6], $fees[7], $fees[8], $fees[9], $fees[10], $fees[11]])
                    ->options([
                        'fill' => 'true',
                        'borderColor' => '#51C1C0',
                    ]);
            }

            if ($fees[$en->month - 1] == 0) {
                $percent = 100;
            }
            else {
                $percent = abs((float) (($fees[$en->month - 1] - $fees[$en->month - 2])*100)/($fees[$en->month - 1]));
            }

            $result = new Collection();
            $result->push($chart);
            $result->push($fees[$en->month - 1]);
            $result->push($percent);
            if ($fees[$en->month - 1] > $fees[$en->month - 2]) {
                $result->push("increase");
            }
            elseif ($fees[$en->month - 1] < $fees[$en->month - 2]) {
                $result->push("decrease");
            }
            else {
                $result->push("equal");
            }

            return $result;
        }
        catch (\Exception $exception) {

        }
    }

    public function receiptDay(Request $request)
    {
        $en = CarbonImmutable::now()->locale('vi_VN');
        $start = $en->startOfWeek(Carbon::MONDAY);
        $end = $en->endOfWeek(Carbon::SUNDAY);
        $receipt = DB::table('cash_book')
            ->join('receipts', 'receipts.code', '=', 'cash_book.cash_code')
            ->select('receipts.*')
            ->get();
        $allDay = new Collection();
        $fees = new Collection();
        //Get total fee of day
        $day = $start;
        for ($i = 0; $i < 7; $i++) {
            $day = $day;
            $allDay->push($day);
            $count = 0;
            foreach ($receipt as $item) {
                if ((new \DateTime($item->register_time))->format('d/m/Y') === $day->format('d/m/Y')) {
                    $count += $item->advance_money;
                }
            }
            $fees->push($count);
            $day = Carbon::parse(new \DateTime($day.'+1 day'));
        }
        //Create chart
        $chart = new ReceiptChart();
        $chart->labels([$allDay[0]->format('d-m-Y'), $allDay[1]->format('d-m-Y'), $allDay[2]->format('d-m-Y'), $allDay[3]->format('d-m-Y'), $allDay[4]->format('d-m-Y'), $allDay[5]->format('d-m-Y'), $allDay[6]->format('d-m-Y')]);
        if ($request->bar == true) {
            $chart->dataset('Học phí', 'bar', [$fees[0], $fees[1], $fees[2], $fees[3], $fees[4], $fees[5], $fees[6]])
                ->options([
                    'fill' => 'true',
                    'borderColor' => '#51C1C0',
                ]);
        }
        else {
            $chart->dataset('Học phí', 'line', [$fees[0], $fees[1], $fees[2], $fees[3], $fees[4], $fees[5], $fees[6]])
                ->options([
                    'fill' => 'true',
                    'borderColor' => '#51C1C0',
                ]);
        }
        return $chart;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $cashBook = CashBook::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function statistic(Request $request) {
        $en = CarbonImmutable::now()->locale('vi_VN');
        $cash1 = DB::table('cash_book')
            ->join('receipts', 'receipts.code', '=', 'cash_book.cash_code')
            ->select('receipts.*')
            ->get();
        $cash2 = DB::table('cash_book')
            ->join('receipt_differents', 'receipt_differents.code', '=', 'cash_book.cash_code')
            ->select('receipt_differents.updated_at as upd', 'receipt_differents.total_money as total')
            ->get();

        $cash_book = $cash2->merge($cash1);

        //Get fees in month
        $fees = new Collection();
        for ($i = 1; $i <= 12; $i++) {
            $count = 0;
            foreach ($cash_book as $item) {
                if (isset($item->register_time)) {
                    if (Carbon::parse(new \DateTime($item->register_time))->year === $en->year && Carbon::parse(new \DateTime($item->register_time))->month === $i) {
                        $count += $item->advance_money;
                    }
                }
                if (isset($item->upd)) {
                    if (Carbon::parse(new \DateTime($item->upd))->year === $en->year && Carbon::parse(new \DateTime($item->upd))->month === $i) {
                        $count += $item->total;
                    }
                }
            }
            $fees->push($count);
        }

        $data = new Collection();
        foreach ($request->all() as $item) {
            $data->push($item);
        }

        $from = $data[0] - 1;
        $to = $data[1] - 1;

        $total_receipt = 0;
        for ($i = $from; $i <= $to; $i++) {
            $total_receipt += $fees[$i];
        }

        $total_expense = 0;
        $total = $total_receipt - $total_expense;

        return view('cash_book.statistic')
            ->with('total_receipt', $total_receipt)
            ->with('total_expense', $total_expense)
            ->with('total', $total);
    }
}
