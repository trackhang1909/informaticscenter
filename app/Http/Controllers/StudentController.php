<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Course;
use App\Models\Receipt;
use App\Models\Schedule;
use App\Models\Student;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = Student::where('name', 'like', '%'.$_GET['data']. '%')
                ->orderBy('students.name', 'asc')
                ->paginate(7);
            return view('student.student', compact('data'));
        }
        $data = Student::orderBy('students.name', 'asc')->paginate(7);
        return view('student.student', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $student = new Student();
        $check = $request->id ? 1 : 0;

        try {
            $student = Student::updateOrCreate(['id' => $request->id], $request->all());
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success',  __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail',  __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail',  __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $student = Student::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Show student detail
    public function detail($input)
    {
        $data = Student::where('id', '=', $input)->paginate(1);
        return view('student.student', compact('data'));
    }

    //Find student
    public function find(Request $request)
    {
        $data = Student::where('name', 'Like', '%' . $request->value . '%')
            ->orWhere('email', 'Like', '%' . $request->value . '%')
            ->orWhere('phone_number', 'Like', '%' . $request->value . '%')
            ->orderBy('students.name', 'asc')
            ->get();
        return response()->json($data);
    }

    //Register course view
    public function register(Request $request) {
        $request->session()->forget('Cart');
        $students = Student::orderBy('name', 'asc')->get();
        $courses = DB::table('courses')
            ->join('schedules', 'schedules.course_id', '=', 'courses.id')
            ->select('schedules.id as scheId', 'courses.*')
            ->where('schedules.membership', '>', 0)
            ->orderBy('courses.name', 'asc')
            ->get();
        if (isset($request->courses) && isset($request->student_id) && isset($request->receipt_id)) {
            $coursesId = explode(', ', $request->courses);
            foreach ($coursesId as $id) {
                $this->addCart($request, $id);
            }
            $student = Student::find($request->student_id);
            $receipt = Receipt::find($request->receipt_id);
            return view('student.register_course', compact('student'), compact('courses'))->with('receipt', $receipt);
        }

        return view('student.register_course', compact('students'), compact('courses'));
    }

    public function addCart(Request $request, $id) {
        $course = Course::find($id);
        if ($course != null) {
            $oldCart = Session('Cart') ? Session('Cart') : null;
            $newCart = new Cart($oldCart);
            $newCart->addCart($course, $id);
            $request->session()->put('Cart', $newCart);
            return view('student.cart_course', compact('newCart'));
        }
    }

    public function deleteCart(Request $request, $id) {
        $oldCart = Session('Cart') ? Session('Cart') : null;
        $newCart = new Cart($oldCart);

        $newCart->deleteItemCart($id);

        if (Count($newCart->courses) > 0) {
            $request->session()->put('Cart', $newCart);
        }
        else {
            $request->session()->forget('Cart');
        }
        return view('student.cart_course', compact('newCart'));
    }

    //Show student info (register course)
    public function info($id)
    {
        $student = Student::find($id);
        return view('student.student_info', compact('student'));
    }

    //Save info register (receipt)
    public function registerCourse(Request $request, $id) {
        $session = Session('Cart');
        //Set code receipt
        $receipt = new Receipt();
        $codeReceipt = $receipt->createCode();
        //Set reason, money, money text, receipt_id to data collection
        $data = new Collection();
        foreach ($request->all() as $item) {
            $data->push($item);
        }
        //Get schedule from session, set schedule_id (ex: 11 4)
        $scheduleId = '';
        foreach ($session->courses as $item) {
            $string = DB::table('schedules')
                ->where('course_id', '=', $item['courseInfo']->id)
                ->select('schedules.id')
                ->first()->id;
            $scheduleId .= $string.' ';
        }
        try {
//            dd($codeReceipt);
            //Get date now
            $dateNow = Carbon::parse(date(NOW()));
            //Save receipt to database
            $receipt = $data[3] != "undefined" ? Receipt::find($data[3]) : new Receipt();
            $receipt->code = $data[3] != "undefined" ? $receipt->code : $codeReceipt;
            $receipt->register_time = $dateNow;
            $receipt->reason = $data[0];
            $receipt->advance_money = $data[1];
            $receipt->money_text = $data[2];
            $receipt->total_money = $session->totalPrice;
            $receipt->remain_money = $session->totalPrice - $data[1];
            $receipt->student_id = $id;
            $receipt->schedule_id = $scheduleId;
            $receipt->save();
            $request->session()->forget('Cart');
            return redirect(route('receipt.index'))->with('success', __('action.register').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.register').__('action.fail'));
        }
    }

}
