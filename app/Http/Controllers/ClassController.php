<?php

namespace App\Http\Controllers;

use App\Models\ClassModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClassController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = DB::table('classes')
                ->join('campuses', 'code', '=', 'classes.campus_code')
                ->where('classes.name', 'like', '%'.$_GET['data'].'%')
                ->select('classes.*', 'campuses.name as campus_name')
                ->paginate(7);
            $codeCampus = DB::table('campuses')->get();
            return view('class.class', compact('data'))->with('campuses', $codeCampus);
        }
        $data = DB::table('classes')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->select('classes.*', 'campuses.name as campus_name')
            ->orderBy('campus_name', 'asc')
            ->orderBy('classes.name', 'asc')
            ->paginate(7);
        $codeCampus = DB::table('campuses')->get();
        return view('class.class', compact('data'))->with('campuses', $codeCampus);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $check = $request->id ? 1 : 0;
        $msgClass = '';
        $msgCampus = '';

        try {
            if ($request->id) {
                $class = ClassModel::updateOrCreate(['id' => $request->id], $request->all());
            }
            else {
                for ($i = 0; $i < sizeof($request->campus_code); $i++) {
                    $query = ClassModel::where('name', 'like', $request->name)
                        ->where('campus_code', 'like', $request->campus_code[$i])
                        ->first();
                    if ($query != null) {
                        $msgClass = $request->name;
                        $class = new ClassModel();
                        $msgCampus .= $class->getCampusName($request->campus_code[$i]).' - ';
                    }
                    else {
                        $class = new ClassModel();
                        $class->name = $request->name;
                        $class->membership = $request->membership;
                        $class->campus_code = $request->campus_code[$i];
                        $class->save();
                    }
                }
            }
            if ($msgClass != null) {
                return redirect()->back()->with('fail', $msgCampus.__('action.exist').' '.__('class.class2').' '.$msgClass);
            }
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $class = ClassModel::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Show detail
    public function detail($name)
    {
        $data = DB::table('classes')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->where('classes.name', $name)
            ->select('classes.*', 'campuses.name as campus_name')
            ->paginate(7);
        $codeCampus = DB::table('campuses')->get();
        return view('class.class', compact('data'))->with('campuses', $codeCampus);
    }

    //Find with name
    public function find(Request $request)
    {
        $data = ClassModel::where('name', 'like', '%' . $request->value . '%')->get();
        return response()->json($data);
    }
}
