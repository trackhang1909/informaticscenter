<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Campus;
use App\Models\InternShip;
use App\Models\Period;
use App\Models\Position;
use App\Models\Schedule;
use App\Models\Staff;
use App\Models\Student;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UniqueController extends Controller
{
    public function uniqueCourse(Request $request) {

        if (isset($request->course_id)) {
            $course_id = $request->course_id;
            $query = Schedule::where('course_id', $course_id)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }

    public function uniqueStd(Request $request) {

        if (isset($request->phone_number)) {
            $phone_number = $request->phone_number;
            $query = Student::where('phone_number', $phone_number)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

        if (isset($request->email)) {
            $email = $request->email;
            $query = Student::where('email', $email)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }


    public function uniqueCamp(Request $request) {

        if (isset($request->name)) {
            $name = $request->name;
            $query = Campus::where('name', $name)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }

    public function uniquePeriod(Request $request) {

        if (isset($request->name)) {
            $name = $request->name;
            $query = Period::where('name', $name)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }

    public function uniqueStaff(Request $request){
        if (isset($request->phone_number)) {
            $phone_number = $request->phone_number;
            $query = Staff::where('phone_number', $phone_number)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

        if (isset($request->email)) {
            $email = $request->email;
            $query = Staff::where('email', $email)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }


    }
    public function uniqueBook(Request $request) {

    if (isset($request->name)) {
        $name = $request->name;
        $query = Book::where('name', $name)->first();
        //Check if have id
        $id = null;
        if (isset($request->id)) {
            $id = $request->id;
        }
        if ($id != null) {
            if ($query != null && $query->id != $id) {
                //Display error
                echo 'false';
            } else {
                echo 'true';
            }
        }
        else {
            if ($query == null) {
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }

}

    public function uniquePosition(Request $request) {

        if (isset($request->name)) {
            $name = $request->name;
            $query = Position::where('name', $name)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }
    public function uniqueRegister(Request $request){
        if (isset($request->phone)) {
            $phone = $request->phone;
            $query = InternShip::where('phone', $phone)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

        if (isset($request->email)) {
            $email = $request->email;
            $query = InternShip::where('email', $email)->first();
            //Check if have id
            $id = null;
            if (isset($request->id)) {
                $id = $request->id;
            }
            if ($id != null) {
                if ($query != null && $query->id != $id) {
                    //Display error
                    echo 'false';
                } else {
                    echo 'true';
                }
            }
            else {
                if ($query == null) {
                    echo 'true';
                } else {
                    echo 'false';
                }
            }
        }

    }

}
