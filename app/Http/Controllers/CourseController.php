<?php

namespace App\Http\Controllers;

use App\Models\Certification;
use App\Models\Course;
use App\Models\Receipt;
use App\Models\Schedule;
use App\Models\Student;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = Course::where('name', 'like', '%'.$_GET['data'].'%')
                ->orderBy('courses.name', 'asc')
                ->paginate(7);
            return view('course.course', compact('data'));
        }
        $data = DB::table('courses')
            ->orderBy('courses.name', 'asc')
//            ->orderBy('courses.type', 'asc')
            ->paginate(7);
        return view('course.course', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        try {
            $check = $request->id ? 1 : 0;

            $query = $request->id ? Course::where('name', 'like', $request->name)->where('group', 'like', $request->group)->where('id', '<>', $request->id)->first() : Course::where('name', 'like', $request->name)->where('group', 'like', $request->group)->first();

            if ($query != null) {
                return redirect()->back()->with('fail', __('course.course').' '.__('course.out_time'));
            }

            $course = Course::updateOrCreate(['id' => $request->id], $request->all());
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $course = Course::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Show course detail
    public function detail($id)
    {
        $data = Course::where('id', $id)->paginate(1);
        return view('course.course', compact('data'));
    }

    //Find course with name
    public function find(Request $request)
    {
        $data = Course::where('name', 'like', '%' . $request->value . '%')
            ->orderBy('courses.name', 'asc')
            ->get();
        return response()->json($data);
    }

    //Show student list
    public function studentList($id) {
        $studentList = new Collection();
        $receipt = Receipt::get();
        foreach ($receipt as $item) {
            //Explode string schedule id from receipt
            $schedulesString = explode(' ', $item->schedule_id);
            foreach ($schedulesString as $item2) {
                //Check string
                if ($item2 != '') {
                    //Get schedule, check course id
                    $schedule = DB::table('schedules')
                        ->where('id', '=', $item2)
                        ->where('course_id', '=', $id)
                        ->first();
                    if ($schedule != null) {
                        //Add student id to student list
                        $studentList->push($item->student_id);
                        break;
                    }
                }
            }
        }
        //Set data student
        if (sizeof($studentList) > 0) {
            $data = DB::table('students')->where('id', '=', $studentList[0]);
            for ($i = 1; $i < sizeof($studentList); $i++) {
                $data = DB::table('students')->where('id', '=', $studentList[$i])->unionAll($data);
            }
            $data = $data->orderBy('name', 'asc')->paginate(7);
            return view('course.student_list_of_course', compact('data'))->with('id', $id);
        }
        return redirect()->back()->with('fail', __('course.empty_student'));
    }

    //Confirm action
    public function confirm(Request $request) {
        //Student list
        $studentList = new Collection();
        foreach ($request->data['arrChecked'] as $item) {
            $studentList->push(Student::find($item));
        }
        if ($request->data['message'] == 1) {
            return view('course.confirm_certification', compact('studentList'))->with('courseId', $request->data['courseId']);
        }
    }
}
