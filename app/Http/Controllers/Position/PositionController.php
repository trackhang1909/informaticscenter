<?php

namespace App\Http\Controllers\Position;

use App\Http\Controllers\Controller;
use App\Models\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PositionController extends Controller
{
    public function index() {
        if (isset($_GET['data'])) {
            $data = Position::where('name', 'like', '%'.$_GET['data'].'%')->paginate(7);
            return view('position', compact('data'));
        }
        $data = DB::table('positions')->paginate('7');
        return view('position', compact('data'));
    }

    public function store(Request $request) {
        $position = new Position();
        $check = 0;
        if ($request->id) {
            $position = Position::find($request->id);
            $check = 1;
        }

        $position->name = $request->name;
        $position->code = $position->parstCode($request->name);

        try {
//            $position = Position::updateOrCreate(['id'=>$request->id], $request->only('code'));
            $position->save();
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));

        }catch (\Exception $ex){
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    public function destroy(Request $request)
    {
        try {
            $position = Position::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function detail($name)
    {
        $data = DB::table('positions')->where('name', $name)->paginate(7);
        return view('position', compact('data'));
    }

    public function find(Request $request)
    {
        $data = Position::where('name', 'Like', '%' . $request->value . '%')->get();
        return response()->json($data);
    }
}
