<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $permission = Permission::paginate(10);
        $role = Role::paginate(10);
        foreach ($role as $item) {
            $role_permission = DB::table('role_permission')
                ->join('permissions', 'permissions.id', '=', 'role_permission.permission_id')
                ->where('role_permission.role_id', '=', $item->id)
                ->get();
            $item->permission = $role_permission;
        }
//        dd($role[0]->permission[0]);
        return view('role.role', compact('permission'), compact('role'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        if (isset($request->id)) {
            $role_permission = RolePermission::get();
            foreach ($role_permission as $data) {
                if ($data->role_id == $request->id) {
                    $rp = RolePermission::findOrFail($data->id)->delete();
                }
            }
            $role_name = $request->name;
            $role = Role::find($request->id);
            $role->name = $role_name;
            $role->save();
        }
        else {
            $role_name = $request->name;
            $role = new Role();
            $role->name = $role_name;
            $role->save();
        }

        foreach ($request->permission as $permission) {
            $role_permission = new RolePermission();
            $role_permission->role_id = $role->id;
            $role_permission->permission_id = $permission;
            $role_permission->save();
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        $role_permission = RolePermission::get();
        foreach ($role_permission as $data) {
            if ($data->role_id == $id) {
                $rp = RolePermission::findOrFail($data->id)->delete();
            }
        }
        $role = Role::findOrFail($id)->delete();
        return redirect()->back();
    }
}
