<?php

namespace App\Http\Controllers;

use App\Models\Certification;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CertificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = DB::table('certifications')
                ->join('students', 'students.id', '=', 'certifications.student_id')
                ->join('courses', 'courses.id', '=', 'certifications.course_id')
                ->where('certifications.code', 'like', '%' . $_GET['data'] . '%')
                ->orWhere('students.name', 'like', '%' . $_GET['data'] . '%')
                ->orWhere('courses.name', 'like', '%' . $_GET['data'] . '%')
                ->select('certifications.*',
                    'students.name as student_name', 'students.phone_number as phone_number', 'students.email as email',
                    'courses.name as course_name', 'courses.type as type', 'courses.group as group')
                ->paginate(7);
            return view('certification.certification', compact('data'));
        }
        $data = DB::table('certifications')
            ->join('students', 'students.id', '=', 'certifications.student_id')
            ->join('courses', 'courses.id', '=', 'certifications.course_id')
            ->select('certifications.*',
                'students.name as student_name', 'students.phone_number as phone_number', 'students.email as email',
                'courses.name as course_name', 'courses.type as type', 'courses.group as group')
            ->paginate(7);
        return view('certification.certification', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool|\Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $testDate = $request->data['testDate'];
        $rank = $request->data['rank'];
        $point = $request->data['point'];
        $studentId = $request->data['studentId'];
        $courseId = $request->data['courseId'];
        $now = Carbon::now();
        try {
            for($i = 0; $i < sizeof($studentId); $i++) {
                $certification = new Certification();
                $code = $certification->createCode();
                $certification->student_id = $studentId[$i];
                $certification->course_id = $courseId;
                $certification->code = $code;
                $certification->create_date = $now;
                $certification->rank = $rank[$i];
                $certification->point = $point[$i];
                $certification->test_date = $testDate[$i];
                $certification->save();
            }
            return route('certification.index');
        }
        catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $certification = DB::table('certifications')
            ->join('students', 'students.id', '=', 'certifications.student_id')
            ->join('courses', 'courses.id', '=', 'certifications.course_id')
            ->select('certifications.*',
                'courses.name as course_name', 'courses.open_time as open_time', 'courses.close_time as close_time',
                'students.name as student_name', 'students.birthday as birthday',
                'students.address as address')
            ->where('certifications.id', '=', $id)
            ->first();
        return view('certification.certification_preview_main', compact('certification'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit($id, Request $request)
    {
        try {
            $certification = Certification::updateOrCreate(['id' => $id], $request->all());
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $certification = Certification::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Export pdf
    public function export($id) {
        $certification = DB::table('certifications')
            ->join('students', 'students.id', '=', 'certifications.student_id')
            ->join('courses', 'courses.id', '=', 'certifications.course_id')
            ->select('certifications.*',
                'courses.name as course_name', 'courses.open_time as open_time', 'courses.close_time as close_time',
                'students.name as student_name', 'students.birthday as birthday',
                'students.address as address')
            ->where('certifications.id', '=', $id)
            ->first();
        //Export pdf
        $pdf = PDF::loadView('certification.certification_preview', compact('certification'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('Certification.pdf');
    }

    //Show detail
    public function detail($id)
    {
        $data = DB::table('certifications')
            ->join('students', 'students.id', '=', 'certifications.student_id')
            ->join('courses', 'courses.id', '=', 'certifications.course_id')
            ->where('certifications.id', '=', $id)
            ->select('certifications.*',
                'students.name as student_name', 'students.phone_number as phone_number', 'students.email as email',
                'courses.name as course_name', 'courses.type as type', 'courses.group as group')
            ->paginate(1);
        return view('certification.certification', compact('data'));
    }

    //Find certification
    public function find(Request $request)
    {
        $data = DB::table('certifications')
            ->join('students', 'students.id', '=', 'certifications.student_id')
            ->join('courses', 'courses.id', '=', 'certifications.course_id')
            ->where('certifications.code', 'like', '%' . $request->value . '%')
            ->orWhere('students.name', 'like', '%' . $request->value . '%')
            ->orWhere('courses.name', 'like', '%' . $request->value . '%')
            ->select('certifications.*',
                'students.name as student_name', 'students.phone_number as phone_number', 'students.email as email',
                'courses.name as course_name', 'courses.type as type', 'courses.group as group')
            ->get();
        return response()->json($data);
    }
}
