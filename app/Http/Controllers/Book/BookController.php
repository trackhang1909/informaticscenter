<?php

namespace App\Http\Controllers\Book;

use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\TeacherBook;
use App\Models\WareHouseBook;
use Faker\Provider\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class BookController extends Controller
{
    const IMAGE_SERVICE = 'images/book';

    public function index() {
        if (isset($_GET['data'])) {
            $data = Book::where('name', 'like', '%'.$_GET['data'].'%')->paginate('4');
            return view('book', compact('data'));
        }
        $data = DB::table('books')
            ->paginate('8');
        $nv = DB::table('staffs')
            ->where('position','like','%GV%')
            ->get();
        return view('book', compact('data'))->with('nv', $nv);

    }

    public function store(Request $request) {
        $book = new Book();
        $check = 0;
        if ($request->id) {
            $book = Book::find($request->id);
            //Get new book quantity
            if ($request->quantity < $book->quantity) {
                $newQuantity = 0;
            }
            else {
                $newQuantity = $request->quantity - $book->quantity;
            }
            $check = 1;
        }

        try {
            //Add, update book
            $book->name = $request->name;
            $book->author = $request->author;
            $book->price = $request->price;
            $book->quantity = $request->quantity;
            $book->content = $request->content;

            //Kiểm tra xem file có được up load chưa bằng các sử dụng hàm 'hasFile'
            if($request->hasFile('image')){
                $file = $request->file('image');   //lấy file name
                $name = $file->getClientOriginalName(); //lấy tên fila ảnh bằng hàm 'getClientOriginalName
                $destination = self::IMAGE_SERVICE.'/' .$name; //tao biến destination để lưu đường dẫn file
                Storage::disk('public')->put($destination, file_get_contents($file),'public');
                //cái này copy cho nhanh vì đơn giản là tạo không gian chứa là file public
                $book->image = $destination; // này là lưu vào database dưới dạng đường dẫn mình đã tạo ở trên
            }
            $book->save();
            //Add data to book warehouse after add book
            $ware = WareHouseBook::where('book_id', 'like', $book->id)->first();
            if($ware == null) {
                $ware = new WareHouseBook();
                $ware->book_id = $book->id;
                $ware->quantity = $book->quantity;
                $ware->total_money = ($book->price)*($ware->quantity);
                $ware->save();
            }
            else {
                $ware->quantity += $newQuantity;
                $ware->total_money = ($book->price)*($ware->quantity);
                $ware->save();
            }

            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));

        } catch (\Exception $ex){
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    public function destroy(Request $request)
    {
        try {
            $book = Book::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function find(Request $request){
        $data = Book::where('name', 'Like', '%' . $request->value . '%')->get();
        return response()->json($data);
    }

    public function detail(Request $request){
        $data = DB::table('books')->where('name', $request->name)->paginate('4');
        return view('book', compact('data'));
    }

    public function send(Request $request){
        //Lấy ra id sách đã gửi
        $book = Book::where('id', 'like', $request->book_id )->first();
        try {
            for ($i = 0; $i < sizeof($request->teacher_id); $i++) {
                // tạo $tb để lưu id sách đã chọn, gửi id giáo viên đã chọn
                $tb = new TeacherBook();
                //save vào database
                $tb->teacher_id = $request->teacher_id[$i];
                $tb->book_id = $request->book_id;
                $tb->save();
                $book->quantity -= 1;
            }
            if($book->quantity<0)
            {
                return redirect()->back()->with('fail',__('action.runout'));
            }
            $book->save();
            return redirect()->back()->with('success', __('action.sent').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.sent').__('action.fail'));
        }
    }
    public function sell(Request $request){
        $book = Book::where('id', 'like', $request->book_id )->first();
    }


}
