<?php
namespace App\Http\Controllers\Staff;
use App\Http\Controllers\Controller;
use App\Models\Staff;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use vendor\project\StatusTest;

class StaffController extends Controller
{
    public function get() {
        if (isset($_GET['data'])) {
            $data = DB::table('staffs')
                ->join('positions', 'code', 'like','staffs.position')
                ->select('staffs.*','positions.name as positionname')
                ->where('staffs.name', 'like', '%'.$_GET['data'].'%')
                ->paginate(7);
            $pos = DB::table('positions')->get();
            return view('staff.staff',compact('data'))->with('pos',$pos);
        }
        $data = DB::table('staffs')
            ->join('positions', 'code', 'like','staffs.position')
            ->select('staffs.*','positions.name as positionname' )
            ->paginate(7);
        $pos = DB::table('positions')->get();
        return view('staff.staff',compact('data'))->with('pos',$pos);
    }

    public function store(Request $request)
    {
        $staff = new Staff();

        $check = 0;
        if ($request->id) {
            $check = 1;
        }

        try {
            $staff = Staff::updateOrCreate(['id' => $request->id], $request->all());
            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->with('fail', __('action.update').__('action.fail'));
        }
    }

    public function destroy(Request $request)
    {
        try {
            $staff = Staff::findOrFail($request->id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function find(Request $request) {
        $data = Staff::where('name', 'Like', '%' . $request->value . '%')
            ->orWhere('email', 'Like', '%' . $request->value . '%')
            ->orWhere('phone_number', 'Like', '%' . $request->value . '%')
            ->get();
        return response()->json($data);

    }

    public function detail($name) {
        $data = DB::table('staffs')
            ->join('positions', 'code', 'like','staffs.position')
            ->select('staffs.*','positions.name as positionname' )
            ->where('staffs.name', $name)
            ->paginate(7);
        $pos = DB::table('positions')->get();
        return view('staff.staff',compact('data'))
            ->with('pos',$pos);

    }
}
