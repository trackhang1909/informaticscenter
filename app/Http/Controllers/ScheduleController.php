<?php

namespace App\Http\Controllers;

use App\Models\ClassModel;
use App\Models\Course;
use App\Models\Period;
use App\Models\Schedule;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use MaddHatter\LaravelFullcalendar\Facades\Calendar;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if (isset($_GET['data'])) {
            $data = DB::table('schedules')
                ->join('periods', 'periods.id', '=', 'schedules.period_id')
                ->join('courses', 'courses.id', '=', 'schedules.course_id')
                ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
                ->join('classes', 'classes.id', '=', 'schedules.class_id')
                ->join('campuses', 'code', '=', 'classes.campus_code')
                ->orderBy('courses.name', 'asc')
                ->select('schedules.*', 'staffs.name as teacher_name', 'periods.start_time', 'periods.end_time', 'courses.group', 'periods.name as period_name', 'courses.name as course_name', 'classes.name as class_name', 'campuses.name as campus_name')
                ->where('courses.name', 'like', '%'.$_GET['data'].'%')
                ->paginate(7);
//            dd($data);
            $periods = Period::get();
            $classes = DB::table('classes')
                ->join('campuses', 'code', '=', 'classes.campus_code')
                ->orderBy('campus_name', 'asc')
                ->orderBy('classes.name', 'asc')
                ->select('classes.*', 'campuses.name as campus_name')
                ->get();
            $courses = DB::table('courses')->orderBy('courses.name', 'asc')->get();
            $teachers = DB::table('staffs')->where('position', '=', 'GV')->orderBy('staffs.name')->get();
            return view('schedule.schedule', compact('data'))
                ->with('periods', $periods)
                ->with('classes', $classes)
                ->with('courses', $courses)
                ->with('teachers', $teachers);
        }
        $data = DB::table('schedules')
            ->join('periods', 'periods.id', '=', 'schedules.period_id')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
            ->join('classes', 'classes.id', '=', 'schedules.class_id')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->orderBy('courses.name', 'asc')
            ->select('schedules.*', 'staffs.name as teacher_name', 'periods.start_time', 'periods.end_time', 'courses.group', 'periods.name as period_name', 'courses.name as course_name', 'classes.name as class_name', 'campuses.name as campus_name')
            ->paginate(7);
        $periods = Period::get();
        $classes = DB::table('classes')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->orderBy('campus_name', 'asc')
            ->orderBy('classes.name', 'asc')
            ->select('classes.*', 'campuses.name as campus_name')
            ->get();
        $courses = DB::table('courses')->orderBy('courses.name', 'asc')->get();
        $teachers = DB::table('staffs')->where('position', '=', 'GV')->orderBy('staffs.name')->get();
        return view('schedule.schedule', compact('data'))
            ->with('periods', $periods)
            ->with('classes', $classes)
            ->with('courses', $courses)
            ->with('teachers', $teachers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $check = $request->id ? 1 : 0;
        $schedule = $request->id ? Schedule::find($request->id) : new Schedule();

        try {
            //Check schedule unique
            $weekday = '';
            for ($i = 0; $i < sizeof($request->weekday); $i++) {
                //When add
                if ($check == 0) {
                    $dataCourse = Course::find($request->course_id);
                    $dataSchedule = DB::table('schedules')
                        ->join('courses', 'courses.id', '=', 'schedules.course_id')
                        ->where('schedules.weekday', 'like', '%'.$request->weekday[$i].'%')
                        ->where('courses.close_time', '>', $dataCourse->open_time)
                        ->where('schedules.class_id', '=', $request->class_id)
                        ->where('schedules.period_id', '=', $request->period_id)
                        ->get();
                    if ($dataSchedule->isNotEmpty()) {
                        return redirect()->back()->withInput()->with('fail', __('schedule.timetable').' '.__('action.exist'));
                    }
                }
                //When update
                if ($check == 1) {
                    $dataCourse = Course::find($request->course_id);
                    $dataSchedule = DB::table('schedules')
                        ->join('courses', 'courses.id', '=', 'schedules.course_id')
                        ->where('schedules.weekday', 'like', '%'.$request->weekday[$i].'%')
                        ->where('courses.close_time', '>', $dataCourse->open_time)
                        ->where('schedules.class_id', '=', $request->class_id)
                        ->where('schedules.period_id', '=', $request->period_id)
                        ->where('schedules.id', '<>', $request->id)
                        ->get();
                    if ($dataSchedule->isNotEmpty()) {
                        return redirect()->back()->withInput()->with('fail', __('schedule.timetable').' '.__('action.exist'));
                    }
                }
                //Set weekday to db
                $weekday .= $request->weekday[$i].' ';
            }

            $schedule->period_id = $request->period_id;
            $schedule->class_id = $request->class_id;
            $schedule->course_id = $request->course_id;
            $schedule->teacher_id = $request->teacher_id;
            $schedule->weekday = $weekday;
            $schedule->membership = $request->id ? $request->membership : ClassModel::find($request->class_id)->first()->membership;
            $schedule->save();

            if ($check === 0) {
                return redirect()->back()->with('success', __('action.add').__('action.success'));
            }
            return redirect()->back()->with('success', __('action.update').__('action.success'));
        }
        catch (\Exception $ex) {
            if ($check === 0) {
                return redirect()->back()->withInput()->with('fail', __('action.add').__('action.fail'));
            }
            return redirect()->back()->withInput()->with('fail', __('action.update').__('action.fail'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $schedule = DB::table('schedules')
            ->join('periods', 'periods.id', '=', 'schedules.period_id')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->join('classes', 'classes.id', '=', 'schedules.class_id')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
            ->select('schedules.*',
                'classes.name as class_name', 'campuses.name as campus_name',
                'courses.open_time', 'courses.close_time', 'courses.name as course_name', 'courses.group as group',
                'periods.start_time', 'periods.end_time', 'staffs.name as teacher')
            ->where('schedules.id', '=', $id)
            ->first();
        //Event detail
        $event_detail = $schedule->course_name.' ('.__('course.group').' '.$schedule->group.') - '
            .__('class.class3').' '.$schedule->class_name.' ('.$schedule->campus_name.') - '
            .__('course.teacher').' '.$schedule->teacher;
        //Get open time
        $open = Carbon::parse($schedule->open_time);
        //Get close time
        $close = Carbon::parse($schedule->close_time);
        //Get total day in course
        $total_day =  $open->diffInDays($close, false);
        //Get weekday
        $weekday = explode(' ', $schedule->weekday);
        $event_list = [];

        //Set event
        for ($i = 0; $i < $total_day; $i++) {
            $background_colors = array('#f29566', '#64f5eb', '#5ff571', '#5f8ff5', '#f75973', '#eaf255',
                '#51eda9', '#5286f7', '#7b6afc', '#ae65f7', '#bcb6c2', '#eda687', '#f5dc8c');
            $rand_background = $background_colors[array_rand($background_colors)];
            for ($j = 0; $j < (sizeof($weekday) - 1); $j++) {
                if ($open->dayOfWeek == $weekday[$j]) {
                    $event_list[] = Calendar::event(
                        $schedule->course_name,
                        false,
                        $open->format('Y-m-d').$schedule->start_time,
                        $open->format('Y-m-d').$schedule->end_time,
                        $i,
                        [
                            'color' => '#57facc',
                        ]
                    );
                    break;
                }
            }
            $open = Carbon::parse(new \DateTime($open.'+1 day'));
        }

        $calendar_details = Calendar::addEvents($event_list);
        return view('schedule.schedule_main', compact('calendar_details'), compact('event_detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
        try {
            $schedule = Schedule::findOrFail($id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    //Show detail
    public function detail($id)
    {
        $data = DB::table('schedules')
            ->join('periods', 'periods.id', '=', 'schedules.period_id')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
            ->join('classes', 'classes.id', '=', 'schedules.class_id')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->orderBy('courses.name', 'asc')
            ->select('schedules.*', 'staffs.name as teacher_name', 'periods.start_time', 'periods.end_time', 'courses.group', 'periods.name as period_name', 'courses.name as course_name', 'classes.name as class_name', 'campuses.name as campus_name')
            ->where('schedules.id', '=', $id)
            ->paginate(1);
        $periods = Period::get();
        $classes = DB::table('classes')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->orderBy('campus_name', 'asc')
            ->orderBy('classes.name', 'asc')
            ->select('classes.*', 'campuses.name as campus_name')
            ->get();
        $courses = DB::table('courses')->orderBy('courses.name', 'asc')->get();
        $teachers = DB::table('staffs')->where('position', '=', 'GV')->orderBy('staffs.name')->get();
        return view('schedule.schedule', compact('data'))
            ->with('periods', $periods)
            ->with('classes', $classes)
            ->with('courses', $courses)
            ->with('teachers', $teachers);
    }

    //Find
    public function find(Request $request)
    {
        $data = DB::table('schedules')
            ->join('courses', 'courses.id', '=', 'schedules.course_id')
            ->select('courses.name as course_name', 'courses.group as group', 'schedules.*')
            ->where('courses.name', 'like', '%' . $request->value . '%')
            ->get();
//        dd($data);
        return response()->json($data);
    }

    //General view
    public function general() {
        $classes = DB::table('classes')
            ->join('campuses', 'code', '=', 'classes.campus_code')
            ->select('classes.*', 'campuses.name as campus_name')
            ->orderBy('campus_name', 'asc')
            ->orderBy('classes.name', 'asc')
            ->get();
        $teachers = DB::table('staffs')
            ->where('staffs.position', '=', 'GV')
            ->orderBy('staffs.name', 'asc')->get();
        return view('schedule.general_schedule', compact('classes'), compact('teachers'));
    }

    public function generalClass($id) {
        $totalSchedule = Schedule::where('class_id', '=', $id)->get();
//        dd(sizeof($schedule));
        $event_list = [];
        foreach ($totalSchedule as $item) {
            $schedule = DB::table('schedules')
                ->join('periods', 'periods.id', '=', 'schedules.period_id')
                ->join('courses', 'courses.id', '=', 'schedules.course_id')
                ->join('classes', 'classes.id', '=', 'schedules.class_id')
                ->join('campuses', 'code', '=', 'classes.campus_code')
                ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
                ->select('schedules.*',
                    'classes.name as class_name', 'campuses.name as campus_name',
                    'courses.open_time', 'courses.close_time', 'courses.name as course_name', 'courses.group as group',
                    'periods.start_time', 'periods.end_time', 'staffs.name as teacher')
                ->where('schedules.id', '=', $item->id)
                ->first();
            //Event detail
            $event_detail = __('class.class3').' '.$schedule->class_name.' ('.$schedule->campus_name.')';
            //Get open time
            $open = Carbon::parse($schedule->open_time);
            //Get close time
            $close = Carbon::parse($schedule->close_time);
            //Get total day in course
            $total_day =  $open->diffInDays($close, false);
            //Get weekday
            $weekday = explode(' ', $schedule->weekday);

            //Set event
            for ($i = 0; $i < $total_day; $i++) {
//                $background_colors = array('#f29566', '#64f5eb', '#5ff571', '#5f8ff5', '#f75973');
//                $rand_background = $background_colors[array_rand($background_colors)];
                for ($j = 0; $j < (sizeof($weekday) - 1); $j++) {
                    if ($open->dayOfWeek == $weekday[$j]) {
                        $event_list[] = Calendar::event(
                            $schedule->course_name.' ('.__('course.group').' '.$schedule->group.')
                            '.$schedule->teacher,
                            false,
                            $open->format('Y-m-d').$schedule->start_time,
                            $open->format('Y-m-d').$schedule->end_time,
                            $i,
                            [
                                'color' => '#57facc',
                            ]
                        );
                        break;
                    }
                }
                $open = Carbon::parse(new \DateTime($open.'+1 day'));
            }
        }
        $calendar_details = Calendar::addEvents($event_list);
        return view('schedule.schedule_main', compact('calendar_details'), compact('event_detail'));
    }

    public function generalTeacher($id) {
        $totalSchedule = Schedule::where('teacher_id', '=', $id)->get();
//        dd(sizeof($schedule));
        $event_list = [];
        foreach ($totalSchedule as $item) {
            $schedule = DB::table('schedules')
                ->join('periods', 'periods.id', '=', 'schedules.period_id')
                ->join('courses', 'courses.id', '=', 'schedules.course_id')
                ->join('classes', 'classes.id', '=', 'schedules.class_id')
                ->join('campuses', 'code', '=', 'classes.campus_code')
                ->join('staffs', 'staffs.id', '=', 'schedules.teacher_id')
                ->select('schedules.*',
                    'classes.name as class_name', 'campuses.name as campus_name',
                    'courses.open_time', 'courses.close_time', 'courses.name as course_name', 'courses.group as group',
                    'periods.start_time', 'periods.end_time', 'staffs.name as teacher')
                ->where('schedules.id', '=', $item->id)
                ->first();
            //Event detail
            $event_detail = __('course.teacher').' '.$schedule->teacher;
            //Get open time
            $open = Carbon::parse($schedule->open_time);
            //Get close time
            $close = Carbon::parse($schedule->close_time);
            //Get total day in course
            $total_day =  $open->diffInDays($close, false);
            //Get weekday
            $weekday = explode(' ', $schedule->weekday);

            //Set event
            for ($i = 0; $i < $total_day; $i++) {
//                $background_colors = array('#f29566', '#64f5eb', '#5ff571', '#5f8ff5', '#f75973');
//                $rand_background = $background_colors[array_rand($background_colors)];
                for ($j = 0; $j < (sizeof($weekday) - 1); $j++) {
                    if ($open->dayOfWeek == $weekday[$j]) {
                        $event_list[] = Calendar::event(
                            $schedule->course_name.' ('.__('course.group').' '.$schedule->group.')
                            '.__('class.class3').' '.$schedule->class_name.' ('.$schedule->campus_name.')',
                            false,
                            $open->format('Y-m-d').$schedule->start_time,
                            $open->format('Y-m-d').$schedule->end_time,
                            $i,
                            [
                                'color' => '#57facc',
                            ]
                        );
                        break;
                    }
                }
                $open = Carbon::parse(new \DateTime($open.'+1 day'));
            }
        }
        $calendar_details = Calendar::addEvents($event_list);
        return view('schedule.schedule_main', compact('calendar_details'), compact('event_detail'));
    }


}
