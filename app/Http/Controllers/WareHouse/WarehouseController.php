<?php

namespace App\Http\Controllers\WareHouse;

use App\Http\Controllers\Controller;
use App\Models\WareHouseBook;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class WarehouseController extends Controller
{
    public function index(){
        $data = DB::table('warehouse_books')
            ->join('books', 'books.id','like', 'warehouse_books.book_id')
            ->where('warehouse_books.deleted_at','=', null)
            ->select('warehouse_books.*', 'books.name as nameofbook')
            ->paginate('4');
        return view('warehouse_book', compact('data'));
    }

    public function createPDF()
    {
        $data = DB::table('warehouse_books')
            ->where('warehouse_books.deleted_at','=', null)
            ->paginate('8');
        view()->share('warehouse-book',$data);
        $pdf = PDF::loadView('pdf_view', compact('data'));
        return $pdf->download('bill.pdf');
    }

    public function destroy(Request $request){
        $warehouse = new WareHouseBook();
        try {
            $warehouse = WareHouseBook::find($request->id)->delete();
            return redirect()->back()->with('success', __('action.delete').__('action.success'));
        }
        catch (\Exception $exception) {
            return redirect()->back()->with('fail', __('action.delete').__('action.fail'));
        }
    }

    public function trash(){
//        $data = WareHouseBook::onlyTrashed()->paginate('4');
        $data = DB::table('warehouse_books')
            ->join('books', 'books.id','like', 'warehouse_books.book_id')
            ->where('warehouse_books.deleted_at','<>', null)
            ->select('warehouse_books.*', 'books.name as nameofbook')
            ->paginate('4');
        return view('product_trash', compact('data'));
    }

    public function ultras($id){

        $data2 = WareHouseBook::withTrashed()->find($id);
        $data2->restore();

        $data = DB::table('warehouse_books')
            ->join('books', 'books.id','like', 'warehouse_books.book_id')
            ->where('warehouse_books.deleted_at','=', null)
            ->select('warehouse_books.*', 'books.name as nameofbook')
            ->paginate('4');
        return redirect()->back()->with('success','Khôi phục thành công');


//        return view('/warehouse_book',compact('data'));
    }

}
