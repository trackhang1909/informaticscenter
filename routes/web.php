<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');

//Staff Form
Route::namespace('Staff')->group(function (){
    Route::get('/staff','StaffController@get')->name('staff.index');
    Route::post('/staff/add','StaffController@store');
    Route::get('/staff/delete','StaffController@destroy');
    Route::get('/staff/find','StaffController@find');
    Route::get('/staff/{name}','StaffController@detail')->name('staff.detail');
});
//Position Form
Route::namespace('Position')->group(function (){
    Route::get('/position','PositionController@index')->name('position.index');
    Route::post('/position/add','PositionController@store');
    Route::get('/position/delete','PositionController@destroy');
    Route::get('/position/find','PositionController@find');
    Route::get('/position/{name}','PositionController@detail')->name('position.detail');
});
//Warehousebook Form
Route::namespace('WareHouse')->group(function () {
    Route::get('/warehouse-book', 'WarehouseController@index')->name('warehouse-book.index');
    Route::get('/warehouse-book/pdf', 'WarehouseController@createPDF')->name('warehouse.pdf');
    Route::get('/warehouse-book/delete','WarehouseController@destroy')->name('warehouse.del');
    Route::get('/warehouse-book/trash','WarehouseController@trash')->name('product_trash');
    Route::get('/product-trash/{id}','WarehouseController@ultras')->name('product_ultras');
    Route::get('/product-trash/force','WarehouseController@force')->name('product_force');
});
//Book Form
Route::namespace('Book')->group(function (){
    Route::get('/book','BookController@index')->name('book.index');
    Route::post('book/add','BookController@store');
    Route::get('/book/delete','BookController@destroy');
    Route::get('/warehouse_book','BookController@warehouse');
    Route::get('/book/find','BookController@find');
    Route::get('/book/{name}','BookController@detail')->name('book.detail');
    Route::post('book/send','BookController@send')->name('book.send');

});
//Customer Form
Route::namespace('Customer')->group(function (){
    Route::get('/customer-book','CustomerController@sell')->name('customer-book.index');
    Route::post('sell/add','CustomerController@store')->name('sell.activate');
    Route::get('/register-book','CustomerController@register')->name('register-book.index');
    Route::get('/price','InternShipController@price')->name('internship.price');


});

//Chart Form
Route::namespace('Chart')->group(function () {
    Route::get('/chart', 'ChartController@index')->name('chart.index');
});


//Teacherbook Form
Route::namespace('TeacherBook')->group(function (){
    Route::get('/teacher-book','TeacherBookController@index')->name('teacher-book.index');
});

//Topic Form
Route::namespace('Topic')->group(function (){
    Route::get('/topic','TopicController@index')->name('topic.index');
    Route::post('/topic/add','TopicController@store')->name('topic.add');
    Route::get('/topic/delete','TopicController@destroy')->name('topic.delete');
    Route::get('/topic/find','TopicController@find')->name('topic.find');
    Route::get('/topic/{name}','TopicController@detail')->name('topic.detail');
});




//Internship and Register Internship Form
Route::namespace('Internship')->group(function (){
    Route::get('/internship','InternShipController@index')->name('internship.index');
    Route::post('/internship/add','InternShipController@store')->name('internship.add');
    Route::get('/intern/delete','InternShipController@destroy')->name('internship.delete');
    Route::get('/internship/complete','InternShipController@trash')->name('internship.complete');
    Route::get('/complete/{id}','InternShipController@unfinished')->name('internship.unfinished');
    Route::get('/intern','InternShipController@show')->name('intern.index');
    Route::get('/ajax','InternShipController@ajax')->name('internship.ajax');
    Route::get('/filter1','InternShipController@filter1')->name('filter1.index');
    Route::get('/filter2','InternShipController@filter2')->name('filter2.index');
    Route::get('/filter3','InternShipController@filter3')->name('filter3.index');
    Route::get('/filter4','InternShipController@filter4')->name('filter4.index');
    Route::get('/filter4','InternShipController@filter4')->name('filter4.index');

});


//Course form
Route::resource('/course', 'CourseController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/course/find', 'CourseController@find');
Route::get('/course/{id}', 'CourseController@detail')->name('course.detail');
Route::get('/course/student-list/{id}', 'CourseController@studentList')->name('course.student-list');
Route::post('/course/student-list/confirm/action', 'CourseController@confirm')->name('course.confirm');

//Certification
Route::resource('/certification', 'CertificationController', ['only' => ['index', 'store', 'destroy', 'show', 'edit']]);
Route::get('/certification/export/{id}', 'CertificationController@export')->name('certification.export');
Route::get('/certification/find/{value}', 'CertificationController@find');
Route::get('/certification/detail/{id}', 'CertificationController@detail')->name('certification.detail');

//Student form
Route::resource('/student', 'StudentController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/student/find', 'StudentController@find');
Route::get('/student/{id}', 'StudentController@detail')->name('student.detail');
//Register course
Route::get('/student/register-course/index', 'StudentController@register')->name('student.register-index');
Route::get('/student/register-course/{id}', 'StudentController@registerCourse')->name('student.register-course');
Route::get('/student/register-course/info/{id}', 'StudentController@info')->name('student.info');
Route::get('/student/register-course/add-cart/{id}', 'StudentController@addCart')->name('student.add-cart');
Route::get('/student/register-course/delete-cart/{id}', 'StudentController@deleteCart');

//Receipt
Route::resource('/receipt', 'ReceiptController', ['only' => ['index', 'destroy']]);
Route::get('/receipt/review/{id}', 'ReceiptController@reviewReceipt')->name('receipt.review');
Route::get('/receipt/find', 'ReceiptController@find');
Route::get('/receipt/{id}', 'ReceiptController@detail')->name('receipt.detail');

//Receipt different form
Route::resource('/receipt-different', 'ReceiptDifferentController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/receipt-different/review/{id}', 'ReceiptDifferentController@reviewReceipt')->name('receipt-different.review');
Route::get('/receipt-different/find', 'ReceiptDifferentController@find');
Route::get('/receipt-different/{id}', 'ReceiptDifferentController@detail')->name('receipt-different.detail');

//Cash book
Route::resource('/cash-book', 'CashBookController', ['only' => ['index', 'destroy']]);
Route::get('/cash-book/statistic', 'CashBookController@statistic')->name('cash-book.statistic');

//Class form
Route::resource('/class', 'ClassController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/class/find', 'ClassController@find');
Route::get('/class/{name}', 'ClassController@detail')->name('class.detail');

//Campus form
Route::resource('/campus', 'CampusController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/campus/find', 'CampusController@find');
Route::get('/campus/{name}', 'CampusController@detail')->name('campus.detail');
Route::get('/campus/class-list/{code}', 'CampusController@classList')->name('campus.class-list');

//Check unique
Route::get('/unique-std', 'UniqueController@uniqueStd');
Route::get('/unique-camp', 'UniqueController@uniqueCamp');
Route::get('/unique-period', 'UniqueController@uniquePeriod');
Route::get('/unique-course', 'UniqueController@uniqueCourse');
Route::get('/unique-staff', 'UniqueController@uniqueStaff');
Route::get('/unique-book', 'UniqueController@uniqueBook');
Route::get('/unique-position', 'UniqueController@uniquePosition');
Route::get('/unique-register', 'UniqueController@uniqueRegister');

//Period form
Route::resource('/period', 'PeriodController', ['only' => ['index', 'store', 'destroy']]);
Route::get('/period/find', 'PeriodController@find');
Route::get('/period/{name}', 'PeriodController@detail')->name('period.detail');

//Schedule form
Route::resource('/schedule', 'ScheduleController', ['only' => ['index', 'store', 'destroy', 'show']]);
Route::get('/schedule/find/{value}', 'ScheduleController@find');
Route::get('/schedule/detail/{id}', 'ScheduleController@detail')->name('schedule.detail');
Route::get('/schedule/general/index', 'ScheduleController@general')->name('schedule.general');
Route::get('/schedule/general/class/{id}', 'ScheduleController@generalClass')->name('schedule.class');
Route::get('/schedule/general/teacher/{id}', 'ScheduleController@generalTeacher')->name('schedule.teacher');

Auth::routes();

//Role
Route::resource('/role', 'RoleController', ['only' => ['index', 'store', 'destroy']]);

//User
Route::resource('/user', 'UserController', ['only' => ['index', 'store', 'destroy']]);
