<?php
return [
    'code' => 'Mã phiếu',
    'register_time' => 'Thời gian đăng ký',
    'advance_money' => 'Số tiến đã nộp',
    'total_money' => 'Tổng học phí',
    'remain_money' => 'Số tiền còn lại',
    'reason' => 'Lý do nộp',
    'money_text' => 'Số tiền (viết bằng chữ)',
    'empty' => 'Không có phiếu thu này',
    'name' => 'Họ tên người nộp tiền',
    'add' => 'Tạo phiếu thu',
    'info' => 'Thông tin phiếu thu',
    'create' => 'Tạo',
    'address' => 'Địa chỉ',
    'total_money2' => 'Tổng tiền',
    'list' => 'Danh sách phiếu thu',
];
