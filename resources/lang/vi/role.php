<?php
return [
    'name' => 'Tên',
    'permission' => 'Quyền',
    'role' => 'Vai trò',
    'description' => 'Mô tả',
    'no_permission' => 'Không có quyền',
    'choose_permission' => 'Chọn quyền',
    'list' => 'Danh sách vai trò',
];

