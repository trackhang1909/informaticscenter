<?php
return [
    'receipt' => 'Phiếu thu',
    'expense' => 'Phiếu chi',
    'list' => 'Danh sách sổ quỹ',
    'fee' => 'Thống kê học phí',
    'diff' => 'Thống kê chi phí khác',
    'month_chart' => 'Biểu đồ tháng',
    'line' => 'Dòng',
    'bar' => 'Cột',
    'statistic' => 'Thống kê',
    'detail' => 'Chi tiết doanh thu',
    'export' => 'Xuất PDF',
];

