<?php
return [
    'manager_course' => 'Quản lý khóa học',
    'manager_class' => 'Quản lý phòng học',
    'manager_staff' => 'Quản lý nhân viên',
    'manager_book' => 'Quản lý giáo trình',
    'manager_student' => 'Quản lý học viên',
    'setting' => 'Cài đặt',
    'user' => 'Quản lý tài khoản',
    'role' => 'Phân quyền',
    'cash_book' => 'Quản lý sổ quỹ',
];
