<?php
return [
    'student' => 'Học viên',
    'course' => 'Khóa học',
    'code' => 'Mã chứng nhận',
    'certification' => 'Chứng nhận',
    'test_date' => 'Ngày kiểm tra',
    'rank' => 'Xếp loại',
    'point' => 'Điểm',
    'empty' => 'Không có chứng nhận này',
    'list' => 'Danh sách chứng nhận',
];
