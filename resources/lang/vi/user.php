<?php
return [
    'account' => 'Tài khoản',
    'name' => 'Tên tài khoản',
    'email' => 'Email',
    'role' => 'Quyền',
    'list' => 'Danh sách tài khoản',
    'register' => 'Đăng ký tài khoản',
    'logout' => 'Đăng xuất',
];
