<?php
return[
//    NhanVIen
    'staff'=>'Staff Management',
    'stafflist'=>'Staff list',
    'addstaff'=>'Add Staff',
    'number'=>'Number',
    'names'=>'Names',
    'name'=>'Name',
    'email'=>'Email',
    'phonenumber'=>'Phone Number',
    'position'=>'Position',
    'detail'=>'Detail',
    'update'=>'Update',
    'delete'=>'Delete',
    'birthday' => 'Day of birth',
    'country' =>'Country',
    'datestarwork'=> 'Date Start Work',
    'salary'=>'Salary',
    'confirm'=>'Are you want to delete this ?',
    'yes'=>'Yes',
    'no'=>'No',
    'add'=>'Add',
    'info'=>'Information',
    'code'=>'Code',
    'price'=>'Price',
    'quantity'=>'Quantity',


//    giaotrinh
    'book'=>'Book Management',

];
