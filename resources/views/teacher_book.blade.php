@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('content')
    <div class="content">
        <div class="container-fluid">

            <div class="clearfix header-kh" style="margin-bottom: -36px;">
                <h3> {{__('msg.teacherbook')}}</h3>
                <form action="{{ route('teacher-book.index') }}">
                    <button class=" btn btn-outline-success"
                            type="submit"style="margin-left: 120px; float: left;
                                margin-top: -38px">Giáo Viên</button>
                </form>
                <form action="{{ route('customer-book.index') }}">
                    <button class="btn btn-outline-info" style="margin-bottom: 50px;
                      margin-top: -38px; margin-left: 215px" type="submit">Khách hàng</button>
                </form>
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>
            <div class="panel-heading">
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col" style="text-align: center">{{__('msg.number')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.teacher')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.bookname')}}</th>

                            <th id=""></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $teacher_book)
                            <tr>
                                <th scope="row" style="text-align: center">{{ $data->firstItem()+ $item }}</th>
                                <td style="text-align: center">{{ $teacher_book->staffname}}</td>
                                <td style="text-align: center">{{ $teacher_book->namebook }}</td>
                                <td style="text-align: center">

                                    <button type="button" style="width: 50%" class="btn btn-xs btn-outline-info" data-toggle="modal"
                                            data-target="#exampleModalXem{{ $teacher_book->id }}">
                                        {{__('msg.detail')}}
                                    </button>

                                    <div class="modal fade" id="exampleModalXem{{ $teacher_book->id }}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"
                                                        style="text-align: center">{{__('msg.info')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
{{--                                    <form class="form-container" action="/warehouse/xoa" method="GET">--}}
{{--                                        <div class="modal fade" id="exampleModalXoa{{ $teacher_book->id }}" tabindex="-1"--}}
{{--                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                            <div class="modal-dialog" role="document">--}}
{{--                                                <div class="modal-content">--}}
{{--                                                    <div class="modal-header">--}}

{{--                                                        <button type="button" class="close" data-dismiss="modal"--}}
{{--                                                                aria-label="Close">--}}
{{--                                                            <span aria-hidden="true">&times;</span>--}}
{{--                                                        </button>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="modal-body">--}}
{{--                                                        @csrf--}}
{{--                                                        <div class="form-group">--}}
{{--                                                            <input style="display: none" type="text" name="id"--}}
{{--                                                                   class="form-control" id="exampleInputStt"--}}
{{--                                                                   value="{{ $teacher_book->id }}">--}}
{{--                                                        </div>--}}
{{--                                                        <p>{{__('msg.confirm')}}</p>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="modal-footer">--}}
{{--                                                        <button type="button" class="btn btn-secondary"--}}
{{--                                                                data-dismiss="modal">{{__('msg.no')}}</button>--}}
{{--                                                        <button type="submit"--}}
{{--                                                                class="btn btn-primary">{{__('msg.yes')}}</button>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </form>--}}
                                    {{--                                    <div class="modal fade" id="exampleModalXem{{ $warehouse->id }}" tabindex="-1" role="dialog"--}}
                                    {{--                                         aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
                                    {{--                                        <div class="modal-dialog" role="document">--}}
                                    {{--                                            <div class="modal-content">--}}
                                    {{--                                                <div class="modal-header">--}}
                                    {{--                                                    <h5 class="modal-title" id="exampleModalLabel"--}}
                                    {{--                                                        style="text-align: center">{{__('msg.info')}}</h5>--}}
                                    {{--                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
                                    {{--                                                        <span aria-hidden="true">&times;</span>--}}
                                    {{--                                                    </button>--}}
                                    {{--                                                </div>--}}
                                    {{--                                                <div class="modal-body ">--}}
                                    {{--                                                    <h5><i>{{__('msg.author')}} :</i><b> {{ $warehouse->book_id }}</b></h5>--}}
                                    {{--                                                </div>--}}
                                    {{--                                                <div class="modal-body ">--}}
                                    {{--                                                    <h5><i>{{__('msg.quantity')}} :</i><b> {{ $warehouse->quantity }}</b>--}}
                                    {{--                                                    </h5>--}}
                                    {{--                                                </div>--}}
                                    {{--                                                <div class="modal-body ">--}}
                                    {{--                                                    <h5><i>{{__('msg.quantity')}} :</i><b> {{ $warehouse->total_money }}</b>--}}
                                    {{--                                                    </h5>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                    {{--                                    <form class="form-container form-add" action="/warehouse/them" method="POST">--}}
                                    {{--                                        <div class="modal fade" id="exampleModalCn{{ $warehouse->id }}" tabindex="-1"--}}
                                    {{--                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
                                    {{--                                            <div class="modal-dialog" role="document">--}}
                                    {{--                                                <div class="modal-content">--}}
                                    {{--                                                    <div class="modal-header">--}}
                                    {{--                                                        <h5 class="modal-title"--}}
                                    {{--                                                            id="exampleModalLabel">{{__('msg.update')}}</h5>--}}

                                    {{--                                                        <button type="button" class="close" data-dismiss="modal"--}}
                                    {{--                                                                aria-label="Close">--}}
                                    {{--                                                            <span aria-hidden="true">&times;</span>--}}
                                    {{--                                                        </button>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="modal-body">--}}
                                    {{--                                                        @csrf--}}
                                    {{--                                                        <div class="form-group">--}}
                                    {{--                                                            <label for="exampleInputStt">{{__('msg.number')}}</label>--}}
                                    {{--                                                            <input type="number" name="id" class="form-control"--}}
                                    {{--                                                                   id="exampleInputStt" value="{{ $warehouse->id }}" readonly>--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                        <div class="form-group">--}}
                                    {{--                                                            <label for="exampleInputStt">{{__('msg.name')}}</label>--}}
                                    {{--                                                            <input type="text" name="book_id" class="form-control"--}}
                                    {{--                                                                   id="exampleInputStt" value="{{ $warehouse->book_id }}">--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                        <div class="form-group">--}}
                                    {{--                                                            <label for="exampleInputStt">{{__('msg.name')}}</label>--}}
                                    {{--                                                            <input type="text" name="quantity" class="form-control"--}}
                                    {{--                                                                   id="exampleInputStt" value="{{ $warehouse->quantity }}">--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                        <div class="form-group">--}}
                                    {{--                                                            <label for="exampleInputStt">{{__('msg.name')}}</label>--}}
                                    {{--                                                            <input type="text" name="total_money" class="form-control"--}}
                                    {{--                                                                   id="exampleInputStt" value="{{ $warehouse->total_money }}">--}}
                                    {{--                                                        </div>--}}
                                    {{--                                                        --}}{{--                                                        <div class="form-group">--}}
                                    {{--                                                        --}}{{--                                                            <label for="exampleInput">{{__('msg.code')}}</label>--}}
                                    {{--                                                        --}}{{--                                                            <input type="text" name="code" class="form-control"--}}
                                    {{--                                                        --}}{{--                                                                   id="exampleInput" value="{{ $position->code }}">--}}
                                    {{--                                                        --}}{{--                                                        </div>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                    <div class="modal-footer">--}}
                                    {{--                                                        <button type="button" class="btn btn-secondary"--}}
                                    {{--                                                                data-dismiss="modal">{{__('msg.no')}}</button>--}}
                                    {{--                                                        <button type="submit"--}}
                                    {{--                                                                class="btn btn-primary">{{__('msg.update')}}</button>--}}
                                    {{--                                                    </div>--}}
                                    {{--                                                </div>--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}

                                    {{--                                    </form>--}}
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="page" style="float: right">
                {{$data->links()}}
            </div>
        </div>
    </div>

@stop
