@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%", no_results_text: "{{ __('class.empty_campus2') }}" });
    </script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('class.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">
                @can('create')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('class.add_class')}}
                    </button>
                @elsecan('superAdmin')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('class.add_class')}}
                    </button>
                @endcan

                <form class="form-container form-add" action="{{ route('class.store') }}" method="POST">
                    <div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('class.class')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput1">{{__('class.name')}} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInput1" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput" generated="true">{{__('class.campus')}} <label class="required-p">*</label></label>
                                        <select class="form-control chosen-select" id="exampleInput" name="campus_code[]" multiple data-placeholder="{{ __('class.select_campus') }}">
                                            @foreach($campuses as $campus)
                                                <option value="{{ $campus->code }}">{{ $campus->name }}</option>
                                            @endforeach
                                        </select>
                                        <label for="exampleInput" generated="true" class="error"></label>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{__('class.membership')}} <label class="required-p">*</label></label>
                                        <input type="number" name="membership" class="form-control" id="exampleInput2" placeholder="" >
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('action.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif

            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{__('class.name')}}</th>
                                <th scope="col">{{__('class.campus')}}</th>
                                <th scope="col">{{__('class.membership')}}</th>
                                @can('update')
                                    <th></th>
                                @elsecan('superAdmin')
                                    <th></th>
                                @elsecan('delete')
                                    <th></th>
                                @endcan
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item => $class)
                                <tr>
                                    <th scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td>{{ $class->name }}</td>
                                    <td>{{ $class->campus_name }}</td>
                                    <td>{{ $class->membership }}</td>
                                    @if(\Illuminate\Support\Facades\Auth::user()->can('create') || \Illuminate\Support\Facades\Auth::user()->can('superAdmin') || \Illuminate\Support\Facades\Auth::user()->can('delete'))
                                        <td>
                                            @can('update')
                                                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $class->id }}">
                                                    {{__('action.update')}}
                                                </button>
                                            @elsecan('superAdmin')
                                                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $class->id }}">
                                                    {{__('action.update')}}
                                                </button>
                                            @endcan

                                            @can('delete')
                                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $class->id }}">
                                                    {{__('action.delete')}}
                                                </button>
                                            @elsecan('superAdmin')
                                                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $class->id }}">
                                                    {{__('action.delete')}}
                                                </button>
                                            @endcan

                                            <form class="form-container" action="{{ route('class.destroy', $class->id) }}" method="POST">
                                                <input name="_method" type="hidden" value="DELETE">
                                                <div class="modal fade" id="exampleModalDelete{{ $class->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">{{__('class.class')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $class->id }}">
                                                                </div>
                                                                <p>{{__('action.confirm_delete')}}</p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                                <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                            <!-- Cap nhat -->
                                            <form class="was-validated form-container form-up" action="{{ route('class.store') }}" method="POST">
                                                <div class="modal fade" id="exampleModalUpdate{{ $class->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <h5 class="modal-title" id="exampleModalLabel">{{__('class.class')}}</h5>
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>

                                                            <div class="modal-body">
                                                                @csrf
                                                                <div class="form-group">
                                                                    <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $class->id }}" readonly>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInputPassword1">{{__('class.name')}} <label class="required-p">*</label></label>
                                                                    <input type="text" name="name" class="form-control" id="exampleInputPassword1" value="{{ $class->name }}" >
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput">{{__('class.campus')}} <label class="required-p">*</label></label>
                                                                    <select class="form-control" id="exampleInput" name="campus_code">
                                                                        <option value="{{ $class->campus_code }}">{{ $class->campus_name }}</option>
                                                                        @foreach($campuses as $campus)
                                                                            <option value="{{ $campus->code }}">{{ $campus->name }}</option>
                                                                        @endforeach
                                                                    </select>
                                                                    <div class="valid-feedback">
                                                                        Looks good!
                                                                    </div>
                                                                    <div class="invalid-feedback">
                                                                        Please choose
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleInput2">{{__('class.membership')}} <label class="required-p">*</label></label>
                                                                    <input type="number" name="membership" class="form-control" id="exampleInput2" value="{{ $class->membership }}" >
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                                <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {
            $("form.form-add").each(function() {
                $(this).validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true,
                        },
                        'campus_code[]': {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("class.name").__("action.required") ?>",
                        },
                        'campus_code[]': {
                            required: "<?php echo __("class.campus").__("action.required") ?>",
                        },
                    },
                });
            });

            $("form.form-up").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        campus_code: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("class.name").__("action.required") ?>",
                        },
                        campus_code: {
                            required: "<?php echo __("class.campus").__("action.required") ?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/class/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'class-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('class.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("class.detail", ":name") }}';
                            url = url.replace(':name', data.name);
                            return '<a href="' + url + '" class="list-group-item">' + data.name + '</a>';                        }
                    }
                },
            ]);

        });
    </script>

@stop
