
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Informatics Center</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<body>
    <div class="content">
        <div class="panel-heading">
            <div class="panel-body table-responsive" >
                <table class="table table-bordered table-striped">
                    <thead style="color: #0e84b5">
                    <tr>
                        <th scope="col" style="text-align: center">Number</th>
                        <th scope="col" style="text-align: center">Book name</th>
                        <th scope="col" style="text-align: center">Quantity</th>
                        <th scope="col" style="text-align: center">Total</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $total = 0;
                        foreach ($data as $item => $wareh) {
                            echo "<tr id='row'>";
                                echo "<td>".($data->firstItem() + $item)."</td>";
                                echo "<td>".$wareh->book_id."</td>";
                                echo "<td>".$wareh->quantity."</td>";
                                echo "<td id='total1'>".$wareh->total_money."</td>";
                                $total += $wareh->total_money;
                            echo "</tr";
                        }
                    ?>
{{--                        @foreach($data as $item => $wareh)--}}
{{--                            <tr id="row">--}}
{{--                                <td>{{ $data->firstItem() + $item }}</td>--}}
{{--                                <td>{{ $wareh->book_id }}</td>s--}}
{{--                                <td>{{ $wareh->quantity }}</td>--}}
{{--                                <td id="total1">{{ $wareh->total_money }}</td>--}}
{{--                            </tr>--}}
{{--                        @endforeach--}}
                        <tr>
                                <td style="background-color: white; float: right" colspan="3">Total</td>
                                <td style="background-color: white" id="total"><?php echo $total  ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

        <script type="text/javascript">
            var tong = document.getElementById('total1');
            var tongb = document.getElementById('total');
            var cart_row = document.getElementById('row')[0];

            function tinhtong() {
                var tongb =0;
                for (var i =0; i<cart_row.length; i++){
                    tongb = tong+=cart_row[i];
                }
                document.getElementById('total')[0].innerHTML = tongb
            }
        </script>
</body>
