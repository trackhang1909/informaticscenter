@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('content')
    <div class="content" id="contentt">
        <div class="container-fluid">
            <div class="clearfix header-kh">
                <h3><i class="fa fa-user icon" aria-hidden="true"></i>  {{__('msg.warehouse')}}</h3>
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
{{--                <button type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalThem">--}}
{{--                    {{__('msg.addposition')}}--}}
{{--                </button>--}}

                <form class="form-container form-add" action="/warehouse/them" method="POST">
                    <div class="modal fade" id="exampleModalThem" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('msg.addposition')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{__('msg.posname')}}</label>
                                        <input type="text" name="name" class="form-control" id="exampleInputPassword1"
                                               placeholder="">
                                    </div>
                                    {{--                                    <div class="form-group">--}}
                                    {{--                                        <label for="exampleInput">{{__('msg.code')}}</label>--}}
                                    {{--                                        <input type="text" name="code" class="form-control" id="exampleInput"--}}
                                    {{--                                               placeholder="">--}}
                                    {{--                                    </div>--}}
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{__('msg.no')}}</button>
                                    <button type="submit" class="btn btn-primary"
                                            onclick="save()">{{__('msg.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>
            {{--        <a href="/staff/them" class="btn btn-primary them" role="button">Thêm nhân viên</a>--}}
            {{--         <h2><i class="fa fa-list-alt" aria-hidden="true"></i> Danh sách nhân viên</h2>--}}

            {{--         <div class="panel panel-default">--}}
            {{--             <div class="panel-heading"></div>--}}
            {{--             <div class="panel panel-default">--}}
            <div class="panel-heading" >
                <div class="panel-body table-responsive" >
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th scope="col" style="text-align: center">{{__('msg.number')}}</th>
                                <th scope="col" style="text-align: center">{{__('msg.bookname')}}</th>
                                <th scope="col" style="text-align: center">{{__('msg.quantity')}}</th>
                                <th scope="col" style="text-align: center">Thời gian nhập</th>
                                <th scope="col" style="text-align: center">{{__('msg.total')}}</th>
                                <th id=""></th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $warehouse)
                            <tr>
                                <th scope="row" style="text-align: center">{{ $data->firstItem()+ $item }}</th>
                                <td style="text-align: center">{{ $warehouse->nameofbook}}</td>
                                <td style="text-align: center">{{ $warehouse->quantity }}</td>
                                <td style="text-align: center">{{ $warehouse->created_at }}</td>
                                <td style="text-align: center">{{ $warehouse->total_money }}</td>
                                <td style="text-align: center;">
                                    <button type="button" style="width: 50%" class="btn btn-xs btn-danger" data-toggle="modal"
                                            data-target="#exampleModalXoa{{ $warehouse->id }}">
                                        {{__('msg.delete')}}
                                    </button>
                                    <button type="button" style="width: 50%" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#exampleModalXoa{{ $warehouse->id }}">
                                        In hóa đơn
                                    </button>
                                    <form class="form-container" action="{{route('warehouse.del')}}" method="GET">
                                        <div class="modal fade" id="exampleModalXoa{{ $warehouse->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="text" name="id"
                                                                   class="form-control" id="exampleInputStt"
                                                                   value="{{ $warehouse->id }}">
                                                        </div>
                                                        <p>{{__('msg.confirm')}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.yes')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="page" style="float: right">
                {{$data->links()}}
            </div>
            <a class="btn btn-primary" href="{{ route('warehouse.pdf') }}"><i class="fa fa-file-pdf" aria-hidden="true"></i></a>
{{--            <a class="fa fa-trash" href="{{ route('product_trash') }}"></a>--}}
{{--            <a href="{{route('product_trash')}}" class="btn-outline-danger">Thung rac</a>--}}
            <a class="btn btn-danger" href="{{ route('product_trash') }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
{{--            <div>--}}
{{--                <button class="btn btn-outline-secondary">--}}
{{--                    <a href="/book">{{__('msg.back')}}</a>--}}
{{--                </button>--}}
{{--            </div>--}}
{{--            <div class="right footer">--}}
{{--                <button class="btn-dark" style="width:10%; text-align: center" onclick="printDiv()">--}}
{{--                    Print--}}
{{--                </button>--}}
{{--            </div>--}}
        </div>
    </div>
{{--    <script src="{{ asset('//code.jquery.com/jquery-1.11.3.min.js') }}}"></script>--}}
{{--    <script src="{{asset('jquery.printThis.js')}}"></script>--}}
{{--    <script>{{asset('jQuery.print.js') }}</script>--}}
{{--    <script type="text/javascript">--}}
{{--        function printDiv() {--}}
{{--            var divContents = document.getElementById("dein").innerHTML;--}}
{{--            var a = window.open('', '', 'height=500, width=500');--}}
{{--            // a.document.write('<html>');--}}
{{--            a.document.write('<body > <h1>Phiếu chi kho sách <br>');--}}
{{--            a.document.write(divContents);--}}
{{--            // a.document.write('</body></html>');--}}
{{--            a.document.close();--}}
{{--            a.print();--}}
{{--        }--}}

{{--    </script>--}}
    <script type="text/javascript">
        $(document).ready(function() {
            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        birthday: {
                            required: true,
                        },
                        email: {
                            required: true,
                        },
                        phone_number: {
                            required: true,
                            maxlength: 11,
                        },
                        country: {
                            required: true,
                        },
                        start_time: {
                            required: true,
                        },
                        salary: {
                            required: true,
                        },
                        position: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("msg.name").__("action.required") ?>",
                        },
                        birthday: {
                            required: "<?php echo __("msg.birthday").__("action.required") ?>",
                        },
                        email: {
                            required:"<?php echo __("msg.email").__("action.required") ?>"
                        },
                        phone_number: {
                            required: "<?php echo __("msg.phonenumber").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.phonenumber").' '.__("action.maxlength11") ?>",
                        },
                        country: {
                            required: "<?php echo __("msg.country").__("action.required")?>",
                        },
                        start_time:{
                            required: "<?php echo __("msg.datestarwork").__("action.required") ?>",
                        },
                        salary: {
                            required: "<?php echo __("msg.salary").__("action.required")?>",
                        },
                        position: {
                            required: "<?php echo __("msg.position").__("action.required")?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/student/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'student-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('course.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            return '<a href="student/' + data.id + '" class="list-group-item">' + data.name + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
