@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%" });
    </script>
@endsection
@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Đã hoàn thành thực tập</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{route('intern.index')}}">Quay lại</a></li>
                        <li class="breadcrumb-item active">Đã hoàn thành</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh">
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif

                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>
            <div class="grid-container" style="text-align: center">
                @foreach($data as $internship)
                    <div class="card" style="width: 14rem; border-radius: 15px; background-color:#E8E8E8">
                        <div class="card-img-top" style="background-image: url({{ asset('storage/'. $internship->image) }});"></div>
                        <div class="card-body">
                            <div class="status" style=""><a href="#" style="color: white">Complete</a></div>
                            <img src="https://thienquantech.com/wp-content/uploads/2020/02/logo-hitech-thien-quan.png"
                                 style="width: 150px">
                            <div>
                                <p>{{''}}</p>
                            </div>
                            <p class="card-text" style="font-family: Verdana; color: #1F2D3D">
                                {{ $internship->name }}</p>
                            <div style="margin: 14px 0;">
                                <hr style="background-color: orange; width: 30px">
                            </div>
                            <p class="card-text" style=" color: #1F2D3D">{{ $internship->topicname }}</p>
                            <button type="button" style="border-radius: 5px; background-color: orange;" class="btn" data-toggle="modal"
                                    data-target="#exampleModalXem1{{ $internship->id }}">
                                Thông tin cá nhân
                            </button>
                            <div style="margin: 14px 0;">
                                <hr style="background-color: orange; width: 30px">
                            </div>
                            <button type="button" style="border-radius: 5px; background-color: forestgreen;" class="btn" data-toggle="modal"
                                    data-target="#exampleModalXem1{{ $internship->id }}">
                                Thông tin thực tập
                            </button>
                            <div style="margin: 14px 0;">
                                <hr style="background-color: orange; width: 30px">
                            </div>
                            <a class="btn btn-xs btn-success" style="width: 40%" href="{{ route('internship.unfinished', $internship->id) }}">
                                <i class="fas fa-sync fa-spin"></i>

                            </a>
                                <form class="form-container" action="{{ route('internship.delete', $internship->id) }}" method="GET">
                                <div class="modal fade" id="exampleModalXoa{{ $internship->id }}" tabindex="-1"
                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('msg.book')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group">
                                                    <input style="display: none" type="text" name="id"
                                                           class="form-control" id="exampleInputStt"
                                                           value="{{ $internship->id }}">
                                                </div>
                                                <p style="text-align: left">{{__('action.confirm_delete')}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{__('msg.no')}}</button>
                                                <button type="submit"
                                                        class="btn btn-primary">{{__('msg.yes')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="modal fade" id="exampleModalXem1{{ $internship->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: darkgrey;">
                                            <h5 class="modal-title" id="exampleModalLabel">Thông tin sinh viên</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body clearfix">
                                            <strong style="text-decoration: underline;margin-left: 73px;
                                            font-style: oblique;letter-spacing: 3px;">
                                                Trung tâm tin học Thiên Quân
                                            </strong>
                                            <img src="{{asset( 'storage/'. $internship->image )}}"
                                                 style="width: 100px; margin-right:388px; border: #00b44e">
                                            <p style=" font-family: Onyx; width: 118px;
                                            margin-left: 213px;margin-top: -100px;font-size: 45px; letter-spacing: 1px">
                                                Thông tin
                                            </p>
                                            <code style="font-size: 67.5%;">
                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                Tuyệt đối bảo mật thông tin cho sinh viên trung tâm
                                            </code>
                                            <div style="float: left; width: 20%;margin-top: 57px;">
                                                <p>Email:</p>
                                                <p>Phone:</p>
                                                <p>Số CMND:</p>
                                                <p>Ngày cấp:</p>
                                                <p>Nơi cấp:</p>
                                                <p>Địa chỉ:</p>
                                                <p>Tên trường:</p>
                                                <p>Trình độ:</p>
                                            </div>
                                            <div style="float: right; width: 75%;margin-top: 31px;">
                                                <p>{{ $internship->email }}</p>
                                                <p>{{ $internship->phone }}</p>
                                                <p>{{ $internship->cmnd }}</p>
                                                <p>{{ $internship->cmndday }}</p>
                                                <p>{{ $internship->cmndplace }}</p>
                                                <p>{{ $internship->address }}</p>
                                                <p>{{ $internship->university }}</p>
                                                <p>{{ $internship->level }}</p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button style="margin-right: 215px; background-color: darkgrey" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                @endforeach
            </div>
            <div class="clearfix">
                <div class="page" style="float: right" >
                    {{ $data->links() }}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>

@stop
