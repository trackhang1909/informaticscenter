@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('student.register_course') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div id="loading" style="display:none">
                <img src="{{ asset('img/ajax-loading.gif') }}" alt="Loading..."/>
            </div>

            <ul class="list-group">
                <li class="list-group-item list-group-item-primary">{{ __('student.information') }}</li>
                <li class="list-group-item">
                    @if(!isset($receipt))
                        <select class="form-control" id="mySelect">
                            <option value="">{{ __('student.select') }}</option>
                            @foreach($students as $student)
                                <option value="{{ $student->id }}">{{ $student->name }} ({{ $student->phone_number }})</option>
                            @endforeach
                        </select>
                        <div id="student-info" class="clearfix"></div>
                    @else
                        <select style="display: none" class="form-control" id="mySelect">
                            <option value="{{ $student->id }}" selected></option>
                        </select>
                        <input style="display: none" id="receipt-id" value="{{ $receipt->id }}">
                        <div id="student-info" class="clearfix">
                            @include('student.student_info')
                        </div>
                    @endif
                </li>
            </ul>

            <div class="clearfix" style="margin-top: 40px; margin-bottom: 40px">
                <ul class="list-group" style="float:left; width: 48%">
                    <li class="list-group-item list-group-item-primary">{{ __('course.course') }}</li>
                    <li class="list-group-item">
                        @if (Session::has("Cart") != null)
                            @foreach($courses as $course)
                                @foreach(Session::get("Cart")->courses as $item)
                                    <?php $count = 0; ?>
                                    @if($course->id === $item['courseInfo']->id)
                                        <div id="{{ $course->id }}" style="display: none">
                                            <div style="float: right">
                                                <a class="btn btn-xs btn-outline-dark" href="{{ route('schedule.show', $course->scheId) }}" role="button">{{ __('schedule.detail') }}</a>
                                                <button type="button" class="btn btn-xs btn-outline-info" data-toggle="modal" data-target="#exampleModalDetail{{ $course->id }}">
                                                    {{__('action.detail')}}
                                                </button>
                                                <a class="btn btn-xs btn-outline-success" onclick="addCart({{ $course->id }})" href="javascript:" role="button">{{ __('action.add') }}</a>
                                            </div>
                                            <p>{{ $course->name }} ({{ __('course.group') }}: {{ $course->group }})</p>
                                        </div>
                                        <?php $count++; ?>
                                        @break
                                    @endif
                                @endforeach
                                <?php
                                if ($count == 0) {
                                ?>
                                <div id="{{ $course->id }}">
                                    <div style="float: right">
                                        <a class="btn btn-xs btn-outline-dark" href="{{ route('schedule.show', $course->scheId) }}" role="button">{{ __('schedule.detail') }}</a>
                                        <button type="button" class="btn btn-xs btn-outline-info" data-toggle="modal" data-target="#exampleModalDetail{{ $course->id }}">
                                            {{__('action.detail')}}
                                        </button>
                                        <a class="btn btn-xs btn-outline-success" onclick="addCart({{ $course->id }})" href="javascript:" role="button">{{ __('action.add') }}</a>
                                    </div>
                                    <p>{{ $course->name }} ({{ __('course.group') }}: {{ $course->group }})</p>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="modal fade" id="exampleModalDetail{{ $course->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-content">
                                                <div class="form-group">
                                                    <p>{{__('course.name')}}: {{ $course->name }}</p>
                                                    <p>{{__('course.group')}}: {{ $course->group }}</p>
                                                    <p>{{__('course.fees')}}: {{ number_format($course->fees,0,',','.') }}</p>
                                                    <p>{{__('course.time')}}: {{ date('d-m-Y', strtotime($course->open_time)) }}</p>
                                                    <p>{{__('course.close_time')}}: {{ date('d-m-Y', strtotime($course->close_time)) }}</p>
                                                    <p>{{__('course.content')}}:</p>
                                                    <textarea class="form-control summary-ckeditor" id="summary-ckeditor" name="content">
                                                        {{ $course->content }}
                                                    </textarea>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            @foreach($courses as $course)
                                <div id="{{ $course->id }}">
                                    <div style="float: right">
                                        <a class="btn btn-xs btn-outline-dark" href="{{ route('schedule.show', $course->scheId) }}" role="button">{{ __('schedule.detail') }}</a>
                                        <button type="button" class="btn btn-xs btn-outline-info" data-toggle="modal" data-target="#exampleModalDetail{{ $course->id }}">
                                            {{__('action.detail')}}
                                        </button>
                                        <a class="btn btn-xs btn-outline-success" onclick="addCart({{ $course->id }})" href="javascript:" role="button">{{ __('action.add') }}</a>
                                    </div>
                                    <div class="modal fade" id="exampleModalDetail{{ $course->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-content">
                                                    <div class="form-group">
                                                        <p>{{__('course.name')}}: {{ $course->name }}</p>
                                                        <p>{{__('course.group')}}: {{ $course->group }}</p>
                                                        <p>{{__('course.fees')}}: {{ number_format($course->fees,0,',','.') }}</p>
                                                        <p>{{__('course.time')}}: {{ date('d-m-Y', strtotime($course->open_time)) }}</p>
                                                        <p>{{__('course.close_time')}}: {{ date('d-m-Y', strtotime($course->close_time)) }}</p>
                                                        <p>{{__('course.content')}}:</p>
                                                        <textarea class="form-control summary-ckeditor" id="summary-ckeditor" name="content">
                                                            {{ $course->content }}
                                                        </textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <p>{{ $course->name }} ({{ __('course.group') }}: {{ $course->group }}) </p>
                                </div>
                            @endforeach
                        @endif
                    </li>
                </ul>
                <ul class="list-group" style="float: right; width: 48%">
                    <li class="list-group-item list-group-item-primary">
                        {{ __('student.info_regis') }}
                        @if(session('fail'))
                            <div style="padding: 0px 5px 0px 5px; font-size: 15px" class="alert alert-danger abc" role="alert">
                                {{ session('fail') }}
                            </div>
                        @endif
                    </li>
                    <li class="list-group-item">
                        <div id="change-item-card">
                            @if (Session::has("Cart") != null)
                                @foreach(Session::get("Cart")->courses as $item)
                                    <button type="button" class="close" aria-label="Close" data-id="{{ $item['courseInfo']->id }}">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <p id="check-course">{{ __('course.name') }}: {{ $item['courseInfo']->name }} - {{ __('course.fees') }}: {{ number_format($item['fees'],0,',','.').' '.__('student.currency_unit') }}</p>
                                    <hr>
                                @endforeach
                                <div class="clearfix">
                                    <p style="float: left">{{ __('student.total_price') }}: {{ number_format(Session::get("Cart")->totalPrice,0,',','.').' '.__('student.currency_unit') }} - {{ __('student.total_quantity') }}: {{ Session::get("Cart")->totalQuantity }}</p>
                                    <button style="float: right" type="submit" class="register-course btn btn-sm btn-info">{{ __('action.register') }}</button>
                                </div>
                            @endif
                        </div>
                    </li>
                </ul>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script>

        function addCart(id) {
            $.ajax({
                url: "/student/register-course/add-cart/" + id,
                type: "GET",
            }).done(function (response) {
                $("#change-item-card").empty();
                $("#change-item-card").html(response);
                alertify.success('Thêm thành công');
            });
            $("#" + id).hide();
        }

        $('#mySelect').change(function() {
            $.ajax({
                url: "/student/register-course/info/" + $(this).val(),
                type: "GET",
            }).done(function (response) {
                $("#student-info").empty();
                $("#student-info").html(response);
                alertify.success('Thêm thành công');
            });
        });

        $("#change-item-card").on("click", ".close", function() {
            $.ajax({
                url: "/student/register-course/delete-cart/" + $(this).data("id"),
                type: "GET",
            }).done(function (response) {
                $("#change-item-card").empty();
                $("#change-item-card").html(response);
                alertify.error('Xóa thành công');
            });
            $("#" + $(this).data("id")).show();
        });

        $("#change-item-card").on("click", ".register-course", function () {
            var reason = $("#reason").val();
            var money = $("#money").val();
            var money_text = $("#money_text").val();
            var receipt_id = $("#receipt-id").val();
            // console.log(receipt_id, money);
            if ($("#mySelect option:selected").val() > 0 & $("#check-course").text() !== "" & reason !== "" & money !== "" & money_text !== "") {
                var url = '{{ route("student.register-course", ":input").'?reason=:reason&money=:money&money_text=:money-text&receipt_id=:receipt' }}';
                url = url.replace(':input', $("#mySelect option:selected").val());
                url = url.replace(':reason', reason);
                url = url.replace(':money', money);
                url = url.replace(':receipt', receipt_id);
                url = url.replace(':money-text', money_text);
                location.assign(url);
            }
            else {
                alertify.error('Hãy điền đầy đủ thông tin');
            }
        })

        $(document).ajaxStart(function() {
            $("#loading").show();
        });

        $(document).ajaxStop(function() {
            $("#loading").hide();
        });

    </script>

@stop
