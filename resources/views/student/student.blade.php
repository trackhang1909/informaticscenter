@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('student.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">

                @can('superAdmin')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('student.add_student')}}
                    </button>
                @elsecan('create')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('student.add_student')}}
                    </button>
                @elsecan('staff')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('student.add_student')}}
                    </button>
                @endcan

                <form class="form-container form-add" action="{{ route('student.store') }}" method="POST">
                    <input style="display: none" name="user_id" value="{{ \Illuminate\Support\Facades\Auth::user()->id }}">
                    <div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('student.student')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput3">{{__('student.name')}} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInput3" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput">{{__('student.phone')}} <label class="required-p">*</label></label>
                                        <input type="number" name="phone_number" class="form-control" id="exampleInput" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1">{{__('student.email')}} <label class="required-p">*</label></label>
                                        <input type="email" name="email" class="form-control" id="exampleInput1" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{__('student.date')}} <label class="required-p">*</label></label>
                                        <input type="date" name="birthday" class="form-control" id="exampleInput2" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{__('student.address')}} <label class="required-p">*</label></label>
                                        <input type="text" name="address" class="form-control" id="exampleInput4" placeholder="" >
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('action.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th style="text-align: center"><input type="checkbox" id="check-all"></th>
                                    <th scope="col" style="text-align: center">{{__('action.id')}}</th>
                                    <th scope="col">{{__('student.name')}}</th>
                                    <th scope="col">{{__('student.date')}}</th>
                                    <th></th>
                                </tr>
                            </thead>

                            <tbody>
                            @foreach ($data as $item => $student)
                                <tr>
                                    <td class="align-middle" style="text-align: center"><input type="checkbox" class="btn-check"></td>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td class="align-middle" id="index">
                                        <p>{{ $student->name }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $student->phone_number.' - '.$student->email }}</p>
                                    </td>
                                    <td class="align-middle">{{ date('d/m/Y', strtotime($student->birthday)) }}</td>
                                    <td class="align-middle">
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModalDetail{{ $student->id }}">
                                            {{__('action.detail')}}
                                        </button>

                                        @can('superAdmin')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $student->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @elsecan('update', $student)
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $student->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @elsecan('update')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $student->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @endcan

                                        @can('superAdmin')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $student->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @elsecan('delete')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $student->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @endcan

                                        <div class="modal fade" id="exampleModalDetail{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('student.student') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body clearfix">
                                                        <div style="float: left; width: 20%">
                                                            <p>{{__('student.phone')}}:</p>
                                                            <p>{{__('student.email')}}:</p>
                                                            <p>{{__('student.address')}}:</p>
                                                        </div>
                                                        <div style="float: right; width: 75%">
                                                            <p>{{ $student->phone_number }}</p>
                                                            <p>{{ $student->email }}</p>
                                                            <p>{{ $student->address }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form class="form-container" action="{{ route('student.destroy', $student->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('student.student')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $student->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form class="form-container form-add" action="{{ route('student.store') }}" method="POST">
                                            <div class="modal fade" id="exampleModalUpdate{{ $student->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('student.student')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="id-std form-control" id="exampleInputStt" value="{{ $student->id }}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput0">{{__('student.name')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="name" class="form-control" id="exampleInput0" value="{{ $student->name }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput">{{__('student.phone')}} <label class="required-p">*</label></label>
                                                                <input type="number" name="phone_number" class="form-control" id="exampleInput" value="{{ $student->phone_number }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput1">{{__('student.email')}} <label class="required-p">*</label></label>
                                                                <input type="email" name="email" class="form-control" id="exampleInput1" value="{{ $student->email }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput2">{{__('student.date')}} <label class="required-p">*</label></label>
                                                                <input type="date" name="birthday" class="form-control" id="exampleInput2" value="{{ date("Y-m-d", strtotime($student->birthday)) }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput3">{{__('student.address')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="address" class="form-control" id="exampleInput3" value="{{ $student->address }}" >
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        email: {
                            required: true,
                            remote: "/unique-std?id=" + $(this).find('.id-std').first().val(),
                        },
                        phone_number: {
                            required: true,
                            maxlength: 11,
                            remote: "/unique-std?id=" + $(this).find('.id-std').first().val(),
                        },
                        birthday: {
                            required: true,
                        },
                        address: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("student.name").__("action.required") ?>",
                        },
                        email: {
                            required: "<?php echo __("student.email").__("action.required") ?>",
                            remote: "<?php echo __("student.email").' '.__("action.exist") ?>"
                        },
                        phone_number: {
                            required: "<?php echo __("student.phone").__("action.required") ?>",
                            maxlength: "<?php echo __("student.phone").' '.__("action.maxlength11") ?>",
                            remote: "<?php echo __("student.phone").' '.__("action.exist") ?>",
                        },
                        birthday: {
                            required: "<?php echo __("student.date").__("action.required") ?>",
                        },
                        address: {
                            required: "<?php echo __("student.address").__("action.required") ?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/student/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'student-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('student.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{!! route("student.detail", ":input") !!}';
                            url = url.replace(':input', data.id);
                            return '<a style="width: 250px" href="' + url + '" class="list-group-item">' + data.name + ' (' + data.phone_number + ')' + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
