@if ($newCart != null)

    @foreach($newCart->courses as $item)
        <button type="button" class="close" aria-label="Close" data-id="{{ $item['courseInfo']->id }}">
            <span aria-hidden="true">&times;</span>
        </button>
        <p id="check-course">{{ __('course.name') }}: {{ $item['courseInfo']->name }} - {{ __('course.fees') }}: {{ number_format($item['fees'],0,',','.').' '.__('student.currency_unit') }}</p>
        <hr>
    @endforeach

    <div class="clearfix">
        <p style="float: left">{{ __('student.total_price') }}: {{ number_format($newCart->totalPrice,0,',','.').' '.__('student.currency_unit') }} - {{ __('student.total_quantity') }}: {{ $newCart->totalQuantity }}</p>
        <button style="float: right" type="submit" class="register-course btn btn-sm btn-info">{{ __('action.register') }}</button>
    </div>
@endif


