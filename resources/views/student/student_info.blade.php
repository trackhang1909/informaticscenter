<br>
<div class="clearfix" style="float: left; width: 50%">
    <div style="float: left; width: 30%">
        <p>{{__('student.name')}}:</p>
        <p>{{__('student.phone')}}:</p>
        <p>{{__('student.email')}}:</p>
        <p>{{__('student.date')}}:</p>
        <p>{{__('student.address')}}:</p>
    </div>
    <div style="float: right; width: 70%">
        <p>{{ $student->name }}</p>
        <p>{{ $student->phone_number }}</p>
        <p>{{ $student->email }}</p>
        <p>{{ date('d/m/Y', strtotime($student->birthday)) }}</p>
        <p>{{ $student->address }}</p>
    </div>
</div>
<div style="float: right; width: 50%">
    <p style="text-align: center"><b>{{ __('student.info_receipt') }}</b></p>
    @if(isset($receipt))
        <input style="width: 100%; margin-bottom: 2%" type="text" placeholder="Lý do nộp" id="reason" value="{{ $receipt->reason }}"><br>
        <input style="width: 100%; margin-bottom: 2%" type="number" placeholder="Số tiền" id="money" value="{{ $receipt->advance_money }}"><br>
        <input style="width: 100%" type="text" placeholder="Số tiền (viết bằng chữ)" id="money_text" value="{{ $receipt->money_text }}">
    @else
        <input style="width: 100%; margin-bottom: 2%" type="text" placeholder="Lý do nộp" id="reason"><br>
        <input style="width: 100%; margin-bottom: 2%" type="number" placeholder="Số tiền" id="money"><br>
        <input style="width: 100%" type="text" placeholder="Số tiền (viết bằng chữ)" id="money_text">
    @endif
</div>

