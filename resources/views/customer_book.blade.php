@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh" style="margin-bottom: -36px;">
                <h3> {{__('msg.teacherbook')}}</h3>
                <form action="{{ route('teacher-book.index') }}">
                    <button class=" btn btn-outline-success"
                            type="submit"style="margin-left: 120px; float: left;
                                margin-top: -38px">Giáo Viên</button>
                </form>
                <form action="{{ route('customer-book.index') }}">
                    <button class="btn btn-outline-info" style="margin-bottom: 50px;
                      margin-top: -38px; margin-left: 215px" type="submit">Khách hàng</button>
                </form>
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>
            <div class="panel-heading">
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col" style="text-align: center; width: 10px">{{__('msg.number')}}</th>
                            <th scope="col" style="">Tên khách hàng</th>
                            <th scope="col" >{{__('msg.bookname')}}</th>
                            <th scope="col" style="text-align: center;" >Số lượng</th>
                            <th scope="col" >Hình ảnh</th>
                            <th id=""></th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $customer)
                            <tr>
                                <th scope="row" style=" width: 5px; text-align: center" class="align-middle">{{ $data->firstItem()+ $item }}</th>
                                <td style=" width: 200px" class="align-middle">{{ $customer->name}}
                                    <p style="color: grey; font-size: 13px">{{ '(+84)'.' '.$customer->phone.' - '.$customer->address }}</p>

                                </td>
                                <td style=" width: 150px" class="align-middle">{{ $customer->bookname }}
                                    <p style="color: grey; font-size: 13px">{{'$:'. $customer->bookprice.' - '.$customer->bookauthor   }}</p>
                                </td>
                                <td style=" width: 70px; text-align: center" class="align-middle">{{ $customer->quantity}}</td>
                                <td style=" width: 100px" class="align-middle">
{{--                                    <button type="button" style="width: 50%" class="btn btn-xs btn-outline-info" data-toggle="modal"--}}
{{--                                            data-target="#exampleModalXem{{ $customer->id }}">--}}
{{--                                        {{__('msg.detail')}}--}}
{{--                                    </button>--}}
                                   <img src="{{asset( 'storage/'.$customer->bookimage ) }}" style="width: 100px">
                                </td>
                                <td style=" width: 10px" class="align-middle">
                                    <button type="button" style="width: 50%; margin-left: 30px" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#exampleModalXem{{ $customer->id }}">
                                        In & Lưu
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="page" style="float: right">
                {{$data->links()}}
            </div>
        </div>
    </div>

@stop
