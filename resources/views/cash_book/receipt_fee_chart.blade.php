<style>
    @if(isset($_GET['month_bar']))
        #line-month {
            margin: 0;
            width: 50%;
            float: left;
            border: none;
            text-align: center;
            list-style-type: none;
            background-color: #bee5eb;
            padding-top: 5px;
            height: 32px;
            color: #1d6879;
            outline: none;
        }

        #bar-month {
            margin: 0;
            width: 50%;
            float: right;
            border-top: solid black;
            border-bottom: none;
            border-right: none;
            border-left: none;
            background-color: white;
            text-align: center;
            list-style-type: none;
            height: 32px;
            padding-top: 2px;
            color: #1d6879;
            outline: none;
        }
    @else
        #line-month {
            margin: 0;
            width: 50%;
            float: left;
            border-top: solid black;
            border-bottom: none;
            border-right: none;
            border-left: none;
            background-color: white;
            text-align: center;
            list-style-type: none;
            height: 32px;
            padding-top: 2px;
            color: #1d6879;
            outline: none;
        }

        #bar-month {
            margin: 0;
            width: 50%;
            float: right;
            border: none;
            text-align: center;
            list-style-type: none;
            background-color: #bee5eb;
            padding-top: 5px;
            height: 32px;
            color: #1d6879;
            outline: none;
        }
    @endif

</style>

<div class="clearfix">
    <div style="width: 80%; margin: 0 auto; border: 5px double #bee5eb; float: left">
        <div class="clearfix" style="background-color: #bee5eb; color: #1d6879; font-size: 15px">
            <div style="float: left; width: 80%; padding: 5px">
                <li style="width: 100%; list-style-type: none;">{{ __('cash_book.month_chart') }}</li>
            </div>
            <div style="float: right; width: 20%">
                <button id="line-month">{{ __('cash_book.line') }}</button>
                <button id="bar-month">{{ __('cash_book.bar') }}</button>
            </div>
        </div>
        <div style="padding: 5px 10px 10px 10px; background-color: white; height: 370px">
            <div style="height: 350px">
                {!! $chart->container() !!}
            </div>
        </div>
    </div>
    <div style="width: 20%; margin: 0 auto; border: 5px double #bee5eb; float: right; border-left: none">
        <div class="clearfix" style="background-color: #bee5eb; color: #1d6879; font-size: 15px">
            <div style="float: left; width: 80%; padding: 5px; text-align: center">
                <li style="width: 100%; list-style-type: none">{{ __('cash_book.detail') }}</li>
            </div>
        </div>
        <div style="height: 370px; background-color: white; text-align: center">
            <div style="padding-top: 10px">
                @if($percentText == "increase")
                    <span style="color: green"><i class="fas fa-angle-double-up" aria-hidden="true"></i> {{ $percent }} %</span>
                @elseif($percentText == "decrease")
                    <span style="color: red"><i class="fas fa-angle-double-down" aria-hidden="true"></i> {{ $percent }} %</span>
                @else
                    <span style="color: orange"><i class="fas fa-angle-double-left" aria-hidden="true"></i> {{ $percent }} %</span>
                @endif
                @php $en = \Carbon\CarbonImmutable::now()->locale('vi_VN'); @endphp
                <h5 style="margin-bottom: 0">{{ number_format($monthFee,0,',','.') }} VND</h5>
                <span style="color: #1d6879">Doanh thu tháng {{ $en->month }}</span>
                <p style="font-size: 12px; color: #1d6879">(So với tháng trước)</p>
            </div>
            <hr>
            <div>
                <p style="margin: 0 0 5px 0; color: #123012; font-size: 20px">Doanh thu tháng</p>
                <p style="text-align: left; margin: 0; padding-left: 10px; color: #123012">Từ:
                    <select style="float:right; margin-right: 10px" id="from">
                        <option value="1">Tháng 1</option>
                        <option value="2">Tháng 2</option>
                        <option value="3">Tháng 3</option>
                        <option value="4">Tháng 4</option>
                        <option value="5">Tháng 5</option>
                        <option value="6">Tháng 6</option>
                        <option value="7">Tháng 7</option>
                        <option value="8">Tháng 8</option>
                        <option value="9">Tháng 9</option>
                        <option value="10">Tháng 10</option>
                        <option value="11">Tháng 11</option>
                        <option value="12">Tháng 12</option>
                    </select>
                </p>
                <p style="text-align: left; margin: 5px 0 0 0; padding-left: 10px; color: #123012">Đến:
                    <select style="float:right; margin-right: 10px" id="to">
                        <option value="1">Tháng 1</option>
                        <option value="2">Tháng 2</option>
                        <option value="3">Tháng 3</option>
                        <option value="4">Tháng 4</option>
                        <option value="5">Tháng 5</option>
                        <option value="6">Tháng 6</option>
                        <option value="7">Tháng 7</option>
                        <option value="8">Tháng 8</option>
                        <option value="9">Tháng 9</option>
                        <option value="10">Tháng 10</option>
                        <option value="11">Tháng 11</option>
                        <option value="12">Tháng 12</option>
                    </select>
                </p>
                <hr style="width: 80%">
                <div id="statistic" style="padding-left: 10px; margin-top: 5px">
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
{!! $chart->script() !!}
<script>

    $("#from").on('change', function () {
        var url = '{{ route('cash-book.statistic').'?from=:from&to=:to' }}';
        url = url.replace(':from', $(this).val());
        url = url.replace(':to', $("#to").val());
        $.ajax({
            url: url,
            type: "GET",
        }).done(function (response) {
            $("#statistic").empty();
            $("#statistic").html(response);
        });
    });

    $("#to").on('change', function () {
        var url = '{{ route('cash-book.statistic').'?from=:from&to=:to' }}';
        url = url.replace(':from', $("#from").val());
        url = url.replace(':to', $(this).val());
        $.ajax({
            url: url,
            type: "GET",
        }).done(function (response) {
            $("#statistic").empty();
            $("#statistic").html(response);
        });
    });

    $("#bar-month").on('click', function () {
        // let bar = document.querySelector("#bar-month");
        // bar.style.backgroundColor = "white";
        // bar.style.paddingTop = '2px';
        // bar.style.borderTop = 'solid black';
        // let line = document.querySelector("#line-month");
        // line.style.backgroundColor = "#bee5eb";
        // line.style.paddingTop = '5px';
        // line.style.border = 'none';
        window.location.href = '{{ route('cash-book.index').'?month_bar=true' }}';
    });

    $("#line-month").on('click', function () {
        // let line = document.querySelector("#line-month");
        // line.style.backgroundColor = "white";
        // line.style.paddingTop = '2px';
        // line.style.borderTop = 'solid black';
        // let bar = document.querySelector("#bar-month");
        // bar.style.backgroundColor = "#bee5eb";
        // bar.style.paddingTop = '5px';
        // bar.style.border = 'none';
        window.location.href = '{{ route('cash-book.index') }}';
    });
</script>
