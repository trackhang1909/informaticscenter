@if(strpos($cbr->cash_code, "PT-TQ") !== false)
    @php $receipt = new \App\Models\Receipt(); /** @var TYPE_NAME $cbr */
        $data = $receipt->read($cbr->cash_code); @endphp

    @if($data != null)
        <a class="btn btn-xs btn-success" href="{{ route('student.register-index').'?courses='.$data->course_id.'&student_id='.$data->student_id.'&receipt_id='.$data->id }}" role="button">{{ __('action.update') }}</a>

        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $cbr->id }}">
            {{__('action.delete')}}
        </button>
    @endif
@else
    @php $data = new \App\Models\ReceiptDifferent(); $receipt = $data->read($cbr->cash_code); @endphp

    @if($receipt != null)
        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $cbr->id }}">
            {{__('action.update')}}
        </button>

        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $cbr->id }}">
            {{__('action.delete')}}
        </button>

        <form class="form-container form-add" action="{{ route('receipt-different.store') }}" method="POST">
            <div class="modal fade" id="exampleModalUpdate{{ $cbr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">{{ __('receipt.info') }}</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>

                        <div class="modal-body">
                            @csrf
                            <input style="display: none" type="text" name="id" class="form-control" id="exampleInput" value="{{ $receipt->id }}" readonly>
                            <div class="form-group">
                                <label for="exampleInput1">{{__('receipt.name')}} <label class="required-p">*</label></label>
                                <input type="text" name="name" class="form-control" id="exampleInput1" value="{{ $receipt->name }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput2">{{__('receipt.address')}} <label class="required-p">*</label></label>
                                <input type="text" name="address" class="form-control" id="exampleInput2" value="{{ $receipt->address }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput3">{{__('receipt.reason')}} <label class="required-p">*</label></label>
                                <input type="text" name="reason" class="form-control" id="exampleInput3" value="{{ $receipt->reason }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput4">{{__('receipt.total_money2')}} <label class="required-p">*</label></label>
                                <input type="number" name="total_money" class="form-control" id="exampleInput4" value="{{ $receipt->total_money }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInput5">{{__('receipt.money_text')}} <label class="required-p">*</label></label>
                                <input type="text" name="money_text" class="form-control" id="exampleInput5" value="{{ $receipt->money_text }}">
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endif
@endif

<form class="form-container" action="{{ route('cash-book.destroy', $cbr->id) }}" method="POST">
    <input name="_method" type="hidden" value="DELETE">
    <div class="modal fade" id="exampleModalDelete{{ $cbr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('receipt.info')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @csrf
                    <p style="margin-bottom: 0">{{__('action.confirm_delete')}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                    <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                </div>
            </div>
        </div>
    </div>
</form>

