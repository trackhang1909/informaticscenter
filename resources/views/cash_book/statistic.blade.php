<p style="margin: 0; text-align: left; color: #123012">Thu: <span style="float: right; padding-right: 10px">{{ number_format($total_receipt, 0,',','.') }} VND</span></p>
<p style="margin: 0; text-align: left; color: #123012">Chi: <span style="float: right; padding-right: 10px">{{ number_format($total_expense, 0,',','.') }} VND</span></p>
<p style="margin: 0; text-align: left; color: #123012">Tổng: <span style="float: right; padding-right: 10px">{{ number_format($total, 0,',','.') }} VND</span></p>
