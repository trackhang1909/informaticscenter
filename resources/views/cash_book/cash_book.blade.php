@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('cash_book.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <div id="chart" style="margin-bottom: 40px">
                        @include('cash_book.receipt_fee_chart')
                    </div>
                </div>

                <div class="panel-body table-responsive">
                    <div class="clearfix header-course">
                        @if(session('success'))
                            <div class="alert alert-success abc" role="alert">
                                {{ session('success') }}
                            </div>
                        @endif
                        @if(session('fail'))
                            <div class="alert alert-danger abc" role="alert">
                                {{ session('fail') }}
                            </div>
                        @endif
                    </div>
                    <div style="margin-bottom: 40px">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center" scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{ __('receipt.code') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item => $cbr)
                                <tr>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td>{{ $cbr->cash_code }}</td>
                                    <td class="align-middle">
                                        <button type="button" class="receipt btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal{{ $cbr->id }}" data-code="{{ $cbr->cash_code }}">
                                            {{ __('action.review_receipt') }}
                                        </button>

                                        <div class="modal fade" id="exampleModal{{ $cbr->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('receipt.info') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                    <div class="modal-footer" style="width: 100%; margin-right: 10%; margin-bottom: 20px">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                        @can('create')
                                                            @if(strpos($cbr->cash_code, "PT-TQ") !== false)
                                                                <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt.review', '0').'?export=true&code='.$cbr->cash_code }}" role="button">{{ __('cash_book.export') }}</a>
                                                            @else
                                                                <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt-different.review', '0').'?export=true&code='.$cbr->cash_code }}" role="button">{{ __('cash_book.export') }}</a>
                                                            @endif
                                                        @endcan
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @include('cash_book.update_delete_cash_book')

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            //Check all checkbox
            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Review receipt
            $(".receipt").each(function () {
                $(this).on('click', function () {
                    var code = $(this).data('code');
                    if (code.search("PT-TQ") !== -1) {
                        var url = '{{ route("receipt.review", "0")."?code=:input" }}';
                        url = url.replace(':input', code);
                    }
                    else {
                        var url = '{{ route("receipt-different.review", "0")."?code=:input" }}';
                        url = url.replace(':input', code);
                    }
                    var id = $(this).data('target');
                    $.ajax({
                        url: url,
                        type: "GET",
                    }).done(function (response) {
                        $(id).find(".modal-body").first().empty();
                        $(id).find(".modal-body").first().html(response);
                    });
                })
            });

        });
    </script>
@stop
