@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%" });
    </script>
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endsection

@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{__('msg.booklist')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh">
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif

                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
                <button type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalThem">
                    {{__('msg.addbook')}}
                </button>
                    <a class="btn btn-primary" style="margin-left: 10px" href="{{route('register-book.index')}}" role="button">Đăng kí sách</a>

                    {{--                    <button style="margin-left: 10px" type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalBan">--}}
{{--                        Đăng kí sách--}}
{{--                    </button>--}}
{{--                    <form class="form-container form-add" action="{{ route('sell.activate') }}" method="POST" enctype="multipart/form-data">--}}
{{--                        <div class="modal fade" id="exampleModalBan" tabindex="-1" role="dialog"--}}
{{--                             aria-labelledby="exampleModalLabel">--}}
{{--                            <div class="modal-dialog" role="document">--}}
{{--                                <div class="modal-content">--}}
{{--                                    <div class="modal-header">--}}
{{--                                        <h5 class="modal-title" id="exampleModalLabel">Nhập đơn hàng</h5>--}}
{{--                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">--}}
{{--                                            <span aria-hidden="true">&times;</span>--}}
{{--                                        </button>--}}
{{--                                    </div>--}}
{{--                                    <div class="modal-body">--}}
{{--                                        @csrf--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput1">Tên khách hàng : <label class="required-p">*</label></label>--}}
{{--                                            <input type="text" name="name" class="form-control" id="exampleInput1" placeholder="">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput2">Số điện thoại :<label class="required-p">*</label></label>--}}
{{--                                            <input type="text" name="phone" class="form-control" id="exampleInput2" placeholder="">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput3">Địa chỉ : <label class="required-p">*</label></label>--}}
{{--                                            <input type="text" name="address" class="form-control" id="exampleInput3" placeholder="">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput2">Chọn sách :<label class="required-p">*</label></label>--}}
{{--                                            <input type="text" name="book_id" class="form-control" id="exampleInput2" placeholder="">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput4">Số lượng sách :<label class="required-p">*</label></label>--}}
{{--                                            <input type="number" name="quantity" class="form-control" id="exampleInput4" placeholder="">--}}
{{--                                        </div>--}}
{{--                                        <div class="form-group">--}}
{{--                                            <label for="exampleInput6">Tổng tiền :  <label class="required-p">*</label></label>--}}
{{--                                            <input type="number" name="money" class="form-control" id="exampleInput6" placeholder="">--}}

{{--                                        </div>--}}
{{--                                        <div class="modal-footer">--}}
{{--                                            <button type="button" class="btn btn-secondary"--}}
{{--                                                    data-dismiss="modal">{{ __('msg.no') }}</button>--}}
{{--                                            <button type="submit" class="btn btn-primary">{{ __('msg.add') }}</button>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </form>--}}

                <form class="form-container form-add" action="/book/add" method="POST" enctype="multipart/form-data">
                    <div class="modal fade" id="exampleModalThem" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('msg.addbook')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput1">{{ __('msg.bookname') }} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInput1" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{ __('msg.author') }} <label class="required-p">*</label></label>
                                        <input type="text" name="author" class="form-control" id="exampleInput2" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{ __('msg.price') }} <label class="required-p">*</label></label>
                                        <input type="number" name="price" class="form-control" id="exampleInput3" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{ __('msg.quantity') }} <label class="required-p">*</label></label>
                                        <input type="number" name="quantity" class="form-control" id="exampleInput4" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput6">{{ __('msg.bookcontent') }} <label class="required-p">*</label></label>
                                        <textarea  name="content" class="form-control summary-ckeditor" id="exampleInput6" placeholder=""></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput5">{{ __('msg.image') }} <label class="required-p">*</label></label>
                                        <input type="file" name="image" class="form-control" id="exampleInput5">
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">{{ __('msg.no') }}</button>
                                        <button type="submit" class="btn btn-primary">{{ __('msg.add') }}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>


            </div>

            <div class="grid-container" style="text-align: center">
                @foreach($data as $book)
                    <div class="card" style="width: 14rem;">
                        <div class="card-img-top" style="background-image: url({{ 'storage/'. $book->image }});"></div>
                        <div class="card-body">
                            <p class="card-text">{{ $book->name }}</p>
                            <p class="card-text"> {{__('msg.price')}} :{{ $book->price }} VND</p>
                            <p class="card-text">Ngày nhập :{{ $book->created_at }}</p>


                            <button type="button" class="btn-del btn btn-xs btn-danger" data-toggle="modal"
                                    data-target="#exampleModalXoa{{ $book->id }}">
                                {{__('msg.delete')}}
                            </button>
                            <button type="button" class="btn-up btn btn-xs btn-success" data-toggle="modal"
                                    data-target="#exampleModalCn{{ $book->id }}">
                                {{__('msg.update')}}
                            </button>
                            <button type="button" class="btn-det btn btn-xs btn-info" data-toggle="modal"
                                    data-target="#exampleModalXem{{ $book->id }}">
                                {{__('msg.detail')}}
                            </button>
                            <button type="button" class="btn-det btn btn-xs btn-warning" data-toggle="modal"
                                    data-target="#exampleModalGui{{ $book->id }}">
                                {{__('msg.send')}}
                            </button>

                            <form class="form-container" action="/book/delete" method="GET">
                                <div class="modal fade" id="exampleModalXoa{{ $book->id }}" tabindex="-1"
                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{__('msg.book')}}</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group">
                                                    <input style="display: none" type="text" name="id"
                                                           class="form-control" id="exampleInputStt"
                                                           value="{{ $book->id }}">
                                                </div>
                                                <p style="text-align: left">{{__('action.confirm_delete')}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{__('msg.no')}}</button>
                                                <button type="submit"
                                                        class="btn btn-primary">{{__('msg.yes')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <div class="modal fade" id="exampleModalXem{{ $book->id }}" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel"
                                                style="text-align: center">{{__('msg.bookinfo')}}</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body ">
                                            <h5><i>{{__('msg.bookname')}} :</i><b> {{ $book->name }}</b></h5>
                                            <h5><i>{{__('msg.author')}} :</i><b> {{ $book->author }}</b></h5>
                                            <h5><i>{{__('msg.price')}} :</i><b> {{ $book->price }}</b></h5>
                                            <h5><i>{{__('msg.quantity')}} :</i><b> {{ $book->quantity }}</b></h5>
                                            <textarea class="form-control summary-ckeditor" id="summary-ckeditor" name="content">
                                                         {{ $book->content }}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

{{--                Form GUIiiiiiiii--}}

                <form class="form-container form-send" action="/book/send" method="POST">
                    <div class="modal fade" id="exampleModalGui{{ $book->id }}" tabindex="-1"
                         role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title"
                                        id="exampleModalLabel">{{__('msg.sendbook')}}</h5>

                                    <button type="button" class="close" data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label style="float: left" for="exampleInputStt">{{__('msg.number')}}</label>
                                        <input type="number" name="id" class=" id-book form-control"
                                               id="exampleInputStt" value="{{ $book->id }}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label style="float: left" for="exampleInputStt" generated="true">{{__('msg.teacher')}}</label>
                                        <select class="form-control chosen-select" id="exampleInputStt" name="teacher_id[]" multiple="">
                                            <option hidden></option>
                                            @foreach($nv as $staff)
                                            <option value="{{$staff->id}}">
                                                    {{ $staff->name }}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label style="float: left" for="exampleInputStt1">{{__('msg.bookname')}}</label>
                                        <select class="form-control" id="exampleInputStt1" name="book_id" >
                                            <option value="{{ $book->id }}">{{ $book->name }}</option>
                                        </select>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{__('msg.no')}}</button>
                                    <button type="submit"
                                            class="btn btn-primary"><i class="fa fa-paper-plane"></i>  {{__('msg.send!')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>


                            <form class="form-container form-up" action="/book/add" method="POST" enctype="multipart/form-data">
                                <div class="modal fade" id="exampleModalCn{{ $book->id }}" tabindex="-1"
                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title"
                                                    id="exampleModalLabel">{{__('msg.update')}}</h5>

                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group">
                                                    <input style="display: none" type="number" name="id" class="id-book form-control"
                                                           id="exampleInput" value="{{ $book->id }}">
                                                </div>
                                                <div class="form-group">
                                                    <label style="float: left" for="exampleInput1">{{ __('msg.bookname') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="name" class="form-control" id="exampleInput1" value="{{ $book->name }}">
                                                </div>
                                                <div class="form-group">
                                                    <label style="float: left" for="exampleInput2">{{ __('msg.author') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="author" class="form-control" id="exampleInput2" value="{{ $book->author }}">
                                                </div>
                                                <div class="form-group">
                                                    <label style="float: left" for="exampleInput3">{{ __('msg.price') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="price" class="form-control" id="exampleInput3" value="{{ $book->price }}">
                                                </div>
                                                <div class="form-group">
                                                    <label style="float: left" for="exampleInput4">{{ __('msg.quantity') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="quantity" class="form-control" id="exampleInput4" value="{{ $book->quantity }}">
                                                </div>
                                                <div class="form-group">
                                                    <label style="float: left" for="exampleInput5" >{{ __('msg.image') }} <label class="required-p">*</label></label>
                                                    <input type="file" name="image" class="form-control" id="exampleInput5">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{ __('msg.no') }}</button>
                                                <button type="submit"
                                                        class="btn btn-primary">{{ __('msg.update') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
            </div>

            @endforeach
                    </div>
            <div class="clearfix">
                <div class="page" style="float: right" >
                    {{ $data->links() }}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <script type="text/javascript">
        $(document).ready(function() {
            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                            remote: "/unique-book?id=" + $(this).find('.id-book').first().val(),
                        },
                        author:{
                            required: true,
                        },
                        price: {
                            required: true,
                            maxlength: 11,
                        },
                        quantity: {
                            required: true,
                            maxlength: 11,
                        },
                        image: {
                            required: true,
                        },

                    },
                    messages: {
                        name: {
                            required: "<?php echo __("msg.bookname").__("action.required") ?>",
                            remote: "<?php echo __("msg.bookexisted")?>"
                        },
                        author: {
                            required: "<?php echo __("msg.author").__("action.required") ?>",
                        },
                        price: {
                            required: "<?php echo __("msg.price").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.price").' '.__("action.maxlength11") ?>",
                        },
                        quantity: {
                            required: "<?php echo __("msg.quantity").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.quantity").' '.__("action.maxlength11") ?>",
                        },
                        image: {
                            required: "<?php echo __("msg.image").__("action.required") ?>",

                        }
                    },
                });
            });

            $("form.form-up").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                            remote: "/unique-book?id=" + $(this).find('.id-book').first().val(),
                        },
                        author:{
                            required: true,
                        },
                        price: {
                            required: true,
                            maxlength: 11,
                        },
                        quantity: {
                            required: true,
                            maxlength: 11,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("msg.bookname").__("action.required") ?>",
                            remote: "<?php echo __("msg.bookexisted")?>"
                        },
                        author: {
                            required: "<?php echo __("msg.author").__("action.required") ?>",
                        },
                        price: {
                            required: "<?php echo __("msg.price").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.price").' '.__("action.maxlength11") ?>",
                        },
                        quantity: {
                            required: "<?php echo __("msg.quantity").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.quantity").' '.__("action.maxlength11") ?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/book/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'book-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('msg.bookempty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("book.detail", ":name") }}';
                            url = url.replace(':name', data.name);
                            return '<a href="' + url + '" class="list-group-item">' + data.name + '</a>';
                        }
                    }
                },
            ]);


        });
    </script>
@stop
