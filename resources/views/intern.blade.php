@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/status.css') }}">
@endsection
@section('javascript')
    <script>
        {{--function selectStatus(obj){--}}
        {{--    var id = $(obj).val();--}}
        {{--    flag = $(obj).parents('.card').find('a.change_status');--}}
        {{--    var innerShipId = $(obj).data('id');--}}
        {{--    $.get(--}}
        {{--        '{{  route('internship.ajax')  }}',{id:id,innerShipId:innerShipId},function(data){--}}
        {{--            $(flag).html(data)--}}
        {{--            $(obj).parents('.card').find('a.change_status').css('background', 'blue');--}}
        {{--            $(obj).parents('.card').find('.status').after().css('background', 'yellow');--}}

        {{--        }--}}
        {{--    )--}}

        $(document).ready(function(){
            $(".select").change(function () {
                var id = $(this).val();
                flag = $(this).parents('.card').find('a.change_status');
                var innerShipId = $(this).data('id');
                $.get(
                    '{{  route('internship.ajax')  }}',{id:id,innerShipId:innerShipId},function(data){
                        $(flag).html(data)
                    }
                )
            })
        })

        $(".chosen-select").chosen({ width: "100%" });
    </script>
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Sinh viên đang thực tập</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a  href="{{ route('filter1.index') }}" style="color: #e81a1a">
                                Chưa hoàn thành
                            </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('filter2.index') }}" style="color: #00A000">
                                Đã hoàn thành
                            </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('filter3.index') }}" style="color: #0e84b5">
                                Chờ xử lý
                                <span class="badge badge-danger" style="overflow: overlay;width: 19px;">
                               </span>
                            </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('filter4.index') }}" style="color: #1a252f">
                                Hủy
                            </a></li>
                        <li class="breadcrumb-item"><a href="{{ route('intern.index') }}">
                                Reload
                            </a></li>
                        {{--                        <li class="breadcrumb-item">--}}
{{--                           <form action="{{ route('filter1.index') }}">--}}
{{--                               <button class="btn btn-xs" style="background-color: yellow">Chưa hoàn thành</button>--}}
{{--                           </form>--}}
{{--                           <form action="{{ route('filter2.index') }}">--}}
{{--                               <button class="btn btn-xs" style="background-color: #00b44e">Hoàn thành</button>--}}
{{--                           </form>--}}
{{--                           <form action="{{ route('filter3.index') }}">--}}
{{--                               <button class="btn btn-xs" style="background-color: #0e84b5">Chờ xử lý</button>--}}
{{--                           </form>--}}
{{--                           <form action="{{ route('filter4.index') }}">--}}
{{--                               <button class="btn btn-xs" style="background-color: #bd2130">Hủy</button>--}}
{{--                           </form>--}}
{{--                            <form action="{{ route('intern.index') }}">--}}
{{--                                <button class="btn btn-xs" style="background-color: #b3d7ff">Reload</button>--}}
{{--                            </form>--}}
{{--                        </li>--}}
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid" style="margin-top: 30px">
            <div class="clearfix header-kh">

                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>
            <div class="grid-container" style="text-align: center">
                @foreach($data as $internship)
                    <div class="card" style="width: 14rem; border-radius: 10px; border-top: 10px; background-color:#E8E8E8">
{{--                        @if($internship->status_id == 1)--}}
{{--                            <div class="status_change" style="">--}}
{{--                                <a class="change_status" id="data-1" data-id="{{ $internship->id  }}" href="#" style="color: white;">--}}
{{--                                    {{$internship->statusname}}--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @elseif($internship->status_id == 2)--}}
{{--                            <div class="status_change1" style="">--}}
{{--                                <a class="change_status" id="data-1" data-id="{{ $internship->id  }}" href="#" style="color: white">--}}
{{--                                    {{$internship->statusname}}--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @elseif($internship->status_id == 3)--}}
{{--                            <div class="status_change2 " style="">--}}
{{--                                <a class="change_status" id="data-1" data-id="{{ $internship->id  }}" href="#" style="color: white">--}}
{{--                                    {{$internship->statusname}}--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        @else--}}

{{--                        @endif--}}
                        <div class="card-img-top" style="background-image: url({{ 'storage/'. $internship->image }});">
                            <div class="status">
                                <a class="change_status
                                    @if($internship->status_id == 1) bg-black
                                    @elseif($internship->status_id == 2) bg-success
                                    @elseif($internship->status_id == 3) bg-info  @elseif($internship->status_id == 4) bg-dark @endif" id="data-1" data-id="{{ $internship->id  }}" href="#" style="color: white">
                                    {{$internship->statusname}}
                                </a>
                            </div>
                        </div>
                        <div class="card-body">
{{--                            <img src="https://thienquantech.com/wp-content/uploads/2020/02/logo-hitech-thien-quan.png"--}}
{{--                                 style="width: 150px">--}}
                            <div>
                                <p>{{''}}</p>
                            </div>
                            <p class="card-text" style="font-family: Verdana; color: #1F2D3D">
                                {{ $internship->name }}</p>
                            <div style="margin: 14px 0;">
                                <hr style="background-color: #b3d7ff; width: 30px">
                            </div>
                            <p class="card-text" style=" color: #1F2D3D">{{ $internship->topicname }}</p>
                            <button type="button" data-toggle="modal"
                                    data-target="#exampleModalCn{{ $internship->id }}" style="border-radius: 5px; background-color: orange;
                        margin-top: -15px;margin-left: 65px;" class="btn btn-xs">
                                Chỉnh sửa
                            </button>
                            <form class="form-container form-up" action="{{ route('internship.add')}}" method="POST">
                                <div class="modal fade" id="exampleModalCn{{ $internship->id }}" tabindex="-1"
                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header" style="background-color: darkgrey;">
                                                <h5 class="modal-title"
                                                    id="exampleModalLabel">Chỉnh sửa thông tin</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                @csrf
                                                <div class="form-group">
                                                    <input style="display: none" type="number" name="id" class="id-staff form-control"
                                                           id="exampleInputStt" value="{{ $internship->id }}" readonly>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput"  >{{ __('msg.name') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="name" class="form-control"
                                                           id="exampleInput" value="{{ $internship->name }}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="exampleInput2">{{ __('msg.email') }} <label class="required-p">*</label></label>
                                                    <input type="text" name="email" class="form-control"
                                                           id="exampleInput2" value="{{ $internship->email }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput3">{{ __('msg.phonenumber') }} <label class="required-p">*</label></label>
                                                    <input type="number" name="phone" class="form-control"
                                                           id="exampleInput3" value="{{ $internship->phone}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput1">Trường đại học <label class="required-p">*</label></label>
                                                    <input type="text" name="university" class="form-control"
                                                           id="exampleInput1"
                                                           value="{{ $internship->university }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput1">Trình độ <label class="required-p">*</label></label>
                                                    <select class="form-control" id="exampleInput1" name="level" required>
                                                        <option value="{{ $internship->level }}">{{ $internship->level }}</option>
                                                        <option >Đại học</option>
                                                        <option >Cao đẳng</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput4">Ảnh đại diện <label class="required-p">*</label></label>
                                                    <input type="text" name="image" class="form-control"
                                                           id="exampleInput4" value="{{ $internship->image }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput5">Đề tài <label class="required-p">*</label></label>
                                                    <select class="form-control" id="exampleInput5" name="topic_id">
                                                        <option value="{{ $internship->topic_id }}">{{ $internship->topicname }}</option>
                                                        @foreach($top as $topic)
                                                            <option value="{{ $topic->id }}">{{ $topic->name }}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput6">Mục đích thực tập <label class="required-p">*</label></label>
                                                    <input type="text" name="type" class="form-control"
                                                           id="exampleInput6" value="{{ $internship->type }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput7">Thời gian <label class="required-p">*</label></label>
                                                    <input type="text" name="total_time" class="form-control"
                                                           id="exampleInput7" value="{{ $internship->total_time }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput8">Bắt đầu  <label class="required-p">*</label></label>
                                                    <input type="datetime-local" name="start_time" class="form-control"
                                                           id="exampleInput8" value="{{ date_format(new DateTime($internship->start_time), 'Y-m-d\TH:i')}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput9">Kết thúc <label class="required-p">*</label></label>
                                                    <input type="datetime-local" name="end_time" class="form-control"
                                                           id="exampleInput9" value="{{ date_format(new DateTime($internship->end_time), 'Y-m-d\TH:i')}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput10">Số CMND: <label class="required-p">*</label></label>
                                                    <input type="text" name="cmnd" class="form-control"
                                                           id="exampleInput10" value="{{ $internship->cmnd }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput11">Ngày cấp : <label class="required-p">*</label></label>
                                                    <input type="datetime-local" name="cmndday" class="form-control"
                                                           id="exampleInput11" value="{{ date_format(new DateTime($internship->cmndday), 'Y-m-d\TH:i')}}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput12">Nơi cấp : <label class="required-p">*</label></label>
                                                    <input type="text" name="cmndplace" class="form-control"
                                                           id="exampleInput12" value="{{ $internship->cmndplace }}">
                                                </div>
                                                <div class="form-group">
                                                    <label for="exampleInput13">Địa chỉ : <label class="required-p">*</label></label>
                                                    <input type="text" name="address" class="form-control"
                                                           id="exampleInput13" value="{{ $internship->address }}">
                                                </div>


                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">{{__('msg.no')}}</button>
                                                <button type="submit"
                                                        class="btn btn-primary">{{__('msg.update')}}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                            <button type="button" style="border-radius: 5px; background-color: dodgerblue;margin-top: -64px;
                                       margin-right: 66px;"
                                    class="btn btn-xs" data-toggle="modal"
                                    data-target="#exampleModalXem1{{ $internship->id }}">
                                Thông tin
                            </button>
                            <div style="margin: 14px 0;">
                                <hr style="background-color: orange; width: 30px">
                            </div>
{{--                            <button type="button" style="border-radius: 5px; background-color: forestgreen;" class="btn btn-xs btn-success" data-toggle="modal"--}}
{{--                                    data-target="#exampleModalXoa{{ $internship->id }}">--}}
{{--                                <i class="fa fa-check" aria-hidden="true"></i>--}}
{{--                            </button>--}}
{{--                            <button type="button" style="border-radius: 5px; background-color: red; width: 25px" class="btn btn-xs btn-danger" data-toggle="modal"--}}
{{--                                    data-target="#exampleModalXoa{{ $internship->id }}">--}}
{{--                                <i class="fa fa-times" aria-hidden="true"></i>--}}
{{--                            </button>--}}
                            <select onchange="selectStatus(this)" class="form-control select" id="data-1" data-id="{{  $internship->id }}" name="select1">
                                <option hidden></option>
                                @foreach($sta as $st)
                                    <option {{  $internship->status_id == $st->id?'selected':'' }} value="{{ $st->id  }}">{{$st->name}}</option>
                                @endforeach
                            </select>
{{--                            <form class="form-container" action="{{route('internship.delete')}}" method="GET">--}}
{{--                                <div class="modal fade" id="exampleModalXoa{{ $internship->id }}" tabindex="-1"--}}
{{--                                     role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">--}}
{{--                                    <div class="modal-dialog" role="document">--}}
{{--                                        <div class="modal-content">--}}
{{--                                            <div class="modal-header">--}}
{{--                                                <h5 class="modal-title" id="exampleModalLabel">Chuyển đến mục hoàn thành</h5>--}}
{{--                                                <button type="button" class="close" data-dismiss="modal"--}}
{{--                                                        aria-label="Close">--}}
{{--                                                    <span aria-hidden="true">&times;</span>--}}
{{--                                                </button>--}}
{{--                                            </div>--}}
{{--                                            <div class="modal-body">--}}
{{--                                                @csrf--}}
{{--                                                <div class="form-group">--}}
{{--                                                    <input style="display: none" type="text" name="id"--}}
{{--                                                           class="form-control"--}}
{{--                                                           value="{{ $internship->id }}">--}}
{{--                                                </div>--}}
{{--                                                <button type="button" class="btn btn-secondary"--}}
{{--                                                        data-dismiss="modal">Hủy</button>--}}
{{--                                                <button type="submit"--}}
{{--                                                        class="btn btn-primary">Chuyển</button>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </form>--}}

                            <div class="modal fade" id="exampleModalXem1{{ $internship->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header" style="background-color: darkgrey;">
                                            <h5 class="modal-title" id="exampleModalLabel">Thông tin sinh viên</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body clearfix">
                                            <strong style="text-decoration: underline;margin-left: 73px;
                                            font-style: oblique;letter-spacing: 3px;">
                                                Trung tâm tin học Thiên Quân
                                            </strong>
                                            <img src="{{asset( 'storage/'. $internship->image )}}"
                                                 style="width: 100px; margin-right:388px; border: #00b44e">
                                            <p style=" font-family: Onyx; width: 118px;
                                            margin-left: 213px;margin-top: -100px;font-size: 45px; letter-spacing: 1px">
                                                Thông tin
                                            </p>
                                            <code style="font-size: 67.5%;">
                                                <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                                                Tuyệt đối bảo mật thông tin cho sinh viên trung tâm
                                            </code>
                                            <div style="float: left; width: 20%;margin-top: 57px;margin-left: 17px;">
                                                <p style="float: left">Email:</p><br>
                                                <p style="float: left">Số CMND:</p>
                                                <p style="float: left">Số ĐT:</p>
                                                <p style="float: left">Ngày cấp:</p>
                                                <p style="float: left">Nơi cấp:</p>
                                                <p style="float: left">Địa chỉ:</p>
                                                <p style="float: left">Tên trường:</p>
                                                <p style="float: left">Trình độ:</p>
                                            </div>
                                            <div style="float: right; width: 75%;margin-top: 31px;">
                                                <p>{{ $internship->email }}</p>
                                                <p>{{ $internship->cmnd }}</p>
                                                <p>{{ $internship->phone }}</p>
                                                <p>{{ $internship->cmndday }}</p>
                                                <p>{{ $internship->cmndplace }}</p>
                                                <p>{{ $internship->address }}</p>
                                                <p>{{ $internship->university }}</p>
                                                <p>{{ $internship->level }}</p>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button style="margin-right: 215px; background-color: darkgrey" type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="clearfix">
                <div class="page" style="float: right" >
                    {{ $data->links() }}
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <script>
        $( document)
            .ready(function() {
                $( document.body ).find( $( "div.card" ) );
                var n = $( ".card" ).children().length;
                $( "span.badge" ).text(n/2)
            })
    </script>
    </script>
@stop
