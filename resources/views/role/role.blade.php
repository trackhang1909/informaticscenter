@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%", no_results_text: "{{ __('role.no_permission') }}" });
    </script>
@endsection
@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('role.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix" style="padding-bottom: 40px">
                <ul class="list-group" style="float: left; width: 60%">
                    <li class="list-group-item active" style="text-align: center">{{ __('role.role') }}</li>
                    <li class="list-group-item">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center">{{ __('action.id') }}</th>
                                <th scope="col">{{ __('role.name') }}</th>
                                <th scope="col">{{ __('role.permission') }}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($role as $item => $data)
                                <tr>
                                    <th scope="row" style="text-align: center">{{ $role->firstItem() + $item }}</th>
                                    <td>{{ $data->name }}</td>
                                    <td>
                                        @foreach($data->permission as $permiss)
                                            @if($permiss->name == 'view')
                                                <button type="button" class="btn btn-xs btn-info">{{ $permiss->name }}</button>
                                            @elseif($permiss->name == 'create')
                                                <button type="button" class="btn btn-xs btn-secondary">{{ $permiss->name }}</button>
                                            @elseif($permiss->name == 'update')
                                                <button type="button" class="btn btn-xs btn-success">{{ $permiss->name }}</button>
                                            @elseif($permiss->name == 'superAdmin')
                                                <button type="button" class="btn btn-xs btn-warning">{{ $permiss->name }}</button>
                                            @elseif($permiss->name == 'staff')
                                                <button type="button" class="btn btn-xs btn-dark">{{ $permiss->name }}</button>
                                            @elseif($permiss->name == 'cashBook')
                                                <button type="button" class="btn btn-xs btn-primary">{{ $permiss->name }}</button>
                                            @else
                                                <button type="button" class="btn btn-xs btn-danger">{{ $permiss->name }}</button>
                                            @endif
                                        @endforeach
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalEdit{{ $data->id }}">
                                            {{ __('action.update') }}
                                        </button>

                                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDel{{ $data->id }}">
                                            {{ __('action.delete') }}
                                        </button>

                                        <form class="form-add form-container" action="{{ route('role.store') }}" method="POST">
                                            @csrf
                                            <div class="modal fade" id="exampleModalEdit{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('role.role') }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                <input style="display: none" name="id" value="{{ $data->id }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="input1">{{ __('role.name') }} <label class="required-p">*</label></label>
                                                                <input type="text" class="form-control" id="input1" name="name" value="{{ $data->name }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="input2" generated="true">{{ __('role.permission') }} <label class="required-p">*</label></label>
                                                                <select class="form-control chosen-select" id="input2" name="permission[]" multiple data-placeholder="{{ __('role.choose_permission') }}">
                                                                    @foreach($permission as $item2)
                                                                        @php $count = 0; @endphp
                                                                        @foreach($data->permission as $item)
                                                                            @if($item->id == $item2->id)
                                                                                <option selected value="{{ $item->id }}">{{ $item->name }}</option>
                                                                                @php /** @var TYPE_NAME $count */
                                                                                    $count++; @endphp
                                                                                @break
                                                                            @endif
                                                                        @endforeach
                                                                        @if($count == 0)
                                                                            <option value="{{ $item2->id }}">{{ $item2->name }}</option>
                                                                        @endif
                                                                    @endforeach
                                                                </select>
                                                                <label for="input2" generated="true" class="error"></label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('action.update') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form class="form-container" action="{{ route('role.destroy', $data->id) }}" method="POST">
                                            @csrf
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDel{{ $data->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('role.role') }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                            <button type="submit" class="btn btn-primary">{{ __('action.update') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div class="clearfix">
                            <button style="float: right" type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#exampleModal">
                                {{ __('action.add') }}
                            </button>
                            <form class="form-add form-container" action="{{ route('role.store') }}" method="POST">
                                @csrf
                                <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">{{ __('role.role') }}</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="input1">{{ __('role.name') }} <label class="required-p">*</label></label>
                                                    <input type="text" class="form-control" id="input1" name="name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="input2" generated="true">{{ __('role.permission') }} <label class="required-p">*</label></label>
                                                    <select class="form-control chosen-select" id="input2" name="permission[]" multiple data-placeholder="{{ __('role.choose_permission') }}">
                                                        @foreach($permission as $data)
                                                            <option value="{{ $data->id }}">{{ $data->name }}</option>
                                                        @endforeach
                                                    </select>
                                                    <label for="input2" generated="true" class="error"></label>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                <button type="submit" class="btn btn-primary">{{ __('action.add') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                </ul>
                <ul class="list-group" style="float: right; width: 38%">
                    <li class="list-group-item active" style="text-align: center">{{ __('role.permission') }}</li>
                    <li class="list-group-item">
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col" style="text-align: center">{{ __('action.id') }}</th>
                                <th scope="col">{{ __('role.name') }}</th>
                                <th scope="col">{{ __('role.description') }}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($permission as $item => $data)
                                <tr>
                                    <th scope="row" style="text-align: center">{{ $permission->firstItem() + $item }}</th>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->description }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </li>
                </ul>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            $("form.form-add").each(function() {
                $(this).validate({
                    ignore: [],
                    rules: {
                        name: {
                            required: true,
                        },
                        'permission[]': {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("role.name").__("action.required") ?>",
                        },
                        'permission[]': {
                            required: "<?php echo __("role.permission").__("action.required") ?>",
                        },
                    },
                });
            });

        });
    </script>
@stop
