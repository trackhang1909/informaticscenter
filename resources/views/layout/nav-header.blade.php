<!DOCTYPE html>
@inject('request', 'Illuminate\Http\Request')
<html lang="en">
@include('layout.header')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Home</a>
            </li>
            <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Contact</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Đăng kí</a>
                <div class="dropdown-menu">
                    <a href="{{ route('internship.index') }}" class="nav-link">Đăng kí thực tập</a>
                    <a href="#" class="nav-link">Đăng kí khóa học</a>
                </div>
            </li>
        </ul>

        <!-- SEARCH FORM -->
        <?php
            if ($request->segment(1)) {
                $urlCurrent = $request->segment(1).'.index';
            }
            else {
                $urlCurrent = 'home';
            }
        ?>
        <form class="form-inline ml-3" action="{{ route($urlCurrent) }}" method="GET">
            <div class="input-group input-group-sm">
                <form class="form-container typeahead btn-header" role="search">
                    <input style="height: 35px; width: 200px; font-size: 15px" type="search" name="data" class="form-control form-control-navbar search-input" placeholder="{{ __('action.search') }}" autocomplete="off">
                </form>
{{--                <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">--}}
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                        <i class="fas fa-search" style="color: black"></i>
                    </button>
                </div>
            </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
            <!-- Logout, register -->
            <li class="nav-item dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#">
                    <i class="fas fa-th-large"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                    @can('superAdmin')
                        <a href="{{ route('register') }}" class="dropdown-item">
                            <i class="fas fa-user-circle"></i> {{ __('user.register') }}
                        </a>
                    @endcan
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fas fa-reply-all"></i> {{ __('user.logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                </div>
            </li>
        </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    @include('layout.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        @yield('content')
    </div>
    <!-- /.content-wrapper -->
    @include('layout.footer')

</div>
<!-- /page container -->
<!-- Footer -->
@yield('javascript')
@yield('css')
</body>
</html>
