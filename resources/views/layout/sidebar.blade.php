@inject('request', 'Illuminate\Http\Request')
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
        <img src="{{ asset('dist/img/AdminLTELogo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">CeoEduCenter</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->name }}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview {{ ($request->segment(1) == 'student' || $request->segment(1) == 'receipt') ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-graduation-cap"></i>
                        <p>
                            {{ __('sidebar.manager_student') }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('student.index') }}" class="nav-link" style="background-color: {{ ($request->segment(1) == 'student' && $request->segment(2) != 'register-course') ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-users"></i>
                                <p>
                                    {{ __('student.student') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('receipt.index') }}" class="nav-link" style="background-color: {{ ($request->segment(1) == 'receipt' || $request->segment(2) == 'register-course') ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-window-restore"></i>
                                <p>
                                    {{ __('student.register_course') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ ($request->segment(1) == 'course' || $request->segment(1) == 'certification' || $request->segment(1) == 'schedule' || $request->segment(1) == 'period') ? 'menu-open' : ''}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-cubes"></i>
                        <p>
                            {{ __('sidebar.manager_course') }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('course.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'course' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-tasks"></i>
                                <p>
                                    {{ __('course.course') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('certification.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'certification' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-clone"></i>
                                <p>
                                    {{ __('course.certification') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('schedule.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'schedule' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-calendar"></i>
                                <p>
                                    {{ __('schedule.name') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('period.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'period' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-hourglass-half "></i>
                                <p>
                                    {{ __('class.period') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ ($request->segment(1) == 'class' || $request->segment(1) == 'campus') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            {{ __('sidebar.manager_class') }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('class.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'class' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-university"></i>
                                <p>
                                    {{ __('class.class') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('campus.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'campus' ? '#211d16;' : ''}}">
                                <i class="nav-icon fas fa-industry"></i>
                                <p>
                                    {{ __('class.campus') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ $request->segment(1) == 'staff' ? 'menu-open' : ('' || $request->segment(1) == 'position' ? 'menu-open' : '' )}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-id-card"></i>
                        <p>
                            {{ __('sidebar.manager_staff') }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/staff" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>
                                    {{ __('msg.staff') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/position" class="nav-link">
                                <i class="nav-icon fas fa-archive"></i>
                                <p>
                                    {{ __('msg.position') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ $request->segment(1) == 'book' ? 'menu-open' : ('' || $request->segment(1) == 'warehouse-book' ? 'menu-open' : '' )}}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-bookmark"></i>
                        <p>
                            {{ __('sidebar.manager_book') }}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/book" class="nav-link">
                                <i class="nav-icon fas fa-book"></i>
                                <p>
                                    {{ __('msg.book') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('warehouse-book.index') }}" class="nav-link">
                                <i class="nav-icon fas fa-folder"></i>
                                <p>
                                    {{ __('msg.warehouse') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('teacher-book.index') }}" class="nav-link">
                                <i class=" nav-icon fa fa-envelope"></i>
                                <p>
                                    {{ __('msg.receive') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{$request->segment(1) == 'intern' ? 'menu-open' : ('' || $request->segment(1) == 'topic' ? 'menu-open' : '' ) }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            {{__('msg.interns')}}
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul style="background-color: #302f2f" class="nav nav-treeview">
                        <li class="nav-item {{$request->segment(1) == 'intern' ? 'active active-sub' : '' }}">
                            <a href="{{ route('intern.index') }}" class="nav-link">
                                <i class="nav-icon fa fa-id-badge"></i>
                                <p>
                                    {{ __('msg.intern') }}
                                </p>
                            </a>
                        </li>
                        <li class="nav-item {{$request->segment(1) == 'topic' ? 'active active-sub' : '' }} ">
                            <a href="{{ route('topic.index') }}" class="nav-link">
                                <i class="nav-icon fa fa-file"></i>
                                <p>
                                    {{ __('msg.topic') }}
                                </p>
                            </a>
                        </li>
                    </ul>
                </li>
                @can('superAdmin')
                    <li class="nav-item has-treeview {{ ($request->segment(1) == 'cash-book' || $request->segment(1) == 'receipt-different') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-calculator"></i>
                            <p>
                                {{ __('sidebar.cash_book') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul style="background-color: #302f2f" class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('cash-book.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'cash-book' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-bars"></i>
                                    <p>
                                        {{ __('cash_book.list') }}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('receipt-different.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'receipt-different' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-share"></i>
                                    <p>
                                        {{ __('cash_book.receipt') }}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fa fa-reply"></i>
                                    <p>
                                        {{ __('cash_book.expense') }}
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @elsecan('cashBook')
                    <li class="nav-item has-treeview {{ ($request->segment(1) == 'cash-book' || $request->segment(1) == 'receipt-different') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-calculator"></i>
                            <p>
                                {{ __('sidebar.cash_book') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul style="background-color: #302f2f" class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('cash-book.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'cash-book' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-bars"></i>
                                    <p>
                                        {{ __('cash_book.list') }}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('receipt-different.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'receipt-different' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-share"></i>
                                    <p>
                                        {{ __('cash_book.receipt') }}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fa fa-reply"></i>
                                    <p>
                                        {{ __('cash_book.expense') }}
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan
                @can('superAdmin')
                    <li class="nav-item has-treeview {{ ($request->segment(1) == 'user' || $request->segment(1) == 'role') ? 'menu-open' : '' }}">
                        <a href="#" class="nav-link">
                            <i class="nav-icon fas fa-cogs"></i>
                            <p>
                                {{ __('sidebar.setting') }}
                                <i class="fas fa-angle-left right"></i>
                            </p>
                        </a>
                        <ul style="background-color: #302f2f" class="nav nav-treeview">
                            <li class="nav-item">
                                <a href="{{ route('user.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'user' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-address-card"></i>
                                    <p>
                                        {{ __('sidebar.user') }}
                                    </p>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="{{ route('role.index') }}" class="nav-link" style="background-color: {{ $request->segment(1) == 'role' ? '#211d16;' : ''}}">
                                    <i class="nav-icon fas fa-cog"></i>
                                    <p>
                                        {{ __('sidebar.role') }}
                                    </p>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endcan
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
