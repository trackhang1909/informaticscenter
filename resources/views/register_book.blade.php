    @extends('layout.master')
    @section('css')
        <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
    @endsection
    @section('content')
        <div class="content">
            <div class="container-fluid">

                <div class="clearfix header-kh">
                    <h3> {{__('msg.teacherbook')}}</h3>

                    @if(session()->get('success'))
                        <div class="alert alert-success abc">
                            {{ session()->get('success') }}
                        </div>
                    @endif
                    @if(session('fail'))
                        <div class="alert alert-danger abc" role="alert">
                            {{ session('fail') }}
                        </div>
                    @endif
                </div>
                <form action="{{ route('sell.activate') }}"  method="POST">
                    @csrf
                    <ul class="list-group"  style="width: 40%; float: left; margin-left: 50px">
                        <li class=" list-group-item list-group-item-success" >Thông tin khách hàng</li>
                        <li class="list-group-item">
                            <label for="exampleInput1">Tên khách hàng<label class="required-p">*</label></label>
                            <input type="text" name="name" class="form-control"   id="exampleInput1" placeholder="...">
                        </li>
                        <li class="list-group-item">
                            <label for="exampleInput1">Số điện thoại<label class="required-p">*</label></label>
                            <input type="number" name="phone" class="form-control"  id="exampleInput1" placeholder="...">
                        </li>
                        <li class="list-group-item">
                            <label for="exampleInput1">Địa chỉ<label class="required-p">*</label></label>
                            <input type="text" name="address" class="form-control" id="exampleInput1" placeholder="...">
                        </li>
                    </ul>
                    <ul class="list-group" style="width: 39%; float: right; margin-right: 60px">
                        <li class=" list-group-item list-group-item-success">Mua sách</li>
                        <li class="list-group-item">
                            <label for="exampleInput1">Chọn sách<label class="required-p">*</label></label>
                            <select class="form-control" name="book_id">
                                <option>--Chọn tên sách</option>
                                @foreach ($book as $books)
                                    <option value="{{$books->id}}">{{$books->name}}</option>
                                @endforeach
                            </select>
                        </li>
                        <li class="list-group-item">
                            <label for="exampleInput1">Số lượng<label class="required-p">*</label></label>
                            <input type="number" name="quantity" class="form-control" id="exampleInput1" placeholder="...">
                        </li>
    {{--                    <li class="list-group-item">--}}
    {{--                        <label for="exampleInput1">mã<label class="required-p">*</label></label>--}}
    {{--                        <input type="text" name="code" class="form-control" id="exampleInput1" placeholder="...">--}}
    {{--                    </li>--}}
                        <li class="list-group-item">Tổng tiền :
    {{--                        <input type="text" name="money" class="form-control" id="exampleInput1" placeholder="...">--}}
                                <span class="price" style="color: red; margin-left: 20px; font-size: 20px"></span>
                                <button style="margin-left: 34px" class="btn btn-info" type="submit">Mua</button>
                        </li>
                    </ul>

                </form>


            </div>
        </div>

    @stop
