@extends('layout.master')
@section('css')
    <style>
        #btn-fee {
            float: left;
            width: 40%;
            height: 50px;
            margin-left: 20px;
            font-size: 17px;
            background-color: red;
            color: whitesmoke;
            border-radius: 5px;
            border: red;
        }

        #btn-fee:hover {
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            background-color: whitesmoke;
            color: black;
            border: 2px solid red;
        }

        #btn-diff {
            float: right;
            width: 40%;
            height: 50px;
            margin-right: 20px;
            font-size: 17px;
            background-color: #2583a8;
            color: whitesmoke;
            border-radius: 5px;
            border: #2583a8;
        }

        #btn-diff:hover {
            box-shadow: 0 12px 16px 0 rgba(0,0,0,0.24),0 17px 50px 0 rgba(0,0,0,0.19);
            background-color: whitesmoke;
            color: black;
            border: 2px solid #2583a8;
        }
    </style>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <br>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <ul class="list-group" style="width: 50%">
                <li class="list-group-item list-group-item-info" style="text-align: center; font-size: 17px;">{{ __('cash_book.receipt') }}</li>
                <li class="list-group-item">
                    <div class="clearfix">
                        <button id="btn-fee">{{ __('cash_book.fee') }}</button>
                        <button id="btn-diff">{{ __('cash_book.diff') }}</button>
                    </div>
                </li>
            </ul>


        </div><!-- /.container-fluid -->
    </section>
    <script>

        $("#btn-fee").on('click', function () {
            window.location.href = "{{ route('chart.receipt-fee') }}";
        });

    </script>
@stop
