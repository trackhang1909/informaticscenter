@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{__('msg.positionlist')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh">
                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
                <button type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalThem">
                    {{__('msg.addposition')}}
                </button>

                <form class="form-container form-add" action="/position/add" method="POST">
                    <div class="modal fade" id="exampleModalThem" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('msg.addposition')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{ __('msg.posname') }} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInputPassword1"
                                               placeholder="">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{ __('msg.no') }}</button>
                                    <button type="submit" class="btn btn-primary"
                                            onclick="save()">{{ __('msg.add') }}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col" style="text-align: center">{{__('msg.number')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.posname')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.code')}}</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $position)
                            <tr>
                                <th scope="row" style="text-align: center">{{ $data->firstItem() + $item }}</th>
                                <td style="text-align: center">{{ $position->name }}</td>
                                <td style="text-align: center">{{ $position->code }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-success" data-toggle="modal"
                                            data-target="#exampleModalCn{{ $position->id }}">
                                        {{__('msg.update')}}
                                    </button>
                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                                            data-target="#exampleModalXoa{{ $position->id }}">
                                        {{__('msg.delete')}}
                                    </button>
                                    <form class="form-container" action="/position/delete" method="GET">
                                        <div class="modal fade" id="exampleModalXoa{{ $position->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('msg.position')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="text" name="id"
                                                                   class="form-control" id="exampleInputStt"
                                                                   value="{{ $position->id }}">
                                                        </div>
                                                        <p>{{__('action.confirm_delete')}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.yes')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <form class="form-container form-add" action="/position/add" method="POST">
                                        <div class="modal fade" id="exampleModalCn{{ $position->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">{{__('msg.update')}}</h5>

                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none;" type="number" name="id" class=" id-position form-control"
                                                                   id="exampleInputStt" value="{{ $position->id }}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput">{{ __('msg.posname') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="name" class="form-control"
                                                                   id="exampleInput" value="{{ $position->name }}">
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.update')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clearfix">
                <div class="page" style="float: right">
                    {{ $data->links() }}
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                            remote: "/unique-position?id=" + $(this).find('.id-position').first().val(),

                        },
                        birthday: {
                            required: true,
                        },
                        email: {
                            required: true,
                        },
                        phone_number: {
                            required: true,
                            maxlength: 11,
                        },
                        country: {
                            required: true,
                        },
                        start_time: {
                            required: true,
                        },
                        salary: {
                            required: true,
                        },
                        position: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("msg.posname").__("action.required") ?>",
                            remote: "<?php echo __("msg.posexisted") ?>",


                        },
                        birthday: {
                            required: "<?php echo __("msg.birthday").__("action.required") ?>",
                        },
                        email: {
                            required:"<?php echo __("msg.email").__("action.required") ?>"
                        },
                        phone_number: {
                            required: "<?php echo __("msg.phonenumber").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.phonenumber").' '.__("action.maxlength11") ?>",
                        },
                        country: {
                            required: "<?php echo __("msg.country").__("action.required")?>",
                        },
                        start_time:{
                            required: "<?php echo __("msg.datestarwork").__("action.required") ?>",
                        },
                        salary: {
                            required: "<?php echo __("msg.salary").__("action.required")?>",
                        },
                        position: {
                            required: "<?php echo __("msg.position").__("action.required")?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/position/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'position-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('msg.positionempty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("position.detail", ":name") }}';
                            url = url.replace(':name', data.name);
                            return '<a href="' + url + '" class="list-group-item">' + data.name + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
