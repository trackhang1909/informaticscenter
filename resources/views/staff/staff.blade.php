@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{__('msg.stafflist')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh">

                @if(session()->get('success'))
                        <div class="alert alert-success abc">
                            {{ session()->get('success') }}
                        </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
                <button type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalThem">
                    {{__('msg.addstaff')}}
                </button>
                <form class="form-container form-add" action="/staff/add" method="POST">
                    <div class="modal fade" id="exampleModalThem" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('msg.addstaff')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{ __('msg.name') }} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInputPassword1"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput">{{ __('msg.birthday') }} <label class="required-p">*</label></label>
                                        <input type="datetime-local" name="birthday" class="form-control" id="exampleInput"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1">{{ __('msg.email') }} <label class="required-p">*</label></label>
                                        <input type="text" name="email" class="form-control" id="exampleInput1" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{ __('msg.phonenumber') }} <label class="required-p">*</label></label>
                                        <input type="text" name="phone_number" class="form-control" id="exampleInput2" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{ __('msg.country') }} <label class="required-p">*</label></label>
                                        <input type="text" name="country" class="form-control" id="exampleInput3"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{ __('msg.datestarwork') }} <label class="required-p">*</label></label>
                                        <input type="datetime-local" name="start_time" class="form-control" id="exampleInput4"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput5">{{ __('msg.salary') }} <label class="required-p">*</label></label>
                                        <input type="text" name="salary" class="form-control" id="exampleInput5"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleFormControlSelect1">{{ __('msg.position') }} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleFormControlSelect1" name="position">
                                            <option hidden>{{''}}</option>
                                            @foreach($pos as $position)
                                                <option value="{{$position->code}}">{{$position->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{__('msg.no')}}</button>
                                    <button type="submit" class="btn btn-primary"
                                            onclick="save()">{{__('msg.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col" style="text-align: center">{{__('msg.number')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.name')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.email')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.phonenumber')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.position')}}</th>
                            <th id=""></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $staff)
                            <tr>
                                <th scope="row" style="text-align: center">{{ $data->firstItem()+ $item }}</th>
                                <td style="text-align: center">{{ $staff->name }}</td>
                                <td style="text-align: center">{{ $staff->email }}</td>
                                <td style="text-align: center">{{ $staff->phone_number }}</td>
                                <td style="text-align: center">{{ $staff->positionname }}</td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#exampleModalXem{{ $staff->id }}">
                                        {{__('msg.detail')}}
                                    </button>
                                    <button type="button" class="btn btn-xs btn-success" data-toggle="modal"
                                            data-target="#exampleModalCn{{ $staff->id }}">
                                        {{__('msg.update')}}
                                    </button>
                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                                            data-target="#exampleModalXoa{{ $staff->id }}">
                                        {{__('msg.delete')}}
                                    </button>

                                    <form class="form-container" action="/staff/delete" method="GET">
                                        <div class="modal fade" id="exampleModalXoa{{ $staff->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('msg.staff')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="text" name="id"
                                                                   class="form-control" id="exampleInputStt"
                                                                   value="{{ $staff->id }}">
                                                        </div>
                                                        <p>{{__('action.confirm_delete')}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.yes')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                    <div class="modal fade" id="exampleModalXem{{ $staff->id }}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"
                                                        style="text-align: center">{{__('msg.info')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.name')}} :</i><b> {{ $staff->name }}</b></h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.birthday')}} :</i><b> {{ $staff->birthday }}</b>
                                                    </h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.email')}} :</i><b> {{ $staff->email }}</b></h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.phonenumber')}} :</i><b> {{ $staff->phone_number }}</b></h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.country')}} :</i><b> {{ $staff->country }}</b>
                                                    </h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.datestarwork')}}:</i><b> {{ $staff->start_time }}</b></h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.salary')}} :</i><b> {{ $staff->salary }}</b>
                                                    </h5>
                                                </div>
                                                <div class="modal-body ">
                                                    <h5><i>{{__('msg.position')}} :</i><b> {{ $staff->positionname }}</b>
                                                    </h5>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <form class="form-container form-add" action="/staff/add" method="POST">
                                        <div class="modal fade" id="exampleModalCn{{ $staff->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">{{__('msg.update')}}</h5>

                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="number" name="id" class="id-staff form-control"
                                                                   id="exampleInputStt" value="{{ $staff->id }}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput">{{ __('msg.name') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="name" class="form-control"
                                                                   id="exampleInput" value="{{ $staff->name }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput1">{{ __('msg.birthday') }} <label class="required-p">*</label></label>
                                                            <input type="datetime-local" name="birthday" class="form-control"
                                                                   id="exampleInput1"
                                                                   value="{{ date_format(new DateTime($staff->birthday), 'Y-m-d\TH:i')}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput2">{{ __('msg.email') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="email" class="form-control"
                                                                   id="exampleInput2" value="{{ $staff->email }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput3">{{ __('msg.phonenumber') }} <label class="required-p">*</label></label>
                                                            <input type="number" name="phone_number" class="form-control"
                                                                   id="exampleInput3" value="{{ $staff->phone_number }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput4">{{ __('msg.country') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="country" class="form-control"
                                                                   id="exampleInput4" value="{{ $staff->country }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput5">{{ __('msg.datestarwork') }} <label class="required-p">*</label></label>
                                                            <input type="datetime-local" name="start_time"
                                                                   class="form-control" id="exampleInput5"
                                                                   value="{{ date_format(new DateTime($staff->start_time), 'Y-m-d\TH:i' ) }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput6">{{ __('msg.salary') }} <label class="required-p">*</label></label>
                                                            <input type="number" name="salary" class="form-control"
                                                                   id="exampleInput6" value="{{ $staff->salary }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleFormControlSelect1">{{ __('msg.position') }} <label class="required-p">*</label></label>
                                                            <select class="form-control" id="exampleFormControlSelect1" name="position" value="{{ $staff->position }}">
                                                                <option value="{{ $staff->position }}">{{ $staff->positionname }}</option>
                                                                @foreach($pos as $position)
                                                                    <option value="{{ $position->code }}">{{ $position->name }}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.update')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clearfix">
                <div style="float: right">
                    {{ $data->links() }}
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        birthday: {
                            required: true,
                        },
                        email: {
                            required: true,
                            remote: "/unique-staff?id=" + $(this).find('.id-staff').first().val(),
                        },
                        phone_number: {
                            required: true,
                            maxlength: 11,
                            remote: "/unique-staff?id=" + $(this).find('.id-staff').first().val(),
                        },
                        country: {
                            required: true,
                        },
                        start_time: {
                            required: true,
                        },
                        salary: {
                            required: true,
                        },
                        position: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("msg.name").__("action.required") ?>",
                        },
                        birthday: {
                            required: "<?php echo __("msg.birthday").__("action.required") ?>",
                        },
                        email: {
                            required:"<?php echo __("msg.email").__("action.required") ?>",
                            remote: "<?php echo __("msg.emailexist")?>",
                        },
                        phone_number: {
                            required: "<?php echo __("msg.phonenumber").__("action.required") ?>",
                            maxlength: "<?php echo __("msg.phonenumber").' '.__("action.maxlength11") ?>",
                            remote: "<?php echo __('msg.phoneexist') ?>",
                        },
                        country: {
                            required: "<?php echo __("msg.country").__("action.required")?>",
                        },
                        start_time:{
                            required: "<?php echo __("msg.datestarwork").__("action.required") ?>",
                        },
                        salary: {
                            required: "<?php echo __("msg.salary").__("action.required")?>",
                        },
                        position: {
                            required: "<?php echo __("msg.position").__("action.required")?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/staff/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'staff-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('msg.bookempty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("staff.detail", ":name") }}';
                            url = url.replace(':name', data.name);
                            return '<a href="' + url + '" class="list-group-item">' + data.name + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
