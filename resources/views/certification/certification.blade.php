@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%", no_results_text: "{{ __('class.empty_campus2') }}" });
    </script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('certification.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">

                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif

                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif

            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center"><input type="checkbox" id="check-all"></th>
                                <th style="text-align: center" scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{__('certification.code')}}</th>
                                <th scope="col">{{__('certification.student')}}</th>
                                <th scope="col">{{__('certification.course')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item => $certification)
                                <tr>
                                    <td class="align-middle" style="text-align: center"><input type="checkbox" class="btn-check"></td>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td class="align-middle">{{ $certification->code }}</td>
                                    <td class="align-middle" id="index">
                                        <p>{{ $certification->student_name }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $certification->phone_number.' - '.$certification->email }}</p>
                                    </td>
                                    <td class="align-middle" id="index">
                                        <p>{{ $certification->course_name.' ('.__('course.group').' '.$certification->group.')' }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $certification->type }}</p>
                                    </td>
                                    <td class="align-middle">
                                        <a class="btn btn-xs btn-info" href="{{ route('certification.show', $certification->id) }}" role="button">{{ __('action.detail') }}</a>
                                        @can('update')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $certification->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $certification->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @endcan

                                        @can('delete')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $certification->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $certification->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @endcan

                                        <form class="form-container" action="{{ route('certification.destroy', $certification->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $certification->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('certification.certification')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $certification->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form class="form-container form-up" action="{{ route('certification.edit', $certification->id) }}" method="GET">
                                            <div class="modal fade" id="exampleModalUpdate{{ $certification->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('certification.certification')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $certification->id }}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput1">{{__('certification.test_date')}} <label class="required-p">*</label></label>
                                                                <input type="date" name="test_date" class="form-control" id="exampleInput1" value="{{ $certification->test_date }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput2">{{__('certification.rank')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="rank" class="form-control" id="exampleInput2" value="{{ $certification->rank }}" >
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput3">{{__('certification.point')}} <label class="required-p">*</label></label>
                                                                <input type="number" step="0.01" name="point" class="form-control" id="exampleInput3" value="{{ $certification->point }}" >
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/certification/find/%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'certification-code',
                    limit: 20,
                    display: function(data) {
                        return data.code;
                    },
                    templates: {
                        empty: [
                            '<div style="width: 250px" class="list-group search-results-dropdown"><div class="list-group-item">{{ __('certification.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{!! route("certification.detail", ":input") !!}';
                            url = url.replace(':input', data.id);
                            return '<a style="width: 200%" href="' + url + '" class="list-group-item">' + data.student_name + ' (' + data.phone_number + ')' + ' - ' + data.course_name + '{{ ' ('.__('course.group').' ' }}' + data.group + ')' + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>

@stop
