<style>
    p {
        margin-top: 1px;
        margin-bottom: 1px;
    }
    .border-dotter {
        border-bottom: 1px dotted;
    }
</style>

<div style="font-family: DejaVu Sans, sans-serif; border: 8px inset #1C6EA4; margin:  0 10% 0 10%; background-color: #ebf4ff;">
    <div style="margin-bottom: 15%">
        <div class="clearfix" style="margin: 20px 20px 10px 20px;">
            <img width="252" height="60" src="https://thienquantech.com/wp-content/uploads/2020/02/logo-hitech-thien-quan.png" class="header-logo-dark" alt="">
            <div style="float: right; color: #1C6EA4">
                <p>Số hiệu chứng nhận: {{ $certification->code }}</p>
                <p>Ngày: {{ date('d/m/Y', strtotime($certification->create_date)) }}</p>
            </div>
        </div>
        <div style="text-align: center; padding: 10px">
            <p style="font-size: 25px; color: #1C6EA4; margin-bottom: 2px"><b>TRUNG TÂM TIN HỌC VÀ NGOẠI NGỮ THIÊN QUÂN</b></p>
            <hr style="width: 50%">
            <p style="color: darkred; font-size: 35px">CHỨNG NHẬN</p>
            <b style="font-size: 20px; text-transform: uppercase;">{{ $certification->course_name }}</b>
        </div>
        <table class="table table-borderless" style="width: 90%; margin: 0 auto;">
            <tbody>
                <tr>
                    <td width="19%">Cấp cho:</td>
                    <td><p class="border-dotter" style="text-align: center">{{ $certification->student_name }}</p></td>
                    <td width="19%" style="padding-left: 20px">Sinh ngày:</td>
                    <td><p class="border-dotter" style="text-align: center">{{ date('d/m/Y', strtotime($certification->birthday)) }}</p></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-borderless" style="width: 90%; margin: 0 auto;">
            <tbody>
                <tr>
                    <td width="19%">Địa chỉ:</td>
                    <td colspan="3"><p class="border-dotter" style="text-align: center">{{ $certification->address }}</p></td>
                </tr>
            </tbody>
        </table>
        <table class="table table-borderless" style="width: 90%; margin: 0 auto;">
            <tbody>
                @php
                    /** @var TYPE_NAME $certification */
                    $open_time = \Carbon\Carbon::parse(new DateTime($certification->open_time));
                    $close_time = \Carbon\Carbon::parse(new DateTime($certification->close_time));
                    $now = \Carbon\Carbon::now();
                @endphp
                <tr>
                    <td colspan="4">Đã hoàn thành khóa học từ ngày {{ $open_time->day }} tháng {{ $open_time->month }} năm {{ $open_time->year }} đến ngày {{ $close_time->day }} tháng {{ $close_time->month }} năm {{ $close_time->year }}</td>
                </tr>
                <tr>
                    <td width="19%">Ngày kiểm tra:</td>
                    <td colspan="3"><p class="border-dotter" style="text-align: center">{{ date('d/m/Y', strtotime($certification->test_date)) }}</p></td>
                </tr>
                <tr>
                    <td>Xếp loại:</td>
                    <td colspan="3"><p class="border-dotter" style="text-align: center">{{ $certification->rank }} (Điểm: {{ number_format($certification->point, 1) }})</p></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td style="text-align: right">TP.HCM, ngày {{ $now->day }} tháng {{ $now->month }} năm {{ $now->year }}</td>
                </tr>
                <tr>
                    <td width="19%"></td>
                    <td width="19%"></td>
                    <td width="19%"></td>
                    <td style="text-align: center">Giám đốc</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
