@extends('layout.master')
@section('content')

    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @include('certification.certification_preview')
            <div class="clearfix">
                <div style="margin: 20px 10% 20px 10%; float: right">
                    <button onclick="window.history.back()" class="btn btn-primary"><i class="fa fa-angle-double-left"></i></button>
                    @can('create')
                        <a class="btn btn-primary" href="{{ route('certification.export', $certification->id) }}" role="button">Xuất chứng nhận</a>
                    @elsecan('superAdmin')
                        <a class="btn btn-primary" href="{{ route('certification.export', $certification->id) }}" role="button">Xuất chứng nhận</a>
                    @endcan
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
