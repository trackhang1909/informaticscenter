<style>
    p {
        margin-top: 1px;
        margin-bottom: 1px;
    }
</style>
<div id="confirm-cer" style="margin: 20px 0">
    <div class="list-group">
        <button type="button" class="list-group-item list-group-item-primary">
            Thông tin chứng nhận
        </button>

        @foreach($studentList as $student)
            <ul class="list-group list-group-horizontal">
                <li style="width: 40%" class="rounded-0 list-group-item">
                    <p>{{ $student->name }} ({{ date('d/m/Y', strtotime($student->birthday)) }})</p>
                    <p style="color: grey; font-size: 13px">{{ $student->phone_number }}</p>
                </li>
                <li style="width: 30%" class="list-group-item">
                    <input id="test-date{{ $student->id }}" style="margin-top: 3px" placeholder="{{ __('certification.test_date') }}" class="textbox-n form-control" type="text" onfocus="(this.type='date')">
                </li>
                <li style="width: 15%" class="list-group-item">
                    <input id="rank{{ $student->id }}" style="margin-top: 3px" type="text" class="form-control" placeholder="{{ __('certification.rank') }}">
                </li>
                <li style="width: 15%" class="rounded-0 list-group-item">
                    <input id="point{{ $student->id }}" style="margin-top: 3px" type="number" step="0.01" class="form-control" placeholder="{{ __('certification.point') }}">
                </li>
            </ul>
        @endforeach

        <ul class="list-group list-group-horizontal">
            <li style="width: 100%" class="rounded-0 list-group-item">
                <button style="float: right" id="btn-confirm" type="button" class="btn btn-primary">Xác nhận</button>
            </li>
        </ul>

    </div>
</div>
<script type="text/javascript">
    $("#btn-confirm").on('click', function () {
        var count = 0;
        var testDate = [];
        var rank = [];
        var point = [];
        var studentId = [];
        var input = {};
        @foreach($studentList as $student)
            if ($('#test-date{{ $student->id }}').val() === "" | $('#rank{{ $student->id }}').val() === "" | $('#point{{ $student->id }}').val() === "") {
                count++;
            }
            testDate.push($('#test-date{{ $student->id }}').val());
            rank.push($('#rank{{ $student->id }}').val());
            point.push($('#point{{ $student->id }}').val());
            studentId.push({{ $student->id }});
        @endforeach
        if(count > 0) {
            alertify.error('Thông tin không đầy đủ');
        }
        else {
            input['testDate'] = testDate;
            input['rank'] = rank;
            input['point'] = point;
            input['studentId'] = studentId;
            input['courseId'] = {{ $courseId }};
            $.ajax({
                url: "{{ route('certification.store') }}",
                type: 'POST',
                data: {'data': input, '_token' : '<?=csrf_token()?>'},
                success : function(response) {
                    window.location = response;
                },
                error : function(e){
                    console.log(e);
                }
            });
        }
    });
</script>
