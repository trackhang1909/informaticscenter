@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('course.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">

                @can('create')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('course.add_course')}}
                    </button>
                @elsecan('superAdmin')
                    <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                        {{__('course.add_course')}}
                    </button>
                @endcan

                <form class="form-container form-add" action="{{ route('course.store') }}" method="POST">
                    <div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput0">{{__('course.name')}} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="name-course form-control" id="exampleInput0" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput5">{{__('course.type')}} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput5" name="type">
                                            <option value="{{__('course.type_online')}}">{{__('course.type_online')}}</option>
                                            <option value="{{__('course.type_offline')}}">{{__('course.type_offline')}}</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{__('course.group')}} <label class="required-p">*</label></label>
                                        <input type="number" name="group" class="form-control" id="exampleInput3" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput">{{__('course.fees')}} <label class="required-p">*</label></label>
                                        <input type="number" name="fees" class="form-control" id="exampleInput" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1">{{__('course.time')}} <label class="required-p">*</label></label>
                                        <input type="date" name="open_time" class="form-control" id="exampleInput1" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{__('course.close_time')}} <label class="required-p">*</label></label>
                                        <input type="date" name="close_time" class="form-control" id="exampleInput2" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{__('course.content')}} <label class="required-p">*</label></label>
                                        <textarea class="form-control summary-ckeditor" id="content" name="content"></textarea>
                                        <label for="content" class="error"></label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('action.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center"><input type="checkbox" id="check-all"></th>
                                <th style="text-align: center" scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{__('course.name')}}</th>
                                <th style="text-align: center" scope="col">{{__('course.group')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item => $course)
                                <tr>
                                    <td class="align-middle" style="text-align: center"><input type="checkbox" class="btn-check"></td>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td class="align-middle" id="index">
                                        <p>{{ $course->name }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $course->type }}</p>
                                    </td>
                                    <td class="align-middle" style="text-align: center">{{ $course->group }}</td>
                                    <td class="align-middle">
                                        <a class="btn btn-xs btn-secondary" href="{{ route('course.student-list', $course->id) }}" role="button">{{ __('course.list_std') }}</a>
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModalDetail{{ $course->id }}">
                                            {{__('action.detail')}}
                                        </button>

                                        @can('update')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $course->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $course->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @endcan

                                        @can('delete')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $course->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $course->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @endcan

                                        <form class="form-container" action="{{ route('course.destroy', $course->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $course->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @method('DELETE')
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInput" value="{{ $course->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <div class="modal fade" id="exampleModalDetail{{ $course->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body text-content">
                                                        <div class="form-group">
                                                            <p>{{__('course.name')}}: {{ $course->name }}</p>
                                                            <p>{{__('course.group')}}: {{ $course->group }}</p>
                                                            <p>{{__('course.fees')}}: {{ number_format($course->fees,0,',','.').' '.__('course.currency') }}</p>
                                                            <p>{{__('course.type')}}: {{ $course->type }}</p>
                                                            <p>{{__('course.time')}}: {{ date('d-m-Y', strtotime($course->open_time)) }}</p>
                                                            <p>{{__('course.close_time')}}: {{ date('d-m-Y', strtotime($course->close_time)) }}</p>
                                                            <p>{{__('course.content')}}:</p>
                                                            <textarea class="form-control summary-ckeditor" id="summary-ckeditor" name="content">
                                                                {{ $course->content }}
                                                            </textarea>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form class="form-container form-add" action="{{ route('course.store') }}" method="POST">
                                            <div class="modal fade" id="exampleModalUpdate{{ $course->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('course.course')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="stt form-control" id="exampleInputS" value="{{ $course->id }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput0">{{__('course.name')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="name" class="form-control" id="exampleInput0" value="{{ $course->name }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput3">{{__('course.group')}} <label class="required-p">*</label></label>
                                                                <input type="number" name="group" class="form-control" id="exampleInput3" value="{{ $course->group }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput">{{__('course.fees')}} <label class="required-p">*</label></label>
                                                                <input type="number" name="fees" class="form-control" id="exampleInput" value="{{ $course->fees }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput5">{{__('course.type')}} <label class="required-p">*</label></label>
                                                                <select class="form-control" id="exampleInput5" name="type">
                                                                    <option value="{{ $course->type }}">{{ $course->type }}</option>
                                                                    @if(__('course.type_online') === $course->type)
                                                                        <option value="{{__('course.type_offline')}}">{{__('course.type_offline')}}</option>
                                                                    @else
                                                                        <option value="{{__('course.type_online')}}">{{__('course.type_online')}}</option>
                                                                    @endif
                                                                </select>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput1">{{__('course.time')}} <label class="required-p">*</label></label>
                                                                <input type="date" name="open_time" class="form-control" id="exampleInput1" value="{{ $course->open_time }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput2">{{__('course.close_time')}} <label class="required-p">*</label></label>
                                                                <input type="date" name="close_time" class="form-control" id="exampleInput2" value="{{ $course->close_time }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput4">{{__('course.content')}} <label class="required-p">*</label></label>
                                                                <textarea class="form-control summary-ckeditor" id="exampleInput4" name="content">{{ $course->content }}</textarea>
                                                                <label for="exampleInput4" class="error"></label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            $("form.form-add").each(function() {
                $(this).validate({
                    ignore: [],
                    debug: false,
                    rules: {
                        name: {
                            required: true,
                        },
                        fees: {
                            required: true,
                            maxlength: 11,
                        },
                        open_time: {
                            required: true,
                        },
                        close_time: {
                            required: true,
                        },
                        group: {
                            required: true,
                        },
                        content: {
                            required: function()
                            {
                                CKEDITOR.instances.content.updateElement();
                            },
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("course.name").__("action.required") ?>",
                        },
                        fees: {
                            required: "<?php echo __("course.fees").__("action.required") ?>",
                            maxlength: "<?php echo __("course.fees").' '.__("action.maxlength11") ?>",
                        },
                        open_time: {
                            required: "<?php echo __("course.time").__("action.required") ?>",
                        },
                        close_time: {
                            required: "<?php echo __("course.close_time").__("action.required") ?>",
                        },
                        group: {
                            required: "<?php echo __("course.group").__("action.required") ?>",
                        },
                        content: {
                            required: "<?php echo __("course.content").__("action.required") ?>"
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/course/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'course-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('course.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("course.detail", ":id") }}';
                            url = url.replace(':id', data.id);
                            return '<a href="' + url + '" class="list-group-item">' + data.name + '</a>';                        }
                    }
                },
            ]);

        });
    </script>
@stop
