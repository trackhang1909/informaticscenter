<style>
    p {
        margin-top: 1px;
        margin-bottom: 1px;
    }
    br {
        margin-top: 1px;
        margin-bottom: 1px;
        margin-left: 0;
        margin-right: 0;
    }
    .col-left {
        width: 30%;
    }
    .col-right {
        width: 70%;
    }
    .border-dotter {
        border-bottom: 1px dotted;
    }
</style>

<div style="font-family: DejaVu Sans, sans-serif; border: ridge; margin: 0 10% 0 10%; font-size: 16px">
    <div style="margin: 10px 10px 20% 10px">
        <div class="clearfix">
            <p><b>Đơn vị:</b> Trung tâm Tin Học và Ngoại ngữ Thiên Quân</p>
            <p style="float: right">Mã phiếu: {{ $receipt->code }}</p>
            <p><b>Địa chỉ:</b> 1234, AAA</p>
        </div>
        <p style="text-align: center; font-size: 30px; margin-bottom: 0px"><b>Phiếu Thu</b></p>
        @php /** @var TYPE_NAME $receipt */
            $now = \Carbon\Carbon::parse(date($receipt->register_time)) @endphp
        <p style="text-align: center">Ngày {{ $now->day }} tháng {{ $now->month }} năm {{ $now->year }}</p>
        <div class="clearfix">
            <table class="table table-borderless table-sm" width="100%" style="font-size: 16px; margin: 0 auto">
                <tr class="table-light">
                    <td class="col-left">Họ và tên học viên:</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ $receipt->name }}</p>
                    </td>
                </tr>
                <tr>
                    <td class="col-left">Địa chỉ:</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ $receipt->address }}</p>
                    </td>
                </tr>
                <tr class="table-light">
                    <td class="col-left">Lý do nộp:</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ $receipt->reason }}</p>
                    </td>
                </tr>
                <tr>
                    <td class="col-left">Môn học đăng ký:</td>
                    <td class="col-right">
                        <p class="border-dotter">
                            @php $total = 0; @endphp
                            @for($i = 0; $i < sizeof($courses); $i++)
                                {{ $courses[$i]->name }} ({{ number_format($courses[$i]->fees,0,',','.') }} VND)
                                @php /** @var TYPE_NAME $courses */
                                     /** @var TYPE_NAME $total */
                                    $total += $courses[$i]->fees @endphp
                                @if($i < sizeof($courses) - 1)
                                    ,
                                @endif
                            @endfor
                        </p>
                    </td>
                </tr>
                <tr class="table-light">
                    <td class="col-left">Tổng học phí:</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ number_format($receipt->total_money,0,',','.') }} VND</p>
                    </td>
                </tr>
                <tr>
                    <td class="col-left">Số tiền:</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ number_format($receipt->advance_money,0,',','.') }} VND</p>
                    </td>
                </tr>
                <tr class="table-light">
                    <td class="col-left">(Viết bằng chữ):</td>
                    <td class="col-right">
                        <p class="border-dotter">{{ $receipt->money_text }}</p>
                    </td>
                </tr>
            </table>
        </div>
        <p style="text-align: right">Ngày {{ $now->day }} tháng {{ $now->month }} năm {{ $now->year }}</p>
        <div class="clearfix">
            <p style="float: right; padding-right: 20px"><b>Người lập phiếu</b></p>
            <p style="float: left; padding-left: 20px"><b>Người nộp tiền</b></p>
        </div>
    </div>
</div>

