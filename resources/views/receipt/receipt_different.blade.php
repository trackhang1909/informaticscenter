@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('receipt.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">

                <button type="button" class="btn btn-primary btn-header" data-toggle="modal" data-target="#exampleModalAdd">
                    {{__('receipt.add')}}
                </button>

                <form class="form-container form-add" action="{{ route('receipt-different.store') }}" method="POST">
                    <div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('receipt.info')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput1">{{__('receipt.name')}} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInput1" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{__('receipt.address')}} <label class="required-p">*</label></label>
                                        <input type="text" name="address" class="form-control" id="exampleInput2" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{__('receipt.reason')}} <label class="required-p">*</label></label>
                                        <input type="text" name="reason" class="form-control" id="exampleInput3" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{__('receipt.total_money2')}} <label class="required-p">*</label></label>
                                        <input type="number" name="total_money" class="form-control" id="exampleInput4" placeholder="" >
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput5">{{__('receipt.money_text')}} <label class="required-p">*</label></label>
                                        <input type="text" name="money_text" class="form-control" id="exampleInput5" placeholder="" >
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('receipt.create')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center"><input type="checkbox" id="check-all"></th>
                                <th scope="col" style="text-align: center">{{__('action.id')}}</th>
                                <th scope="col">{{__('receipt.code')}}</th>
                                <th scope="col">{{__('receipt.name')}}</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($data as $item => $receipt)
                                <tr>
                                    <td class="align-middle" style="text-align: center"><input type="checkbox" class="btn-check"></td>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td class="align-middle">{{ $receipt->code }}</td>
                                    <td class="align-middle" id="index">
                                        <p>{{ $receipt->name }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $receipt->address }}</p>
                                    </td>
                                    <td class="align-middle">
                                        <button type="button" class="receipt btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal{{ $receipt->id }}" data-id="{{ $receipt->id }}">
                                            {{ __('action.review_receipt') }}
                                        </button>

                                        <div class="modal fade" id="exampleModal{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('student.info_receipt') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                    <div class="modal-footer" style="width: 100%; margin-right: 10%; margin-bottom: 20px">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                        <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt-different.review', $receipt->id).'?export=true' }}" role="button">{{ __('student.export_save') }}</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $receipt->id }}">
                                            {{__('action.update')}}
                                        </button>

                                        <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $receipt->id }}">
                                            {{__('action.delete')}}
                                        </button>

                                        <form class="form-container" action="{{ route('receipt-different.destroy', $receipt->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('student.info_receipt') }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $receipt->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form class="form-container form-add" action="{{ route('receipt-different.store') }}" method="POST">
                                            <div class="modal fade" id="exampleModalUpdate{{ $receipt->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{ __('student.info_receipt') }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInput" value="{{ $receipt->id }}" readonly>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput1">{{__('receipt.name')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="name" class="form-control" id="exampleInput1" value="{{ $receipt->name }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput2">{{__('receipt.address')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="address" class="form-control" id="exampleInput2" value="{{ $receipt->address }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput3">{{__('receipt.reason')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="reason" class="form-control" id="exampleInput3" value="{{ $receipt->reason }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput4">{{__('receipt.total_money2')}} <label class="required-p">*</label></label>
                                                                <input type="number" name="total_money" class="form-control" id="exampleInput4" value="{{ $receipt->total_money }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput5">{{__('receipt.money_text')}} <label class="required-p">*</label></label>
                                                                <input type="text" name="money_text" class="form-control" id="exampleInput5" value="{{ $receipt->money_text }}">
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            //Check all checkbox
            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Review receipt
            $(".receipt").each(function () {
                $(this).on('click', function () {
                    var receipt_id = $(this).data('id');
                    console.log(receipt_id);
                    var url = '{{ route("receipt-different.review", ":input") }}';
                    url = url.replace(':input', receipt_id);
                    var id = $(this).data('target');
                    $.ajax({
                        url: url,
                        type: "GET",
                    }).done(function (response) {
                        $(id).find(".modal-body").first().empty();
                        $(id).find(".modal-body").first().html(response);
                    });
                })
            });

            $("form.form-add").each(function() {
                $(this).validate({
                    rules: {
                        name: {
                            required: true,
                        },
                        address: {
                            required: true,
                        },
                        reason: {
                            required: true,
                        },
                        total_money: {
                            required: true,
                        },
                        money_text: {
                            required: true,
                        },
                    },
                    messages: {
                        name: {
                            required: "<?php echo __("receipt.name").__("action.required") ?>",
                        },
                        address: {
                            required: "<?php echo __("receipt.address").__("action.required") ?>",
                        },
                        reason: {
                            required: "<?php echo __("receipt.reason").__("action.required") ?>",
                        },
                        total_money: {
                            required: "<?php echo __("receipt.total_money2").__("action.required") ?>",
                        },
                        money_text: {
                            required: "<?php echo __("receipt.money_text").__("action.required") ?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/receipt-different/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'receipt-different-code',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('receipt.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{!! route("receipt-different.detail", ":input") !!}';
                            url = url.replace(':input', data.id);
                            return '<a style="width: 250px" href="' + url + '" class="list-group-item">' + data.name + ' (' + data.address + ')' + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
