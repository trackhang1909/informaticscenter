@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%" });
    </script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('student.register_course') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <div class="clearfix header-course">
                @can('superAdmin')
                    <a class="btn btn-primary" href="{{ route('student.register-index') }}" role="button">{{ __('action.register_course') }}</a>
                @elsecan('create')
                    <a class="btn btn-primary" href="{{ route('student.register-index') }}" role="button">{{ __('action.register_course') }}</a>
                @endcan
                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>

                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th style="text-align: center"><input type="checkbox" id="check-all"></th>
                                <th style="text-align: center" scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{__('receipt.code')}}</th>
                                <th scope="col">{{__('student.student')}}</th>
                                <th scope="col">{{__('course.course')}}</th>
                                <th></th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($receipt as $item => $data)
                                <tr>
                                    <td class="align-middle" style="text-align: center"><input type="checkbox" class="btn-check"></td>
                                    <th class="align-middle" style="text-align: center" scope="row">{{ $receipt->firstItem() + $item }}</th>
                                    <td class="align-middle">{{ $data->code }}</td>
                                    <td class="align-middle" id="index">
                                        <p>{{ $data->name }}</p>
                                        <p style="color: grey; font-size: 13px">{{ $data->phone_number.' - '.$data->email }}</p>
                                    </td>
                                    <td class="align-middle">{{ $data->course_name }}</td>
                                    <td class="align-middle">
                                        <button type="button" class="receipt btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModal{{ $data->id }}" data-id="{{ $data->id }}">
                                            {{ __('action.review_receipt') }}
                                        </button>

                                        <div class="modal fade" id="exampleModal{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{ __('student.info_receipt') }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body"></div>
                                                    <div class="modal-footer" style="width: 100%; margin-right: 10%; margin-bottom: 20px">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('action.close') }}</button>
                                                        @can('create')
                                                            <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt.review', $data->id).'?export=true' }}" role="button">{{ __('student.export_save') }}</a>
                                                        @elsecan('superAdmin')
                                                            <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt.review', $data->id).'?export=true' }}" role="button">{{ __('student.export_save') }}</a>
                                                        @elsecan('update', \App\Models\Student::find($data->student_id))
                                                            <a style="margin-right: 10%" class="btn btn-primary" href="{{ route('receipt.review', $data->id).'?export=true' }}" role="button">{{ __('student.export_save') }}</a>
                                                        @endcan
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        @can('update')
                                            <a class="btn btn-xs btn-success" href="{{ route('student.register-index').'?courses='.$data->course_id.'&student_id='.$data->student_id.'&receipt_id='.$data->id }}" role="button">{{ __('action.update') }}</a>
                                        @elsecan('superAdmin')
                                            <a class="btn btn-xs btn-success" href="{{ route('student.register-index').'?courses='.$data->course_id.'&student_id='.$data->student_id.'&receipt_id='.$data->id }}" role="button">{{ __('action.update') }}</a>
                                        @endcan

                                        @can('delete')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $data->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $data->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @endcan

                                        <form class="form-container" action="{{ route('receipt.destroy', $data->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $data->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('student.info_regis')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $data->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $receipt->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <script type="text/javascript">
        $(document).ready(function() {

            //Check all checkbox
            $("#check-all").click(function () {
                $('input:checkbox').not(this).prop('checked', this.checked);
            });

            //Review receipt
            $(".receipt").each(function () {
                $(this).on('click', function () {
                    var receipt_id = $(this).data('id');
                    console.log(receipt_id);
                    var url = '{{ route("receipt.review", ":input") }}';
                    url = url.replace(':input', receipt_id);
                    var id = $(this).data('target');
                    $.ajax({
                        url: url,
                        type: "GET",
                    }).done(function (response) {
                        $(id).find(".modal-body").first().empty();
                        $(id).find(".modal-body").first().html(response);
                    });
                })
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/receipt/find?value=%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'receipt-code',
                    limit: 20,
                    display: function(data) {
                        return data.code;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('receipt.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{!! route("receipt.detail", ":input") !!}';
                            url = url.replace(':input', data.id);
                            return '<a style="width: 200%" href="' + url + '" class="list-group-item">' + data.name + ' (' + data.phone_number + ')' + ' - ' + data.code + '</a>';
                        }
                    }
                },
            ]);

        });
    </script>
@stop
