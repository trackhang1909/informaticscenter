@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/course.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
@endsection
@section('javascript')
    <script>
        $(".chosen-select").chosen({ width: "100%", no_results_text: "{{ __('schedule.empty_day') }}" });
    </script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{ __('schedule.list') }}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">{{ __('action.home') }}</a></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-course">
                <div class="btn-header">
                    @can('create')
                        <button style="margin-right: 15px" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalAdd">
                            {{__('schedule.add')}}
                        </button>
                    @elsecan('superAdmin')
                        <button style="margin-right: 15px" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalAdd">
                            {{__('schedule.add')}}
                        </button>
                    @endcan

                    <a class="btn btn-primary" href="{{ route('schedule.general') }}" role="button">{{__('schedule.general')}}</a>
                </div>
                <form class="form-container form-add" action="{{ route('schedule.store') }}" method="POST">
                    <div class="modal fade" id="exampleModalAdd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{ __('schedule.name') }}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInput1">{{__('schedule.period')}} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput1" name="period_id">
                                            @foreach($periods as $period)
                                                <option value="{{ $period->id }}">{{ $period->name.' - '.$period->start_time.__('schedule.to').$period->end_time }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{__('schedule.class')}} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput2" name="class_id">
                                            @foreach($classes as $class)
                                                <option value="{{ $class->id }}">{{ $class->name.' - '.$class->campus_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{__('schedule.course')}} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput3" name="course_id">
                                            @foreach($courses as $course)
                                                <option value="{{ $course->id }}">{{ $course->name.' - '.__('course.group').': '.$course->group }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput5">{{__('schedule.teacher')}} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput5" name="teacher_id">
                                            @foreach($teachers as $teacher)
                                                <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput4">{{__('schedule.teach_day')}} <label class="required-p">*</label></label>
                                        <select class="form-control chosen-select" id="exampleInput4" name="weekday[]" multiple data-placeholder="{{ __('schedule.select_day') }}">
                                            <option value="1">{{ __('schedule.monday') }}</option>
                                            <option value="2">{{ __('schedule.tuesday') }}</option>
                                            <option value="3">{{ __('schedule.wednesday') }}</option>
                                            <option value="4">{{ __('schedule.thursday') }}</option>
                                            <option value="5">{{ __('schedule.friday') }}</option>
                                            <option value="6">{{ __('schedule.saturday') }}</option>
                                            <option value="7">{{ __('schedule.sunday') }}</option>
                                        </select>
                                        <label for="exampleInput4" generated="true" class="weekday error"></label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                    <button type="submit" class="btn btn-primary">{{__('action.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                @if(session('success'))
                    <div class="alert alert-success abc" role="alert">
                        {{ session('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body table-responsive">
                    <div>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th scope="col">{{__('action.id')}}</th>
                                <th scope="col">{{__('schedule.course')}}</th>
                                <th scope="col">{{__('schedule.period')}}</th>
                                <th scope="col">{{__('schedule.class')}}</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($data as $item => $event)
                                <tr>
                                    <th scope="row">{{ $data->firstItem() + $item }}</th>
                                    <td>{{ $event->course_name.' - '.__('course.group').': '.$event->group }}</td>
                                    <td>{{ $event->period_name.' - '.$event->start_time.__('schedule.to').$event->end_time }}</td>
                                    <td>{{ $event->class_name.' - '.$event->campus_name }}</td>
                                    <td>
                                        <a class="btn btn-xs btn-secondary" href="{{ route('schedule.show', $event->id) }}" role="button">{{ __('schedule.detail') }}</a>
                                        <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#exampleModalDetail{{ $event->id }}">
                                            {{__('action.detail')}}
                                        </button>
                                        @can('update')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $event->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#exampleModalUpdate{{ $event->id }}">
                                                {{__('action.update')}}
                                            </button>
                                        @endcan

                                        @can('delete')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $event->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @elsecan('superAdmin')
                                            <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#exampleModalDelete{{ $event->id }}">
                                                {{__('action.delete')}}
                                            </button>
                                        @endcan

                                        <div class="modal fade" id="exampleModalDetail{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('schedule.name')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            <p>{{__('schedule.teacher')}}: {{ $event->teacher_name }}</p>
                                                            <p>{{__('class.membership')}}: {{ $event->membership }}</p>
                                                            <p>
                                                                {{__('schedule.teach_day').': '}}
                                                                <?php
                                                                /** @var TYPE_NAME $event */
                                                                $weekday = explode(' ', $event->weekday);
                                                                foreach ($weekday as $item) {
                                                                    if ($item == 1) {
                                                                        echo __('schedule.monday').' ';
                                                                    }
                                                                    if ($item == 2) {
                                                                        echo __('schedule.tuesday').' ';
                                                                    }
                                                                    if ($item == 3) {
                                                                        echo __('schedule.wednesday').' ';
                                                                    }
                                                                    if ($item == 4) {
                                                                        echo __('schedule.thursday').' ';
                                                                    }
                                                                    if ($item == 5) {
                                                                        echo __('schedule.friday').' ';
                                                                    }
                                                                    if ($item == 6) {
                                                                        echo __('schedule.saturday').' ';
                                                                    }
                                                                    if ($item == 7) {
                                                                        echo __('schedule.sunday').' ';
                                                                    }
                                                                }
                                                                ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <form class="form-container" action="{{ route('schedule.destroy', $event->id) }}" method="POST">
                                            <input name="_method" type="hidden" value="DELETE">
                                            <div class="modal fade" id="exampleModalDelete{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('class.campus')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            @csrf
                                                            <div class="form-group">
                                                                <input style="display: none" type="text" name="id" class="form-control" id="exampleInputStt" value="{{ $event->id }}">
                                                            </div>
                                                            <p>{{__('action.confirm_delete')}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.delete')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>

                                        <form class="form-container form-add" action="{{ route('schedule.store') }}" method="POST">
                                            <div class="modal fade" id="exampleModalUpdate{{ $event->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title" id="exampleModalLabel">{{__('class.campus')}}</h5>
                                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>

                                                        <div class="modal-body">
                                                            @csrf
                                                            <input style="display: none" type="text" name="id" class="id-course form-control" id="exampleInputStt" value="{{ $event->id }}" readonly>
                                                            <div class="form-group">
                                                                <label for="exampleInput1">{{__('schedule.period')}} <label class="required-p">*</label></label>
                                                                <select class="form-control" id="exampleInput1" name="period_id">
                                                                    <option value="{{ $event->period_id }}">{{ $event->period_name.' - '.$event->start_time.__('schedule.to').$event->end_time }}</option>
                                                                    @foreach($periods as $period)
                                                                        <option value="{{ $period->id }}">{{ $period->name.' - '.$period->start_time.__('schedule.to').$period->end_time }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="exampleInput1" generated="true" class="error"></label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput2">{{__('schedule.class')}} <label class="required-p">*</label></label>
                                                                <select class="form-control" id="exampleInput2" name="class_id">
                                                                    <option value="{{ $event->class_id }}">{{ $event->class_name.' - '.$event->campus_name }}</option>
                                                                    @foreach($classes as $class)
                                                                        <option value="{{ $class->id }}">{{ $class->name.' - '.$class->campus_name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="exampleInput2" generated="true" class="error"></label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput6">{{__('class.membership')}} <label class="required-p">*</label></label>
                                                                <input type="number" name="membership" class="form-control" id="exampleInput6" value="{{ $event->membership }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput3">{{__('schedule.course')}} <label class="required-p">*</label></label>
                                                                <select class="form-control" id="exampleInput3" name="course_id">
                                                                    <option value="{{ $event->course_id }}">{{ $event->course_name.' - '.__('course.group').': '.$event->group }}</option>
                                                                    @foreach($courses as $course)
                                                                        <option value="{{ $course->id }}">{{ $course->name.' - '.__('course.group').': '.$course->group }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="exampleInput3" generated="true" class="error"></label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput5">{{__('schedule.teacher')}} <label class="required-p">*</label></label>
                                                                <select class="form-control" id="exampleInput5" name="teacher_id">
                                                                    <option value="{{ $event->teacher_id }}">{{ $event->teacher_name }}</option>
                                                                    @foreach($teachers as $teacher)
                                                                        <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                                                    @endforeach
                                                                </select>
                                                                <label for="exampleInput5" generated="true" class="error"></label>
                                                            </div>
                                                            <div class="form-group">
                                                                <label for="exampleInput4">{{__('schedule.teach_day')}} <label class="required-p">*</label></label>
                                                                <select class="form-control chosen-select" id="exampleInput4" name="weekday[]" multiple data-placeholder="{{ __('schedule.select_day') }}">
                                                                    <?php
                                                                        $day = array(__('schedule.monday'), __('schedule.tuesday'), __('schedule.wednesday'), __('schedule.thursday'), __('schedule.friday'), __('schedule.saturday'), __('schedule.sunday'));
                                                                        for($i = 1; $i < 8; $i++) {
                                                                            $count = 0;
                                                                            foreach ($weekday as $day2) {
                                                                                if ($day2 == $i) {
                                                                                    echo "<option selected value='".$i."'>".$day[$i-1]."</option>";
                                                                                    $count++;
                                                                                }
                                                                            }
                                                                            if ($count == 0) {
                                                                                echo "<option value='".$i."'>".$day[$i-1]."</option>";
                                                                            }
                                                                        }
                                                                    ?>
                                                                </select>
                                                                <label for="exampleInput4" generated="true" class="weekday error"></label>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('action.close')}}</button>
                                                            <button type="submit" class="btn btn-primary">{{__('action.update')}}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="clearfix">
                        <div style="float: right">
                            {!! $data->links() !!}
                        </div>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <script type="text/javascript">

        $("select.chosen-select").each(function() {
            $(this).on("change", function () {
                if ($(this).val().length > 0) {
                    console.log($(".chosen-select").val());
                    console.log($('.weekday.error').text())
                    $('.weekday.error').text("");
                }
            })
        });

        $(document).ready(function() {

            $("form.form-add").each(function() {
                // $.validator.setDefaults({ ignore: ":hidden:not(.chosen-select)" });
                $(this).validate({
                    ignore: [],
                    rules: {
                        course_id: {
                            remote: "/unique-course?id=" + $(this).find('.id-course').first().val(),
                        },
                        'weekday[]': {
                            required: true,
                        },
                    },
                    messages: {
                        course_id: {
                            remote: "<?php echo __("course.course").' '.__("action.exist") ?>",
                        },
                        'weekday[]': {
                            required: "<?php echo __("schedule.teach_day").__("action.required") ?>",
                        },
                    },
                });
            });

            var engine1 = new Bloodhound({
                remote: {
                    url: '/schedule/find/%QUERY%',
                    wildcard: '%QUERY%'
                },
                datumTokenizer: Bloodhound.tokenizers.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace
            });

            $(".search-input").typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, [
                {
                    source: engine1.ttAdapter(),
                    name: 'sche-course-name',
                    limit: 20,
                    display: function(data) {
                        return data.name;
                    },
                    templates: {
                        empty: [
                            '<div class="list-group search-results-dropdown"><div class="list-group-item">{{ __('course.empty') }}</div></div>'
                        ],
                        header: [
                        ],
                        suggestion: function (data) {
                            var url = '{{ route("schedule.detail", ":id") }}';
                            url = url.replace(':id', data.id);
                            return '<a href="' + url + '" class="list-group-item">' + data.course_name + " - " + "{{ __('course.group') }}" + ": "  + data.group + '</a>';                        }
                    }
                },
            ]);

        });
    </script>
@stop
