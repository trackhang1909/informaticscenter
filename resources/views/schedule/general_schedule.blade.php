@extends('layout.master')
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">

            <ul class="list-group" style="margin-bottom: 40px">
                <li class="list-group-item list-group-item-primary">{{__('class.class')}}</li>
                <li class="list-group-item">
                    @foreach($classes as $class)
                        <a style="float: right" class="btn btn-xs btn-outline-info" href="{{ route('schedule.class', $class->id) }}" role="button">{{__('schedule.show_general')}}</a>
                        <p>{{ $class->name.' - '.__('class.campus').': '.$class->campus_name }} </p>
                    @endforeach
                </li>
            </ul>

            <ul class="list-group" style="margin-bottom: 40px">
                <li class="list-group-item list-group-item-primary">{{__('schedule.teacher')}}</li>
                <li class="list-group-item">
                    @foreach($teachers as $teacher)
                        <a style="float: right" class="btn btn-xs btn-outline-info" href="{{ route('schedule.teacher', $teacher->id) }}" role="button">{{__('schedule.show_general')}}</a>
                        <p>{{ $teacher->name }} </p>
                    @endforeach
                </li>
            </ul>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@stop
