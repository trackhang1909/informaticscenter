@extends('layout.master')
@section('css')
    <link rel="stylesheet" href="{{ asset('css/staff/staff.css') }}">
@endsection
@section('javascript')
    <script src="{{ asset('js/ckeditor.js') }}"></script>
@endsection
@section('content')
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">{{__('msg.topiclist')}}</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard v1</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->

        </div><!-- /.container-fluid -->
    </div>

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="clearfix header-kh">

                @if(session()->get('success'))
                    <div class="alert alert-success abc">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session('fail'))
                    <div class="alert alert-danger abc" role="alert">
                        {{ session('fail') }}
                    </div>
                @endif
                    <button type="button" class="btn btn-primary them" data-toggle="modal" data-target="#exampleModalThem">
                        {{__('msg.addtopic')}}
                    </button>
{{--                    <form action="{{route('type.index')}}" method="GET">--}}
{{--                        <button type="submit" class="btn btn-warning " data-toggle="modal" data-target="#exampleModal" style="margin-left: 15px">--}}
{{--                            {{__('msg.type')}}--}}
{{--                        </button>--}}
{{--                    </form>--}}


                <form class="form-container form-add" action="{{route('topic.add')}}" method="POST">
                    <div class="modal fade" id="exampleModalThem" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">{{__('msg.addtopic')}}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    @csrf
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">{{ __('msg.topicname') }} <label class="required-p">*</label></label>
                                        <input type="text" name="name" class="form-control" id="exampleInputPassword1"
                                               placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput1">{{ __('msg.type') }} <label class="required-p">*</label></label>
                                        <select class="form-control" id="exampleInput1" name="type" onchange="kiemtra()">
                                            <option hidden></option>
                                            <option id="yes">Có hỗ trợ</option>
                                            <option id="no">Không hỗ trợ</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput2">{{ __('msg.totalmoney') }} <label class="required-p">*</label></label>
                                        <input type="text" name="total_money" class="form-control" id="exampleInput2" placeholder="">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInput3">{{ __('msg.content') }} <label class="required-p">*</label></label>
                                        <textarea class="form-control summary-ckeditor" id="exampleInput3" name="content"></textarea>
                                        <label for="content" class="error"></label>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">{{__('msg.no')}}</button>
                                    <button type="submit" class="btn btn-primary"
                                            onclick="save()">{{__('msg.add')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>

            </div>

            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="panel-body table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th scope="col" style="text-align: center">{{__('msg.number')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.topicname')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.type')}}</th>
                            <th scope="col" style="text-align: center">{{__('msg.totalmoney')}}</th>
{{--                            <th scope="col" style="text-align: center">{{__('msg.content')}}</th>--}}
                            <th id="" style="width: 200px"></th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($data as $item => $topic)
                            <tr>
                                <th scope="row" style="text-align: center">{{ $data->firstItem()+ $item }}</th>
                                <td style="text-align: center">{{ $topic->name }}</td>
                                <td style="text-align: center">{{ $topic->type }}</td>
                                <td style="text-align: center">{{ $topic->total_money }}</td>
{{--                                <td style="text-align: center">{{ $topic->content }}</td>--}}
                                <td>
                                    <button type="button" class="btn btn-xs btn-info" data-toggle="modal"
                                            data-target="#exampleModalXem{{ $topic->id }}">
                                        {{__('msg.detail')}}
                                    </button>
                                    <button type="button" class="btn btn-xs btn-success" data-toggle="modal"
                                            data-target="#exampleModalCn{{ $topic->id }}">
                                        {{__('msg.update')}}
                                    </button>
                                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal"
                                            data-target="#exampleModalXoa{{ $topic->id }}">
                                        {{__('msg.delete')}}
                                    </button>

                                    <form class="form-container" action="{{route('topic.delete')}}" method="GET">
                                        <div class="modal fade" id="exampleModalXoa{{ $topic->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">{{__('msg.staff')}}</h5>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="text" name="id"
                                                                   class="form-control" id="exampleInputStt"
                                                                   value="{{ $topic->id }}">
                                                        </div>
                                                        <p>{{__('action.confirm_delete')}}</p>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.yes')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <div class="modal fade" id="exampleModalXem{{ $topic->id }}" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel"
                                                        style="text-align: center">{{__('msg.detail.content')}}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
{{--                                                <div class="modal-body ">--}}
{{--                                                    <h5><i>{{__('msg.name')}} :</i><b> {{ $topic->name }}</b></h5>--}}
{{--                                                </div>--}}
                                                <div class="modal-body ">
                                                     <textarea class="form-control summary-ckeditor" id="summary-ckeditor" name="content">
                                                         {{ $topic->content }}
                                                     </textarea>
                                                </div>
{{--                                                <div class="modal-body ">--}}
{{--                                                    <h5><i>{{__('msg.email')}} :</i><b> {{ $topic->type }}</b></h5>--}}
{{--                                                </div>--}}
{{--                                                <div class="modal-body ">--}}
{{--                                                    <h5><i>{{__('msg.phonenumber')}} :</i><b> {{ $topic->total_money }}</b></h5>--}}
{{--                                                </div>--}}

                                            </div>
                                        </div>
                                    </div>

                                    <form class="form-container form-add" action="{{route('topic.add')}}" method="POST">
                                        <div class="modal fade" id="exampleModalCn{{ $topic->id }}" tabindex="-1"
                                             role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h5 class="modal-title"
                                                            id="exampleModalLabel">{{__('msg.update')}}</h5>

                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        @csrf
                                                        <div class="form-group">
                                                            <input style="display: none" type="number" name="id" class="id-staff form-control"
                                                                   id="exampleInputStt" value="{{ $topic->id }}" readonly>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput">{{ __('msg.topicname') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="name" class="form-control"
                                                                   id="exampleInput" value="{{ $topic->name }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput3">{{ __('msg.type') }} <label class="required-p">*</label></label>
                                                            <select class="form-control" id="exampleInput3" name="type" value="{{$topic->type}}">
                                                                <option >{{$topic->type}}</option>
                                                                <option >Có hỗ trợ</option>
                                                                <option>Không hỗ trợ</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput4">{{ __('msg.invest') }} <label class="required-p">*</label></label>
                                                            <input type="text" name="total_money" class="form-control"
                                                                   id="exampleInput4" value="{{ $topic->total_money }}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="exampleInput5">{{ __('msg.detail.content') }} <label class="required-p">*</label></label>
                                                            <textarea class="form-control summary-ckeditor" id="exampleInput5" name="content">{{$topic->content}}</textarea>
                                                            <label for="exampleInput5" class="error"></label>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-secondary"
                                                                data-dismiss="modal">{{__('msg.no')}}</button>
                                                        <button type="submit"
                                                                class="btn btn-primary">{{__('msg.update')}}</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clearfix">
                <div style="float: right">
                    {{ $data->links() }}
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
                <script type="text/javascript">
                    function kiemtra() {
                        var kiemtra = document.getElementById('exampleInput1').value()
                        var khong = document.getElementById('no').value()
                        if(kiemtra == khong)
                        {
                           return document.getElementById("exampleInput2").disabled = true
                        }
                        return  document.getElementById("exampleInput2").disabled= false
                    }
                </script>

@stop
