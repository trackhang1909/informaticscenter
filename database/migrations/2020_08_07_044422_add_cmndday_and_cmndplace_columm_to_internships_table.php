<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCmnddayAndCmndplaceColummToInternshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('internships', function (Blueprint $table) {
            if (!Schema::hasColumn('internships', 'cmndday')) {
                $table->date('cmndday');
            }
            if (!Schema::hasColumn('internships', 'cmndplace')) {
                $table->string('cmndplace');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('internships', function (Blueprint $table) {
            if (Schema::hasColumn('internships', 'cmndday')) {
                $table->dropColumn('cmndday');
            }
            if (Schema::hasColumn('internships', 'cmndplace')) {
                $table->dropColumn('cmndplace');
            }
        });
    }
}
