<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBookIdAndQuantityAndMoneyColumnInCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function (Blueprint $table) {
            $table->integer('book_id');
            $table->integer('quantity');
            $table->integer('money');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasColumn('customers','book_id'))
        Schema::table('customers', function (Blueprint $table) {
            $table->dropColumn('book_id');
        });

        if(Schema::hasColumn('customers','quantity'))
            Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('quantity');
            });

        if(Schema::hasColumn('customers','money'))
            Schema::table('customers', function (Blueprint $table) {
                $table->dropColumn('money');
            });
    }
}
