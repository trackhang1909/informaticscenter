<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SchedulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('schedules')->insert([
            [
                'period_id' => '1',
                'class_id' => '1',
                'course_id' => '5',
                'weekday' => '1 3',
                'teacher_id' => '1',
                'membership' => '99'
            ],
            [
                'period_id' => '1',
                'class_id' => '1',
                'course_id' => '10',
                'weekday' => '2 4',
                'teacher_id' => '1',
                'membership' => '100'
            ],
            [
                'period_id' => '2',
                'class_id' => '1',
                'course_id' => '4',
                'weekday' => '1 3 5',
                'teacher_id' => '1',
                'membership' => '99'
            ],
            [
                'period_id' => '1',
                'class_id' => '5',
                'course_id' => '7',
                'weekday' => '1 3',
                'teacher_id' => '2',
                'membership' => '100'
            ],
            [
                'period_id' => '3',
                'class_id' => '1',
                'course_id' => '1',
                'weekday' => '2 4 6',
                'teacher_id' => '2',
                'membership' => '99'
            ],
        ]);
    }
}
