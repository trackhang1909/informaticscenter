<?php

use Illuminate\Database\Seeder;

class ReceiptsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('receipts')->insert([
            [
                'code' => 'PT-TQ/2020/0001',
                'register_time' => '2020-08-28 07:13:14',
                'advance_money' => '15000000',
                'total_money' => '15000000',
                'remain_money' => '0',
                'student_id' => '1',
                'schedule_id' => '1',
                'reason' => 'Đóng học phí',
                'money_text' => 'Mười lăm triệu đồng chẵn',
            ],
            [
                'code' => 'PT-TQ/2020/0002',
                'register_time' => '2020-08-28 07:13:44',
                'advance_money' => '7000000',
                'total_money' => '12000000',
                'remain_money' => '5000000',
                'student_id' => '4',
                'schedule_id' => '3',
                'reason' => 'Tạm ứng học phí',
                'money_text' => 'Bảy triệu đồng chẵn',
            ],
            [
                'code' => 'PT-TQ/2020/0003',
                'register_time' => '2020-08-28 07:14:22',
                'advance_money' => '10000000',
                'total_money' => '10000000',
                'remain_money' => '0',
                'student_id' => '9',
                'schedule_id' => '5',
                'reason' => 'Đóng học phí',
                'money_text' => 'Mười triệu đồng chẵn',
            ],
        ]);
    }
}
