<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('students')->insert([
            [
                'name' => 'Nguyễn Văn A',
                'phone_number' => '0123456789',
                'email' => 'a@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123A, HCM City',
                'user_id' => '2',
            ],
            [
            'name' => 'Nguyễn Văn B',
                'phone_number' => '0223456789',
                'email' => 'b@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123B, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn C',
                'phone_number' => '0323456789',
                'email' => 'c@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123C, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn D',
                'phone_number' => '0423456789',
                'email' => 'd@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123D, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn E',
                'phone_number' => '0523456789',
                'email' => 'e@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123E, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn F',
                'phone_number' => '0623456789',
                'email' => 'f@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123F, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn G',
                'phone_number' => '0723456789',
                'email' => 'g@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123G, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn H',
                'phone_number' => '0823456789',
                'email' => 'h@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123H, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn I',
                'phone_number' => '0923456789',
                'email' => 'i@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123I, HCM City',
                'user_id' => '2',
            ],
            [
                'name' => 'Nguyễn Văn J',
                'phone_number' => '01023456789',
                'email' => 'j@gmail.com',
                'birthday' => '2020-08-27',
                'address' => '123J, HCM City',
                'user_id' => '2',
            ]
        ]);
    }
}
