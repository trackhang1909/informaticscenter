<?php

use Illuminate\Database\Seeder;

class PeriodsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periods')->insert([
            [
                'name' => 'Ca 1',
                'start_time' => '07:00:00',
                'end_time' => '09:00:00',
            ],
            [
                'name' => 'Ca 2',
                'start_time' => '09:30:00',
                'end_time' => '11:30:00',
            ],
            [
                'name' => 'Ca 3',
                'start_time' => '12:00:00',
                'end_time' => '14:00:00',
            ],
            [
                'name' => 'Ca 4',
                'start_time' => '14:30:00',
                'end_time' => '16:30:00',
            ],
            [
                'name' => 'Ca 5',
                'start_time' => '17:00:00',
                'end_time' => '19:00:00',
            ],
        ]);
    }
}
