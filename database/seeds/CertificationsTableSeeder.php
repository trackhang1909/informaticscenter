<?php

use Illuminate\Database\Seeder;

class CertificationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('certifications')->insert([
            [
                'student_id' => '1',
                'course_id' => '5',
                'code' => 'CN-TQ/2020/0001',
                'create_date' => '2020-08-28',
                'rank' => 'Giỏi',
                'point' => '10',
                'test_date' => '2020-08-28',
            ],
        ]);
    }
}
