<?php

use Illuminate\Database\Seeder;

class StaffsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('staffs')->insert([
            [
                'name' => 'Dannie Hagenes',
                'birthday' => '1983-01-15',
                'email' => 'gbatz@yahoo.com',
                'phone_number' => '52347377',
                'country' => 'UK',
                'start_time' => '1999-12-23',
                'salary' => '10000000',
                'position' => 'GV',
            ],
            [
                'name' => 'Rory Greenholt MD',
                'birthday' => '1974-05-15',
                'email' => 'md@yahoo.com',
                'phone_number' => '523473789',
                'country' => 'UK',
                'start_time' => '1999-12-23',
                'salary' => '10000000',
                'position' => 'GV',
            ],
        ]);
    }
}
