<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            [
                'name' => 'view',
                'description' => 'Xem dữ liệu',
            ],
            [
                'name' => 'create',
                'description' => 'Tạo dữ liệu',
            ],
            [
                'name' => 'update',
                'description' => 'Cập nhật dữ liệu',
            ],
            [
                'name' => 'delete',
                'description' => 'Xóa dữ liệu',
            ],
            [
                'name' => 'superAdmin',
                'description' => 'Toàn quyền hệ thống',
            ],
            [
                'name' => 'staff',
                'description' => 'Thêm, cập nhật và xuất phiếu thu học viên',
            ],
            [
                'name' => 'cashBook',
                'description' => 'Thao tác sổ quỹ',
            ],
        ]);
    }
}
