<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CashBookTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cash_book')->insert([
            [
                'cash_code' => 'PT-TQ/2020/0001',
            ],
            [
                'cash_code' => 'PT-TQ/2020/0002',
            ],
            [
                'cash_code' => 'PT-TQ/2020/0003',
            ],
            [
                'cash_code' => 'PT/2020/0001',
            ],
            [
                'cash_code' => 'PT/2020/0002',
            ],
        ]);
    }
}
