<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClassesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classes')->insert([
            [
                'name' => 'A001',
                'campus_code' => 'Q7',
                'membership' => '100',
            ],
            [
                'name' => 'A002',
                'campus_code' => 'Q7',
                'membership' => '100',
            ],
            [
                'name' => 'A003',
                'campus_code' => 'Q7',
                'membership' => '100',
            ],
            [
                'name' => 'A004',
                'campus_code' => 'Q7',
                'membership' => '100',
            ],
            [
                'name' => 'A005',
                'campus_code' => 'Q7',
                'membership' => '100',
            ],
            [
                'name' => 'A001',
                'campus_code' => 'Q9',
                'membership' => '100',
            ],
            [
                'name' => 'A002',
                'campus_code' => 'Q9',
                'membership' => '100',
            ],
            [
                'name' => 'A003',
                'campus_code' => 'Q9',
                'membership' => '100',
            ],
            [
                'name' => 'A004',
                'campus_code' => 'Q9',
                'membership' => '100',
            ],
            [
                'name' => 'A005',
                'campus_code' => 'Q9',
                'membership' => '100',
            ],
        ]);
    }
}
