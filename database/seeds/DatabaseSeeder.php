<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        $this->call(BooksTableSeeder::class);
        $this->call(CampusesTableSeeder::class);
        $this->call(CertificationsTableSeeder::class);
        $this->call(ClassesTableSeeder::class);
        $this->call(CoursesTableSeeder::class);
//        $this->call(InternsTableSeeder::class);
        $this->call(PeriodsTableSeeder::class);
//        $this->call(PositionsTableSeeder::class);
        $this->call(ReceiptsTableSeeder::class);
        $this->call(SchedulesTableSeeder::class);
        $this->call(StaffsTableSeeder::class);
//        $this->call(TopicsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(StudentsTableSeeder::class);
        $this->call(PermissionTableSeeder::class);
        $this->call(RolePermissionTableSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(ReceiptDifferentTableSeeder::class);
        $this->call(CashBookTableSeeder::class);
    }
}
