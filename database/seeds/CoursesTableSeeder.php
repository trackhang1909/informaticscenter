<?php

use Illuminate\Database\Seeder;

class CoursesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert([
            [
                'name' => 'Tin học văn phòng',
                'content' => 'Dạy tin học văn phòng',
                'fees' => '10000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '1',
                'type' => 'Học tại trung tâm',
            ],
            [
                'name' => 'Anh Văn',
                'content' => 'Dạy anh văn',
                'fees' => '5000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '1',
                'type' => 'Học tại trung tâm',
            ],
            [
                'name' => 'Word',
                'content' => 'Dạy word',
                'fees' => '11000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '1',
                'type' => 'Học tại trung tâm',
            ],
            [
                'name' => 'Excel',
                'content' => 'Dạy excel',
                'fees' => '12000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '1',
                'type' => 'Học tại trung tâm',
            ],
            [
                'name' => 'Access',
                'content' => 'Dạy access',
                'fees' => '15000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '1',
                'type' => 'Học tại trung tâm',
            ],
            [
                'name' => 'Tin học văn phòng',
                'content' => 'Dạy tin học văn phòng',
                'fees' => '10000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '2',
                'type' => 'Học trực tuyến',
            ],
            [
                'name' => 'Anh Văn',
                'content' => 'Dạy anh văn',
                'fees' => '5000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '2',
                'type' => 'Học trực tuyến',
            ],
            [
                'name' => 'Word',
                'content' => 'Dạy word',
                'fees' => '11000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '2',
                'type' => 'Học trực tuyến',
            ],
            [
                'name' => 'Excel',
                'content' => 'Dạy excel',
                'fees' => '12000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '2',
                'type' => 'Học trực tuyến',
            ],
            [
                'name' => 'Access',
                'content' => 'Dạy access',
                'fees' => '15000000',
                'close_time' => '2020-10-27',
                'open_time' => '2020-08-27',
                'group' => '2',
                'type' => 'Học trực tuyến',
            ],
        ]);
    }
}
