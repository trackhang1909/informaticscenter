<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ReceiptDifferentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('receipt_differents')->insert([
            [
                'created_at' => '2020-08-28 07:21:08',
                'updated_at' => '2020-08-28 07:21:08',
                'code' => 'PT/2020/0001',
                'name' => 'Công ty ABC',
                'address' => '125, LA Street',
                'reason' => 'Tài trợ',
                'total_money' => '150000000',
                'money_text' => 'Một trăm năm mươi triệu đồng',
            ],
            [
                'created_at' => '2020-08-28 07:21:58',
                'updated_at' => '2020-08-28 07:21:08',
                'code' => 'PT/2020/0002',
                'name' => 'Công ty HG',
                'address' => '127, LA Street',
                'reason' => 'Tài trợ',
                'total_money' => '700000000',
                'money_text' => 'Bảy trăm triệu đồng chẵn',
            ],
        ]);
    }
}
