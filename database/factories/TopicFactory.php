<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Topic::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'content' => $faker->text,
        'type' => $faker->text,
        'total_money' => $faker->randomNumber(5),
    ];
});
