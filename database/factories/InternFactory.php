<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Intern::class, function (Faker $faker) {
    return [
        'student_id' => $faker->randomDigit,
        'type' => $faker->text,
        'topic_id' => $faker->randomDigit,
        'total_time' => $faker->text,
        'start_time' => $faker->date(),
        'end_time' => $faker->date(),
    ];
});
