<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Certification::class, function (Faker $faker) {
    return [
        'register_course_id' => $faker->randomDigit,
        'start_time' => $faker->date(),
        'end_time' => $faker->date(),
    ];
});
