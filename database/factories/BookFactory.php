<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Book::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'author' => $faker->name,
        'price' => $faker->randomNumber(5),
        'quantity' => $faker->randomNumber(5),
        'image'=>$faker->randomNumber(5),
    ];
});
